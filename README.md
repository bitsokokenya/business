# business

**please wait.. commit rules will be updated shortly**

stay tuned to our [developer community channel](https://discord.gg/MmbaCbC5Vx) for updates


make sure your node is accessible on a public ip address, if not you can use the nameservers below

ns1.bitsoko.org

ns2.bitsoko.org

or use this dns route to add a CNAME record to your existing domain

cname.bitsoko.org



`#install
$ bash <(curl -s https://get.bitsoko.org/soko) -a install -d exampleshop.com -c algorand
 
#update
$ bash <(curl -s https://get.bitsoko.org/soko) -a update

#remove
$ bash <(curl -s https://get.bitsoko.org/soko) -a remove 
`
     

**Disclaimer:** Just a friendly reminder to please note this project is still under continouus and heavy development, which means things may break or change from time to time. But that doesn't mean we want it to break! So we ask if you do encounter any issues, to please file them and one of us will try to respond as quickly as possible.

   //contracts
           
  
             
             async function editTran(id,conf,amount,time,high,low,type,sender){
                 
               //status codes
               //["opening","trading","closing","expiring"]
               //
               
    switch (type) {
      case 'contract': 
            
     bgCl = '#ffc1005e';
     outCls = 'badge-warning';
      break;
      case 'token': 
         
     bgCl = '#28a74566';
     outCls = 'badge-success';     
      break;
      case 'unknown': 
         
     bgCl = '#00000000';      
 
     outCls = 'badge-danger';
		} 
		
               
    switch (status) {
      case 'opening': 
            
     bgSt = 'lighter';          
      break;
      case 'trading': 
         
     bgSt = 'success';     
      break;
      case 'closing': 
         
     bgSt = 'warning';      
      break;
      case 'expiring': 
         
     bgSt = 'danger';   
      break;
      default:
     bgSt = 'dark';      
 
		} 
		

const template = document.createElement('template');
var tRate=0.00000034;
                 

                 var transTemplate='<tr class="data-item">'+
                                    '<td class="data-col dt-tnxno">'+
                                        '<div class="d-flex align-items-center">'+
                                            '<div class="data-state data-state-approved">'+
                                                '<span class="d-none">Pending</span>'+
                                            '</div>'+
                                            '<div class="fake-class">'+
                                                '<span class="lead tnx-id" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;width: 90px;">'+id+'</span>'+
                                                '<span class="sub sub-date">'+timeConverter(time)+'</span>'+
                                            '</div>'+
                                        '</div>'+
                                    '</td>'+
                                    '<td class="data-col dt-amount">'+
                                        '<span class="lead token-amount">'+amount+'</span>'+
                                        '<span class="sub sub-symbol">PLAAS</span>'+
                                    '</td>'+
                                    '<td class="data-col dt-btc-amount">'+
                                        '<span class="lead amount-pay">'+amount*tRate+'</span>'+
                                        '<span class="sub sub-symbol">'+
                                            tRate+'BTC/PLAAS'+
                                        '</span>'+
                                    '</td>'+
                                    '<td class="data-col dt-account">'+
                                    
                                        '<div class="d-flex align-items-center">'+
                                        '<span class="lead user-info" style="white-space: nowrap;overflow: hidden;text-overflow: ellipsis;width: 150px;">'+sender+'</span></br>'+
                                        '<span class="sub sub-date"> <em class="fas fa-info-circle" data-toggle="tooltip" data-placement="bottom" title="etherdelta/76y3e3--"></em></span>'+
                                                '</div>'+
                                    
                                    '</td>'+
                                    '<td class="data-col dt-type">'+
                                        '<span class="dt-type-md badge badge-outline  '+outCls+' badge-md" style="background:'+bgCl+';">'+type+'</span>'+
                                        '<span class="dt-type-sm badge badge-sq badge-outline '+outCls+' badge-md" style="background:'+bgCl+';">'+type.charAt(0)+'</span>'+
                                    '</td>'+
                                    '<td class="data-col text-right">'+
                                        '<ul class="data-action-list d-none d-md-inline-flex">'+
                                            '<li>'+
                                                '<a href="#" class="btn btn-auto btn-primary btn-xs">'+
                                                    '<span>'+
                                                        'view'+
                                                    '</span>'+
                                                    '<em class="ti ti-wallet"></em>'+
                                                '</a>'+
                                            '</li>'+
                                            //'<li><a href="#" class="btn btn-danger-alt btn-xs btn-icon"><em class="ti ti-trash"></em></a></li>'+
                                        '</ul>'+
                                    '</td>'+
                                '</tr>'+
                                '<!-- .data-item -->';
                 template.innerHTML = transTemplate;
                 return template.content.firstElementChild;
             }
             
            async function addTrans(){
                var transHolder=document.querySelector('#transactions-holder');
                
                var data = await fetch('https://api.etherscan.io/api?module=account&action=txlist&address=0xEB9cfFf249feF6427bf02881f89eDe58DD8f91BB&apikey=Q1VDCIGKZK189XPMT8W8EX7QUH166U94PK');
                var res = await data.json();
                var cList=res.result.reverse();
                
                var allTrans=[];
                
                for(var i in cList){
                    
                    var editedTran= editTran(cList[i].hash,'5',1280,cList[i].timeStamp,1340,1290,'contract',cList[i].from);
                    
                    allTrans.push(editedTran);
                    
                   // transHolder.appendChild(editedTran);
                
                    
                } 
             
                
                
                var data = await fetch('https://api.etherscan.io/api?module=account&action=tokentx&contractaddress=0x60571e95e12c78cba5223042692908f0649435a5&page=1&offset=100&sort=dec&apikey=Q1VDCIGKZK189XPMT8W8EX7QUH166U94PK');
                var res = await data.json();
                var cList=res.result.reverse();
                
                
                
                
                
                for(var i in cList){
                    
                    var editedTran= editTran(cList[i].hash,'5',cList[i].value/Math.pow(10, cList[i].tokenDecimal),cList[i].timeStamp,1340,1290,'token',cList[i].from); 
                    
                    allTrans.push(editedTran);
                    
                
                    
                } 
                
                
Promise.all(allTrans).then(function(values) {
  for(var j in values){
      
                    transHolder.appendChild(values[j]);
  }
});
             
                 
             }
             addTrans();
             
             function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}


(self.AMP = self.AMP || []).push({
    n: "amp-script",
    v: "1911191835190",
    f: (function(AMP, _) {
        var q;
        function aa(a) {
            var b = 0;
            return function() {
                return b < a.length ? {
                    done: !1,
                    value: a[b++]
                } : {
                    done: !0
                }
            }
        }
        var ba = "function" == typeof Object.defineProperties ? Object.defineProperty : function(a, b, c) {
            a != Array.prototype && a != Object.prototype && (a[b] = c.value)
        }
          , t = function(a) {
            return "undefined" != typeof window && window === a ? a : "undefined" != typeof global && null != global ? global : a
        }(this);
        function u() {
            u = function() {}
            ;
            t.Symbol || (t.Symbol = ca)
        }
        function da(a, b) {
            this.R = a;
            ba(this, "description", {
                configurable: !0,
                writable: !0,
                value: b
            })
        }
        da.prototype.toString = function() {
            return this.R
        }
        ;
        var ca = function() {
            function a(c) {
                if (this instanceof a)
                    throw new TypeError("Symbol is not a constructor");
                return new da("jscomp_symbol_" + (c || "") + "_" + b++,c)
            }
            var b = 0;
            return a
        }();
        function ea() {
            u();
            var a = t.Symbol.iterator;
            a || (a = t.Symbol.iterator = t.Symbol("Symbol.iterator"));
            "function" != typeof Array.prototype[a] && ba(Array.prototype, a, {
                configurable: !0,
                writable: !0,
                value: function() {
                    return fa(aa(this))
                }
            });
            ea = function() {}
        }
        function fa(a) {
            ea();
            var b = {
                next: a
            };
            b[t.Symbol.iterator] = function() {
                return this
            }
            ;
            return b
        }
        function ha(a) {
            var b = "undefined" != typeof Symbol && Symbol.iterator && a[Symbol.iterator];
            return b ? b.call(a) : {
                next: aa(a)
            }
        }
        function oa(a) {
            for (var b, c = []; !(b = a.next()).done; )
                c.push(b.value);
            return c
        }
        var pa = "function" == typeof Object.create ? Object.create : function(a) {
            function b() {}
            b.prototype = a;
            return new b
        }
        , qa;
        if ("function" == typeof Object.setPrototypeOf)
            qa = Object.setPrototypeOf;
        else {
            var ra;
            a: {
                var sa = {
                    a: !0
                }
                  , ta = {};
                try {
                    ta.__proto__ = sa;
                    ra = ta.a;
                    break a
                } catch (a) {}
                ra = !1
            }
            qa = ra ? function(a, b) {
                a.__proto__ = b;
                if (a.__proto__ !== b)
                    throw new TypeError(a + " is not extensible");
                return a
            }
            : null
        }
        var ua = qa
          , va = /log|development/i.test(location.hash);
        function wa(a, b, c, d, e) {
            var f = e.executorsAllowed.includes(8);
            return {
                execute: function(a, d) {
                    if (f) {
                        var e = a[d + 1]
                          , g = b.getNode(e);
                        if (g) {
                            var h = g.transferControlToOffscreen()
                              , k = {};
                            c.messageToWorker((k[12] = 9,
                            k[13] = [g._index_],
                            k[38] = h,
                            k), [h])
                        } else
                            console.error("'OFFSCREEN_CANVAS_INSTANCE': getNode(" + e + ") is null.")
                    }
                    return d + 2
                },
                print: function(a, b, c) {
                    return {
                        type: "OFFSCREEN_CANVAS_INSTANCE",
                        target: c,
                        allowedExecution: f
                    }
                }
            }
        }
        var ya = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
          , za = {
            0: "ATTRIBUTES",
            1: "CHARACTER_DATA",
            2: "CHILD_LIST",
            3: "PROPERTIES",
            4: "EVENT_SUBSCRIPTION",
            5: "GET_BOUNDING_CLIENT_RECT",
            6: "LONG_TASK_START",
            7: "LONG_TASK_END",
            8: "OFFSCREEN_CANVAS_INSTANCE",
            9: "OBJECT_MUTATION",
            10: "OBJECT_CREATION",
            11: "IMAGE_BITMAP_INSTANCE",
            12: "STORAGE"
        }
          , Aa = new Map;
        function w(a) {
            return a && "value"in a
        }
        function Ba(a, b) {
            w(b) && null === b.oninput && (b.oninput = function() {
                return Ca(a, b)
            }
            )
        }
        function Da(a, b) {
            w(b) && !Aa.get(b) && ((new MutationObserver(function(b) {
                return b.map(function(b) {
                    return Ca(a, b.target)
                })
            }
            )).observe(b, {
                attributes: !0
            }),
            Aa.set(b, !0))
        }
        function Ca(a, b) {
            var c = {}
              , d = {};
            return a.messageToWorker((d[12] = 4,
            d[40] = (c[7] = b._index_,
            c[21] = b.value,
            c),
            d))
        }
        function Ea(a) {
            return Object.values(a).map(function(a) {
                return [a.identifier, a.screenX, a.screenY, a.clientX, a.clientY, a.pageX, a.pageY, a.target._index_]
            })
        }
        function Fa(a, b, c, d, e) {
            function f(a, d, e, f) {
                if (a === b.baseElement)
                    d ? addEventListener(e, g[f] = h(1)) : removeEventListener(e, g[f]);
                else {
                    var k = null !== a.oninput
                      , l = "change" === e;
                    d ? (l && (k = !0,
                    a.onchange = null),
                    a.addEventListener(e, g[f] = h(a._index_))) : (l && (k = !1),
                    a.removeEventListener(e, g[f]));
                    w(a) && (k || Ba(c, a),
                    Da(c, a))
                }
            }
            function h(a) {
                return function(b) {
                    if (w(b.currentTarget))
                        Ca(c, b.currentTarget);
                    else if ("resize" === b.type) {
                        var d = window
                          , e = d.innerWidth
                          , g = d.innerHeight;
                        if (m[0] === e && m[1] === g)
                            return;
                        d = m = [window.innerWidth, window.innerHeight];
                        var f = {};
                        c.messageToWorker((f[12] = 5,
                        f[40] = d,
                        f))
                    }
                    d = {};
                    f = {};
                    c.messageToWorker((f[12] = 1,
                    f[39] = (d[7] = a,
                    d[25] = b.bubbles,
                    d[26] = b.cancelable,
                    d[27] = b.cancelBubble,
                    d[28] = [b.currentTarget._index_ || 0],
                    d[29] = b.defaultPrevented,
                    d[30] = b.eventPhase,
                    d[31] = b.isTrusted,
                    d[32] = b.returnValue,
                    d[13] = [b.target._index_ || 0],
                    d[33] = b.timeStamp,
                    d[12] = b.type,
                    d[35] = "keyCode"in b ? b.keyCode : void 0,
                    d[60] = "pageX"in b ? b.pageX : void 0,
                    d[61] = "pageY"in b ? b.pageY : void 0,
                    d[65] = "offsetX"in b ? b.offsetX : void 0,
                    d[66] = "offsetY"in b ? b.offsetY : void 0,
                    d[62] = "touches"in b ? Ea(b.touches) : void 0,
                    d[63] = "changedTouches"in b ? Ea(b.changedTouches) : void 0,
                    d),
                    f))
                }
            }
            var g = []
              , l = e.executorsAllowed.includes(4)
              , m = [window.innerWidth, window.innerHeight];
            return {
                execute: function(c, d) {
                    var e = c[d + 3]
                      , g = c[d + 2]
                      , h = d + 4 + 2 * g
                      , k = d + 4 + 2 * (e + g);
                    if (l) {
                        var m = c[d + 1]
                          , n = b.getNode(m);
                        if (n)
                            for (d += 4; d < k; d += 2)
                                f(n, d <= h, a.get(c[d]), c[d + 1]);
                        else
                            console.error("getNode(" + m + ") is null.")
                    }
                    return k
                },
                print: function(b, c, d) {
                    var e = b[c + 2]
                      , f = c + 4 + 2 * e;
                    e = c + 4 + 2 * (b[c + 3] + e);
                    var g = []
                      , h = [];
                    for (c += 4; c < e; c += 2) {
                        var k = c <= f ? h : g;
                        k.push({
                            type: a.get(b[c]),
                            index: b[c + 1]
                        })
                    }
                    return {
                        target: d,
                        allowedExecution: l,
                        removedEventListeners: g,
                        addedEventListeners: h
                    }
                }
            }
        }
        function Ga(a, b, c, d, e) {
            var f = e.executorsAllowed.includes(5);
            return {
                execute: function(a, d) {
                    if (f) {
                        var e = a[d + 1];
                        if (a = b.getNode(e)) {
                            var g = a.getBoundingClientRect();
                            e = {};
                            c.messageToWorker((e[12] = 6,
                            e[13] = [a._index_],
                            e[38] = [g.top, g.right, g.bottom, g.left, g.width, g.height],
                            e))
                        } else
                            console.error("GET_BOUNDING_CLIENT_RECT: getNode(" + e + ") is null.")
                    }
                    return d + 2
                },
                print: function(a, c) {
                    return {
                        type: "GET_BOUNDING_CLIENT_RECT",
                        target: b.getNode(a[c + 1]),
                        allowedExecution: f
                    }
                }
            }
        }
        function Ha(a, b, c, d, e) {
            var f = b.getNode
              , h = e.executorsAllowed.includes(2);
            return {
                execute: function(a, b) {
                    var d = a[b + 4]
                      , e = a[b + 5];
                    if (h) {
                        var g = a[b + 1]
                          , l = f(g);
                        l ? (0 < e && a.slice(b + 6 + d, b + 6 + d + e).forEach(function(a) {
                            var b = f(a);
                            b ? b.remove() : console.error("CHILD_LIST: getNode(" + a + ") is null.")
                        }),
                        0 < d && a.slice(b + 6, b + 6 + d).forEach(function(d) {
                            var e = a[b + 2]
                              , g = f(d);
                            g && (l.insertBefore(g, e && f(e) || null),
                            Ba(c, g),
                            Da(c, g))
                        })) : console.error("CHILD_LIST: getNode(" + g + ") is null.")
                    }
                    return b + 6 + d + e
                },
                print: function(a, b) {
                    var c = f(a[b + 1])
                      , d = a[b + 4]
                      , e = Array.from(a.slice(b + 6 + d, b + 6 + d + a[b + 5])).map(function(a) {
                        return f(a) || a
                    })
                      , g = Array.from(a.slice(b + 6, b + 6 + d)).map(function(a) {
                        return f(a) || a
                    });
                    return {
                        target: c,
                        allowedExecution: h,
                        nextSibling: f(a[b + 2]) || null,
                        previousSibling: f(a[b + 3]) || null,
                        addedNodes: g,
                        removedNodes: e
                    }
                }
            }
        }
        function Ia(a, b, c, d, e) {
            function f(b, c) {
                b = b[c + 4];
                return 0 !== b ? a.get(b - 1) : null
            }
            var h = e.executorsAllowed.includes(0);
            return {
                execute: function(c, d) {
                    if (h) {
                        var g = c[d + 1]
                          , l = b.getNode(g)
                          , k = a.get(c[d + 2]);
                        c = f(c, d);
                        l ? null != k && (e.sanitizer ? e.sanitizer.setAttribute(l, k, c) : null == c ? l.removeAttribute(k) : l.setAttribute(k, c)) : console.error("ATTR_LIST: getNode(" + g + ") is null.")
                    }
                    return d + 5
                },
                print: function(c, d) {
                    var e = b.getNode(c[d + 1])
                      , g = a.get(c[d + 2]);
                    c = f(c, d);
                    return {
                        target: e,
                        allowedExecution: h,
                        attributeName: g,
                        value: c,
                        remove: null == c
                    }
                }
            }
        }
        function Ja(a, b, c, d, e) {
            var f = e.executorsAllowed.includes(1);
            return {
                execute: function(c, d) {
                    if (f) {
                        var e = c[d + 1]
                          , g = b.getNode(e);
                        c = c[d + 2];
                        g ? c && (g.textContent = a.get(c)) : console.error("CHAR_DATA: getNode(" + e + ") is null.")
                    }
                    return d + 3
                },
                print: function(c, d) {
                    return {
                        target: b.getNode(c[d + 1]),
                        allowedExecution: f,
                        value: a.get(c[d + 2])
                    }
                }
            }
        }
        function Ka(a, b, c, d, e) {
            function f(b, c) {
                var d = b[c + 4];
                return 1 === b[c + 3] ? 1 === d : 0 !== d ? a.get(d) : null
            }
            var h = e.executorsAllowed.includes(3);
            return {
                execute: function(c, d) {
                    if (h) {
                        var g = c[d + 1]
                          , l = b.getNode(g)
                          , k = a.get(c[d + 2]);
                        c = f(c, d);
                        l ? k && null != c && (e.sanitizer ? e.sanitizer.setProperty(l, k, String(c)) : l[k] = c) : console.error("PROPERTY: getNode(" + g + ") is null.")
                    }
                    return d + 5
                },
                print: function(c, d) {
                    var e = b.getNode(c[d + 1])
                      , g = a.get(c[d + 2]);
                    c = f(c, d);
                    return {
                        target: e,
                        name: g,
                        value: c,
                        allowedExecution: h
                    }
                }
            }
        }
        function La(a, b, c, d, e) {
            var f = e.executorsAllowed.includes(6), h = 0, g;
            return {
                execute: function(a, b) {
                    f && e.longTask && (6 === a[b] ? (h++,
                    g || e.longTask(new Promise(function(a) {
                        return g = a
                    }
                    ))) : 7 === a[b] && (h--,
                    g && 0 >= h && (g(),
                    g = null,
                    h = 0)));
                    return b + 2
                },
                print: function(a, b) {
                    return {
                        type: za[a[b]],
                        allowedExecution: f
                    }
                },
                get active() {
                    return null !== g
                }
            }
        }
        var Ma = new Float32Array(1)
          , Na = new Uint16Array(Ma.buffer);
        function y(a, b, c, d, e, f) {
            for (var h = [], g = 0; g < c; g++)
                switch (a[b++]) {
                case 1:
                    h.push(a[b++]);
                    break;
                case 2:
                    Na[0] = a[b++];
                    Na[1] = a[b++];
                    h.push(Ma[0]);
                    break;
                case 3:
                    h.push(d.get(a[b++]));
                    break;
                case 4:
                    var l = a[b++]
                      , m = y(a, b, l, d, e, f);
                    h.push(m.args);
                    b = m.offset;
                    break;
                case 5:
                    if (!f)
                        throw Error("objectContext not provided.");
                    h.push(f.get(a[b++]));
                    break;
                case 6:
                    var n = e.getNode(a[b++]);
                    h.push(n.getContext("2d"));
                    break;
                case 7:
                    h.push(e.getNode(a[b++]));
                    break;
                default:
                    throw Error("Cannot deserialize argument.");
                }
            return {
                args: h,
                offset: b
            }
        }
        function $a(a, b, c, d, e) {
            var f = e.executorsAllowed.includes(9);
            return {
                execute: function(c, e) {
                    var g = a.get(c[e + 1])
                      , h = c[e + 2];
                    e = y(c, e + 3, 1, a, b, d);
                    var n = e.offset
                      , k = e.args;
                    e = k[0];
                    c = y(c, n, h, a, b, d);
                    var p = c.offset;
                    c = c.args;
                    f && (ab(e, g) ? e[g] = c[0] : e[g].apply(e, c instanceof Array ? c : oa(ha(c))));
                    return p
                },
                print: function(c, e) {
                    var g = a.get(c[e + 1]);
                    c = y(c, e + 3, 1, a, b, d).args[0];
                    return {
                        type: "OBJECT_MUTATION",
                        target: c,
                        functionName: g,
                        isSetter: ab(c, g),
                        allowedExecution: f
                    }
                }
            }
        }
        function ab(a, b) {
            if (!a)
                throw Error("Property " + b + " does not exist on " + a + ".");
            var c = Object.getOwnPropertyDescriptor(a, b);
            return void 0 !== c ? "set"in c : ab(Object.getPrototypeOf(a), b)
        }
        function bb(a, b, c, d, e) {
            var f = e.executorsAllowed.includes(10);
            if (!d)
                throw Error("objectContext is not defined.");
            return {
                execute: function(c, e) {
                    var g = a.get(c[e + 1])
                      , h = c[e + 2]
                      , n = c[e + 3]
                      , k = y(c, e + 4, 1, a, b, d);
                    e = k.args[0];
                    n = y(c, k.offset, n, a, b, d);
                    c = n.offset;
                    n = n.args;
                    f && "new" !== g && d.store(h, e[g].apply(e, n instanceof Array ? n : oa(ha(n))));
                    return c
                },
                print: function(c, e) {
                    var g = a.get(c[e + 1])
                      , h = c[e + 2]
                      , n = c[e + 3];
                    return {
                        type: "OBJECT_CREATION",
                        target: y(c, e + 4, 1, a, b, d).args[0],
                        functionName: g,
                        objectId: h,
                        argCount: n,
                        allowedExecution: f
                    }
                }
            }
        }
        function cb(a, b, c, d, e) {
            var f = e.executorsAllowed.includes(11);
            return {
                execute: function(a, d) {
                    if (f) {
                        var e = a[d + 1]
                          , g = b.getNode(e);
                        g ? self.createImageBitmap(g).then(function(b) {
                            var e = {};
                            c.messageToWorker((e[12] = 10,
                            e[73] = a[d + 2],
                            e[38] = b,
                            e), [b])
                        }) : console.error("IMAGE_BITMAP_INSTANCE: getNode(" + e + ") is null.")
                    }
                    return d + 3
                },
                print: function(a, c) {
                    return {
                        type: "IMAGE_BITMAP_INSTANCE",
                        target: b.getNode(a[c + 1]),
                        allowedExecution: f,
                        callIndex: a[c + 2]
                    }
                }
            }
        }
        function db(a, b, c, d, e) {
            function f(a, b, c) {
                if (e.sanitizer)
                    e.sanitizer.setStorage(a, b, c);
                else {
                    if (0 === a)
                        var d = window.localStorage;
                    else
                        1 === a && (d = window.sessionStorage);
                    if (d)
                        if (null == b)
                            if (null == c)
                                d.clear();
                            else
                                throw Error("Unexpected storage operation.");
                        else
                            null == c ? d.removeItem(b) : d.setItem(b, c);
                    else
                        console.error('STORAGE: Unexpected location: "' + a + '".')
                }
            }
            function h(a, b) {
                e.sanitizer && 2 === a ? e.sanitizer.getStorage(a, b).then(function(d) {
                    var e = {};
                    d = (e[12] = 11,
                    e[74] = b || "",
                    e[75] = a,
                    e[21] = d,
                    e);
                    c.messageToWorker(d)
                }) : console.error("STORAGE: Sanitizer not found or unsupported location:", a)
            }
            var g = e.executorsAllowed.includes(12);
            return {
                execute: function(b, c) {
                    if (g) {
                        var d = b[c + 1]
                          , e = b[c + 2]
                          , l = b[c + 3]
                          , m = b[c + 4];
                        b = 0 < l ? a.get(l) : null;
                        var Z = 0 < m ? a.get(m) : null;
                        1 === d ? h(e, b) : 2 === d && f(e, b, Z)
                    }
                    return c + 5
                },
                print: function(b, c) {
                    var d = b[c + 1]
                      , e = b[c + 2]
                      , f = b[c + 3];
                    b = b[c + 4];
                    f = 0 < f ? a.get(f) : null;
                    b = 0 < b ? a.get(b) : null;
                    return {
                        type: "STORAGE",
                        operation: d,
                        location: e,
                        key: f,
                        value: b,
                        allowedExecution: g
                    }
                }
            }
        }
        function eb(a, b, c, d, e) {
            var f = this;
            this.mutationQueue = [];
            this.pendingMutations = !1;
            this.syncFlush = function() {
                f.mutationQueue.forEach(function(a) {
                    for (var b = 0, c = a.length; b < c; ) {
                        var d = a[b]
                          , e = f.executors[d];
                        va && console.log(za[d], e.print(a, b));
                        b = e.execute(a, b)
                    }
                });
                f.mutationQueue = [];
                f.pendingMutations = !1
            }
            ;
            this.stringContext = a;
            this.nodeContext = b;
            this.sanitizer = d.sanitizer;
            this.mutationPumpFunction = d.mutationPump;
            a = [a, b, c, e, d];
            var h = La.apply(null, a);
            b = {};
            this.executors = (b[2] = Ha.apply(null, a),
            b[0] = Ia.apply(null, a),
            b[1] = Ja.apply(null, a),
            b[3] = Ka.apply(null, a),
            b[4] = Fa.apply(null, a),
            b[5] = Ga.apply(null, a),
            b[6] = h,
            b[7] = h,
            b[8] = wa.apply(null, a),
            b[9] = $a.apply(null, a),
            b[10] = bb.apply(null, a),
            b[11] = cb.apply(null, a),
            b[12] = db.apply(null, a),
            b)
        }
        eb.prototype.mutate = function(a, b, c, d) {
            this.stringContext.storeValues(c);
            this.nodeContext.createNodes(b, this.sanitizer);
            this.mutationQueue = this.mutationQueue.concat(d);
            this.pendingMutations || (this.pendingMutations = !0,
            this.mutationPumpFunction(this.syncFlush, a))
        }
        ;
        function fb(a, b) {
            var c = this;
            this.createNodes = function(a, b) {
                var d = new Uint16Array(a)
                  , e = d.length;
                for (a = 0; a < e; a += 5) {
                    if (3 === d[a + 1])
                        var g = document.createTextNode(c.stringContext.get(d[a + 3]));
                    else if (8 === d[a + 1])
                        g = document.createComment(c.stringContext.get(d[a + 3]));
                    else if (11 === d[a + 1])
                        g = document.createDocumentFragment();
                    else {
                        var l = c.stringContext.get(d[a + 2]);
                        g = 0 !== d[a + 4] ? document.createElementNS(c.stringContext.get(d[a + 4]), l) : document.createElement(l);
                        if (b && !b.sanitize(g))
                            continue
                    }
                    c.storeNode(g, d[a])
                }
            }
            ;
            this.getNode = function(a) {
                return (a = c.nodes.get(a)) && "BODY" === a.nodeName ? c.baseElement : a
            }
            ;
            this.storeNodes = function(a) {
                c.storeNode(a, ++c.count);
                a.childNodes.forEach(function(a) {
                    return c.storeNodes(a)
                })
            }
            ;
            this.count = 2;
            this.stringContext = a;
            this.nodes = new Map([[1, b], [2, b]]);
            this.baseElement = b;
            b._index_ = 2;
            b.childNodes.forEach(function(a) {
                return c.storeNodes(a)
            })
        }
        fb.prototype.storeNode = function(a, b) {
            a._index_ = b;
            this.nodes.set(b, a)
        }
        ;
        function gb() {
            this.strings = []
        }
        gb.prototype.get = function(a) {
            return this.strings[a] || ""
        }
        ;
        gb.prototype.store = function(a) {
            this.strings.push(a)
        }
        ;
        gb.prototype.storeValues = function(a) {
            var b = this;
            a.forEach(function(a) {
                return b.store(a)
            })
        }
        ;
        function hb(a) {
            return Object.assign({}, {
                mutationPump: requestAnimationFrame.bind(null),
                executorsAllowed: ya
            }, a)
        }
        var ib = [8, 3];
        function jb(a, b, c, d) {
            var e = [].slice.call(a.childNodes).filter(c)
              , f = {}
              , h = (f[7] = a._index_,
            f[11] = 0,
            f[0] = a.nodeType,
            f[1] = b(a.localName || a.nodeName),
            f[4] = e.map(function(a) {
                return jb(a, b, c, d)
            }),
            f[2] = [].map.call(a.attributes || [], function(a) {
                return [b(a.namespaceURI || "null"), b(a.name), b(a.value)]
            }),
            f);
            null != a.namespaceURI && (h[6] = b(a.namespaceURI));
            ib.includes(a.nodeType) && null !== a.textContent && (h[5] = b(a.textContent));
            Ba(d, a);
            Da(d, a);
            return h
        }
        function kb(a, b, c) {
            function d(a) {
                if (f.has(a))
                    return f.get(a);
                var b = e.length;
                f.set(a, b);
                e.push(a);
                return b
            }
            b = b.hydrateFilter || function() {
                return !0
            }
            ;
            var e = []
              , f = new Map
              , h = jb(a, d, b, c);
            return {
                skeleton: h,
                strings: e
            }
        }
        function lb(a, b, c) {
            return jb(a, function(a) {
                return a
            }, b.hydrateFilter || function() {
                return !0
            }
            , c)
        }
        function mb(a, b) {
            return null != b && a.getNode(b[0]) || b
        }
        function nb(a) {
            var b = {
                nodeType: a[0],
                name: a[1],
                attributes: null,
                childNodes: null
            }
              , c = a[2];
            c && (b.attributes = c.map(function(a) {
                return {
                    name: a[1],
                    value: a[2]
                }
            }));
            var d = a[4];
            d && (b.childNodes = d.map(nb));
            return b
        }
        function ob(a, b) {
            function c(b) {
                return "number" === typeof b || "boolean" === typeof b ? void 0 !== b ? b : null : void 0 !== b && null !== b ? mb(a, b) : null
            }
            return {
                type: b[12],
                bubbles: c(b[25]),
                cancelable: c(b[26]),
                cancelBubble: c(b[27]),
                defaultPrevented: c(b[29]),
                eventPhase: c(b[30]),
                isTrusted: c(b[31]),
                returnValue: c(b[32]),
                currentTarget: c(b[28]),
                target: c(b[13]),
                scoped: c(b[34]),
                keyCode: c(b[35])
            }
        }
        function qb(a, b) {
            if (1 == b[12])
                a = {
                    type: "EVENT",
                    event: ob(a, b[39])
                };
            else if (4 == b[12]) {
                b = b[40];
                var c = b[7];
                a = {
                    type: "SYNC",
                    sync: {
                        target: a.getNode(c) || c,
                        value: b[21]
                    }
                }
            } else
                a = 6 === b[12] ? {
                    type: "GET_BOUNDING_CLIENT_RECT",
                    target: mb(a, b[13])
                } : "Unrecognized MessageToWorker type: " + b[12];
            return a
        }
        function rb(a, b, c, d, e) {
            this.nodeContext = b;
            this.config = e;
            var f = kb(a, e, this);
            b = f.skeleton;
            f = f.strings;
            var h = []
              , g = []
              , l = e.sanitizer ? e.sanitizer.getStorage(0) : window.localStorage
              , m = e.sanitizer ? e.sanitizer.getStorage(1) : window.sessionStorage;
            for (k in a.style)
                h.push(k);
            for (var n in a)
                n.startsWith("on") && g.push(n);
            var k = "'use strict';(function(){" + c + "self['window']=self;var workerDOM=WorkerThread.workerDOM;WorkerThread.hydrate(workerDOM.document," + JSON.stringify(f) + "," + JSON.stringify(b) + "," + JSON.stringify(h) + "," + JSON.stringify(g) + ",[" + window.innerWidth + "," + window.innerHeight + "]," + JSON.stringify(l) + "," + JSON.stringify(m) + ");workerDOM.document[59](this);Object.keys(workerDOM).forEach(key => self[key]=workerDOM[key]);}).call(self);" + d + "//# sourceURL=" + encodeURI(e.authorURL);
            this[55] = new Worker(URL.createObjectURL(new Blob([k])));
            va && console.info("debug", "hydratedNode", nb(lb(a, e, this)));
            if (e.onCreateWorker)
                e.onCreateWorker(a, f, b, h)
        }
        rb.prototype.messageToWorker = function(a, b) {
            va && console.info("debug", "messageToWorker", qb(this.nodeContext, a));
            if (this.config.onSendMessage)
                this.config.onSendMessage(a);
            this.worker.postMessage(a, b || [])
        }
        ;
        t.Object.defineProperties(rb.prototype, {
            worker: {
                configurable: !0,
                enumerable: !0,
                get: function() {
                    return this[55]
                }
            }
        });
        function sb() {
            this.objects = new Map
        }
        sb.prototype.store = function(a, b) {
            this.objects.set(a, b)
        }
        ;
        sb.prototype.get = function(a) {
            var b = this.objects.get(a);
            if (b)
                return b;
            throw Error("Object with id (" + a + ") does not exist.");
        }
        ;
        var tb = [3, 2];
        function ub(a, b, c) {
            var d = new gb
              , e = new sb
              , f = new fb(d,b)
              , h = hb(c);
            return a.then(function(a) {
                a = ha(a);
                var g = a.next().value
                  , m = a.next().value;
                if (g && m && c.authorURL) {
                    a = new rb(b,f,g,m,h);
                    var n = new eb(d,f,a,h,e);
                    a.worker.onmessage = function(a) {
                        var b = a.data;
                        if (tb.includes(b[12]) && (n.mutate(b[54], b[37], b[41], new Uint16Array(b[36])),
                        c.onReceiveMessage))
                            c.onReceiveMessage(a)
                    }
                    ;
                    return a.worker
                }
                return null
            })
        }
        function vb(a) {
            if (null !== a.parentNode) {
                var b = (a.parentNode.localName || a.parentNode.nodeName).toLowerCase();
                return !/amp-/.test(b) || "amp-script" === b
            }
            return !0
        }
        function wb(a, b, c) {
            c.hydrateFilter = vb;
            return ub(b, a, c)
        }
        ;var xb = Object.prototype.hasOwnProperty;
        function yb() {
            var a, b = Object.create(null);
            a && Object.assign(b, a);
            return b
        }
        function z(a) {
            return a || {}
        }
        ;function zb(a) {
            var b = ["\u26a14email", "amp4email"]
              , c = a.documentElement
              , d = b.some(function(a) {
                return c.hasAttribute(a)
            });
            return d
        }
        ;function Ab(a, b) {
            b = void 0 === b ? "" : b;
            try {
                return decodeURIComponent(a)
            } catch (c) {
                return b
            }
        }
        ;var Bb = /(?:^[#?]?|&)([^=&]+)(?:=([^&]*))?/g;
        function Cb(a) {
            var b = Object.create(null);
            if (!a)
                return b;
            for (var c; c = Bb.exec(a); ) {
                var d = Ab(c[1], c[1])
                  , e = c[2] ? Ab(c[2].replace(/\+/g, " "), c[2]) : "";
                b[d] = e
            }
            return b
        }
        ;var Db = "";
        var A = self.AMP_CONFIG || {}
          , Eb = {
            thirdParty: A.thirdPartyUrl || "https://3p.ampproject.net",
            thirdPartyFrameHost: A.thirdPartyFrameHost || "ampproject.net",
            thirdPartyFrameRegex: ("string" == typeof A.thirdPartyFrameRegex ? new RegExp(A.thirdPartyFrameRegex) : A.thirdPartyFrameRegex) || /^d-\d+\.ampproject\.net$/,
            cdn: A.cdnUrl || "https://cdn.ampproject.org",
            cdnProxyRegex: ("string" == typeof A.cdnProxyRegex ? new RegExp(A.cdnProxyRegex) : A.cdnProxyRegex) || /^https:\/\/([a-zA-Z0-9_-]+\.)?cdn\.ampproject\.org$/,
            localhostRegex: /^https?:\/\/localhost(:\d+)?$/,
            errorReporting: A.errorReportingUrl || "https://amp-error-reporting.appspot.com/r",
            localDev: A.localDev || !1,
            trustedViewerHosts: [/(^|\.)google\.(com?|[a-z]{2}|com?\.[a-z]{2}|cat)$/, /(^|\.)gmail\.(com|dev)$/]
        };
        self.__AMP_LOG = self.__AMP_LOG || {
            user: null,
            dev: null,
            userForEmbed: null
        };
        var Fb = self.__AMP_LOG;
        function E() {
            if (!Fb.user)
                throw Error("failed to call initLogConstructor");
            return Fb.user
        }
        function G() {
            if (Fb.dev)
                return Fb.dev;
            throw Error("failed to call initLogConstructor");
        }
        function H(a, b, c, d, e, f) {
            return E().assert(a, b, c, d, e, f, void 0, void 0, void 0, void 0, void 0)
        }
        ;function Gb() {
            var a = 100;
            this.T = a;
            this.D = this.G = 0;
            this.w = Object.create(null)
        }
        Gb.prototype.has = function(a) {
            return !!this.w[a]
        }
        ;
        Gb.prototype.get = function(a) {
            var b = this.w[a];
            if (b)
                return b.access = ++this.D,
                b.payload
        }
        ;
        Gb.prototype.put = function(a, b) {
            this.has(a) || this.G++;
            this.w[a] = {
                payload: b,
                access: this.D
            };
            if (!(this.G <= this.T)) {
                G().warn("lru-cache", "Trimming LRU cache");
                a = this.w;
                var c = this.D + 1, d;
                for (d in a) {
                    var e = a[d].access;
                    if (e < c) {
                        c = e;
                        var f = d
                    }
                }
                void 0 !== f && (delete a[f],
                this.G--)
            }
        }
        ;
        function I(a, b) {
            return b.length > a.length ? !1 : 0 == a.lastIndexOf(b, 0)
        }
        ;var Hb = z({
            c: !0,
            v: !0,
            a: !0,
            ad: !0,
            action: !0
        }), Ib, Jb, Kb = /[?&]amp_js[^&]*/, Lb = /[?&]amp_gsa[^&]*/, Mb = /[?&]amp_r[^&]*/, Nb = /[?&]amp_kit[^&]*/, Ob = /[?&]usqp[^&]*/;
        function J(a) {
            var b;
            Ib || (Ib = self.document.createElement("a"),
            Jb = self.__AMP_URL_CACHE || (self.__AMP_URL_CACHE = new Gb));
            var c = b ? null : Jb
              , d = Ib;
            if (c && c.has(a))
                a = c.get(a);
            else {
                d.href = a;
                d.protocol || (d.href = d.href);
                var e = {
                    href: d.href,
                    protocol: d.protocol,
                    host: d.host,
                    hostname: d.hostname,
                    port: "0" == d.port ? "" : d.port,
                    pathname: d.pathname,
                    search: d.search,
                    hash: d.hash,
                    origin: null
                };
                "/" !== e.pathname[0] && (e.pathname = "/" + e.pathname);
                if ("http:" == e.protocol && 80 == e.port || "https:" == e.protocol && 443 == e.port)
                    e.port = "",
                    e.host = e.hostname;
                e.origin = d.origin && "null" != d.origin ? d.origin : "data:" != e.protocol && e.host ? e.protocol + "//" + e.host : e.href;
                c && c.put(a, e);
                a = e
            }
            return a
        }
        function Pb(a) {
            "string" == typeof a && (a = J(a));
            return Eb.cdnProxyRegex.test(a.origin)
        }
        function Qb(a) {
            "string" == typeof a && (a = J(a));
            if (!Pb(a))
                return a.href;
            var b = a.pathname.split("/");
            H(Hb[b[1]], "Unknown path prefix in url %s", a.href);
            var c = b[2]
              , d = "s" == c ? "https://" + decodeURIComponent(b[3]) : "http://" + decodeURIComponent(c);
            H(0 < d.indexOf("."), "Expected a . in origin %s", d);
            b.splice(1, "s" == c ? 3 : 2);
            b = d + b.join("/");
            d = (d = a.search) && "?" != d ? (d = d.replace(Kb, "").replace(Lb, "").replace(Mb, "").replace(Nb, "").replace(Ob, "").replace(/^[?&]/, "")) ? "?" + d : "" : "";
            return b + d + (a.hash || "")
        }
        function Rb(a, b) {
            "string" == typeof b && (b = J(b));
            if ("function" == typeof URL)
                var c = (new URL(a,b.href)).toString();
            else {
                c = a;
                var d = b;
                "string" == typeof d && (d = J(d));
                c = c.replace(/\\/g, "/");
                var e = J(c);
                c = I(c.toLowerCase(), e.protocol) ? e.href : I(c, "//") ? d.protocol + c : I(c, "/") ? d.origin + c : d.origin + d.pathname.replace(/\/[^/]*$/, "/") + c
            }
            return c
        }
        function Sb(a) {
            var b = J(a)
              , c = Cb(b.search);
            H(!("__amp_source_origin"in c), "Source origin is not allowed in %s", a)
        }
        ;var dc = /(\S+)(?:\s+(?:(-?\d+(?:\.\d+)?)([a-zA-Z]*)))?\s*(?:,|$)/g;
        function ec(a) {
            for (var b = [], c; c = dc.exec(a); ) {
                var d = c[1]
                  , e = void 0
                  , f = void 0;
                if (c[2]) {
                    var h = c[3].toLowerCase();
                    if ("w" == h)
                        e = parseInt(c[2], 10);
                    else if ("x" == h)
                        f = parseFloat(c[2]);
                    else
                        continue
                } else
                    f = 1;
                b.push({
                    url: d,
                    width: e,
                    dpr: f
                })
            }
            return new fc(b)
        }
        function fc(a) {
            H(0 < a.length, "Srcset must have at least one source");
            this.h = a;
            for (var b = !1, c = !1, d = 0; d < a.length; d++) {
                var e = a[d];
                b = b || !!e.width;
                c = c || !!e.dpr
            }
            H(!!(b ^ c), "Srcset must have width or dpr sources, but not both");
            a.sort(b ? gc : hc);
            this.O = b
        }
        fc.prototype.select = function(a, b) {
            if (this.O) {
                b *= a;
                a = this.h;
                for (var c = 0, d = Infinity, e = Infinity, f = 0; f < a.length; f++) {
                    var h = a[f].width
                      , g = Math.abs(h - b);
                    if (g <= 1.1 * d || 1.2 < b / e)
                        c = f,
                        d = g,
                        e = h;
                    else
                        break
                }
                b = c
            } else {
                a = this.h;
                c = 0;
                d = Infinity;
                for (e = 0; e < a.length; e++)
                    if (f = Math.abs(a[e].dpr - b),
                    f <= d)
                        c = e,
                        d = f;
                    else
                        break;
                b = c
            }
            return this.h[b].url
        }
        ;
        fc.prototype.getUrls = function() {
            return this.h.map(function(a) {
                return a.url
            })
        }
        ;
        fc.prototype.stringify = function(a) {
            for (var b = [], c = this.h, d = 0; d < c.length; d++) {
                var e = c[d]
                  , f = e.url;
                a && (f = a(f));
                f = this.O ? f + (" " + e.width + "w") : f + (" " + e.dpr + "x");
                b.push(f)
            }
            return b.join(", ")
        }
        ;
        function gc(a, b) {
            H(a.width != b.width, "Duplicate width: %s", a.width);
            return a.width - b.width
        }
        function hc(a, b) {
            H(a.dpr != b.dpr, "Duplicate dpr: %s", a.dpr);
            return a.dpr - b.dpr
        }
        ;function ic(a, b, c) {
            return jc(b) ? kc(a, b, c) : c
        }
        function jc(a) {
            return "src" == a || "href" == a || "xlink:href" == a || "srcset" == a
        }
        function kc(a, b, c) {
            var d = self.location;
            Sb(c);
            var e = Pb(d)
              , f = J(Qb(d));
            if ("href" == b && !I(c, "#"))
                return Rb(c, f);
            if ("src" == b)
                return "amp-img" == a ? lc(c, f, e) : Rb(c, f);
            if ("srcset" == b) {
                try {
                    var h = ec(c)
                } catch (g) {
                    return E().error("URL-REWRITE", "Failed to parse srcset: ", g),
                    c
                }
                return h.stringify(function(a) {
                    return lc(a, f, e)
                })
            }
            return c
        }
        function lc(a, b, c) {
            a = J(Rb(a, b));
            return "data:" == a.protocol || Pb(a) || !c ? a.href : Eb.cdn + "/i/" + ("https:" == a.protocol ? "s/" : "") + encodeURIComponent(a.host) + a.pathname + (a.search || "") + (a.hash || "")
        }
        ;var mc = {
            "AMP-IMG": ["src", "srcset", "layout", "width", "height"]
        };
        function nc(a, b) {
            var c = I(a.tagName, "AMP-")
              , d = a.hasAttribute("i-amphtml-binding");
            if (!d && mc[a.tagName])
                a.setAttribute("i-amphtml-ignore", "");
            else if (d || c)
                a.hasAttribute("i-amphtml-key") || a.setAttribute("i-amphtml-key", b())
        }
        var oc = {
            applet: !0,
            audio: !0,
            base: !0,
            embed: !0,
            frame: !0,
            frameset: !0,
            iframe: !0,
            img: !0,
            link: !0,
            meta: !0,
            object: !0,
            style: !0,
            video: !0
        }
          , pc = {
            "amp-accordion": !0,
            "amp-anim": !0,
            "amp-bind-macro": !0,
            "amp-carousel": !0,
            "amp-fit-text": !0,
            "amp-img": !0,
            "amp-layout": !0,
            "amp-selector": !0,
            "amp-sidebar": !0,
            "amp-timeago": !0
        }
          , qc = "amp-fx fallback heights layout min-font-size max-font-size on option placeholder submitting submit-success submit-error validation-for verify-error visible-when-invalid href style text subscriptions-action subscriptions-actions subscriptions-decorate subscriptions-dialog subscriptions-display subscriptions-section subscriptions-service amp-nested-submenu amp-nested-submenu-open amp-nested-submenu-close itemprop".split(" ")
          , rc = {
            a: ["rel", "target"],
            div: ["template"],
            form: ["action-xhr", "verify-xhr", "custom-validation-reporting", "target"],
            input: ["mask-output"],
            template: ["type"],
            textarea: ["autoexpand"]
        }
          , sc = ["_top", "_blank"]
          , tc = /^(?:\w+script|data|blob):/i
          , uc = /^(?:blob):/i
          , vc = /[\u0000-\u0020\u00A0\u1680\u180E\u2000-\u2029\u205f\u3000]/g
          , wc = Object.freeze(z({
            input: {
                type: /(?:image|button)/i
            }
        }))
          , xc = Object.freeze(z({
            input: {
                type: /(?:button|file|image|password)/i
            }
        }))
          , yc = Object.freeze("form formaction formmethod formtarget formnovalidate formenctype".split(" "))
          , zc = Object.freeze(z({
            input: yc,
            textarea: yc,
            select: yc
        }))
          , Ac = Object.freeze(z({
            "amp-anim": ["controls"],
            form: ["name"]
        }))
          , Bc = /!important|position\s*:\s*fixed|position\s*:\s*sticky/i;
        function Cc(a, b, c, d) {
            var e = !0;
            e = void 0 === e ? !1 : e;
            var f = c ? c.replace(vc, "") : "";
            if (!e) {
                if (I(b, "on") && "on" != b)
                    return !1;
                var h = f.toLowerCase();
                if (0 <= h.indexOf("<script") || 0 <= h.indexOf("\x3c/script") || tc.test(f))
                    return !1
            }
            if (uc.test(f))
                return !1;
            if ("style" == b)
                return !Bc.test(c);
            if ("class" == b && c && /(^|\W)i-amphtml-/i.test(c) || jc(b) && /__amp_source_origin/.test(c))
                return !1;
            var g = zb(d)
              , l = Object.assign(yb(), zc, g ? Ac : {})[a];
            if (l && -1 != l.indexOf(b))
                return !1;
            var m = Object.assign(yb(), wc, g ? xc : {})[a];
            if (m) {
                var n = m[b];
                if (n && -1 != c.search(n))
                    return !1
            }
            return !0
        }
        ;function Dc() {
            var a, b;
            this.promise = new Promise(function(c, d) {
                a = c;
                b = d
            }
            );
            this.resolve = a;
            this.reject = b
        }
        ;/*
 https://mths.be/cssescape v1.5.1 by @mathias | MIT license */
        function Ec(a, b, c) {
            if (b(a))
                c();
            else {
                var d = a.ownerDocument.defaultView;
                if (d.MutationObserver) {
                    var e = new d.MutationObserver(function() {
                        b(a) && (e.disconnect(),
                        c())
                    }
                    );
                    e.observe(a, {
                        childList: !0
                    })
                } else
                    var f = d.setInterval(function() {
                        b(a) && (d.clearInterval(f),
                        c())
                    }, 5)
            }
        }
        function Fc(a, b) {
            Ec(a.documentElement, function() {
                return !!a.body
            }, b)
        }
        function Gc(a) {
            return new Promise(function(b) {
                return Fc(a, b)
            }
            )
        }
        function Hc(a, b) {
            for (var c; a && a !== c; a = a.parentElement)
                if (b(a))
                    return a;
            return null
        }
        function Ic(a) {
            return a.closest ? a.closest("[data-ampdevmode]") : Hc(a, function(a) {
                var b = a.matches || a.webkitMatchesSelector || a.mozMatchesSelector || a.msMatchesSelector || a.oMatchesSelector;
                return b ? b.call(a, "[data-ampdevmode]") : !1
            })
        }
        ;var K = Object.freeze || function(a) {
            return a
        }
          , Jc = K("a abbr acronym address area article aside audio b bdi bdo big blink blockquote body br button canvas caption center cite code col colgroup content data datalist dd decorator del details dfn dir div dl dt element em fieldset figcaption figure font footer form h1 h2 h3 h4 h5 h6 head header hgroup hr html i img input ins kbd label legend li main map mark marquee menu menuitem meter nav nobr ol optgroup option output p pre progress q rp rt ruby s samp section select shadow small source spacer span strike strong style sub summary sup table tbody td template textarea tfoot th thead time tr track tt u ul var video wbr".split(" "))
          , Kc = K("svg a altglyph altglyphdef altglyphitem animatecolor animatemotion animatetransform audio canvas circle clippath defs desc ellipse filter font g glyph glyphref hkern image line lineargradient marker mask metadata mpath path pattern polygon polyline radialgradient rect stop style switch symbol text textpath title tref tspan video view vkern".split(" "))
          , Lc = K("feBlend feColorMatrix feComponentTransfer feComposite feConvolveMatrix feDiffuseLighting feDisplacementMap feDistantLight feFlood feFuncA feFuncB feFuncG feFuncR feGaussianBlur feMerge feMergeNode feMorphology feOffset fePointLight feSpecularLighting feSpotLight feTile feTurbulence".split(" "))
          , Mc = K("math menclose merror mfenced mfrac mglyph mi mlabeledtr mmultiscripts mn mo mover mpadded mphantom mroot mrow ms mspace msqrt mstyle msub msup msubsup mtable mtd mtext mtr munder munderover".split(" "))
          , Nc = K(["#text"])
          , Oc = Object.freeze || function(a) {
            return a
        }
          , Pc = Oc("accept action align alt autocomplete background bgcolor border cellpadding cellspacing checked cite class clear color cols colspan controls coords crossorigin datetime default dir disabled download enctype face for headers height hidden high href hreflang id integrity ismap label lang list loop low max maxlength media method min minlength multiple name noshade novalidate nowrap open optimum pattern placeholder poster preload pubdate radiogroup readonly rel required rev reversed role rows rowspan spellcheck scope selected shape size sizes span srclang start src srcset step style summary tabindex title type usemap valign value width xmlns".split(" "))
          , Qc = Oc("accent-height accumulate additive alignment-baseline ascent attributename attributetype azimuth basefrequency baseline-shift begin bias by class clip clip-path clip-rule color color-interpolation color-interpolation-filters color-profile color-rendering cx cy d dx dy diffuseconstant direction display divisor dur edgemode elevation end fill fill-opacity fill-rule filter filterunits flood-color flood-opacity font-family font-size font-size-adjust font-stretch font-style font-variant font-weight fx fy g1 g2 glyph-name glyphref gradientunits gradienttransform height href id image-rendering in in2 k k1 k2 k3 k4 kerning keypoints keysplines keytimes lang lengthadjust letter-spacing kernelmatrix kernelunitlength lighting-color local marker-end marker-mid marker-start markerheight markerunits markerwidth maskcontentunits maskunits max mask media method mode min name numoctaves offset operator opacity order orient orientation origin overflow paint-order path pathlength patterncontentunits patterntransform patternunits points preservealpha preserveaspectratio primitiveunits r rx ry radius refx refy repeatcount repeatdur restart result rotate scale seed shape-rendering specularconstant specularexponent spreadmethod stddeviation stitchtiles stop-color stop-opacity stroke-dasharray stroke-dashoffset stroke-linecap stroke-linejoin stroke-miterlimit stroke-opacity stroke stroke-width style surfacescale tabindex targetx targety transform text-anchor text-decoration text-rendering textlength type u1 u2 unicode values viewbox visibility version vert-adv-y vert-origin-x vert-origin-y width word-spacing wrap writing-mode xchannelselector ychannelselector x x1 x2 xmlns y y1 y2 z zoomandpan".split(" "))
          , Rc = Oc("accent accentunder align bevelled close columnsalign columnlines columnspan denomalign depth dir display displaystyle encoding fence frame height href id largeop length linethickness lspace lquote mathbackground mathcolor mathsize mathvariant maxsize minsize movablelimits notation numalign open rowalign rowlines rowspacing rowspan rspace rquote scriptlevel scriptminsize scriptsizemultiplier selection separator separators stretchy subscriptshift supscriptshift symmetric voffset width xmlns".split(" "))
          , Sc = Oc(["xlink:href", "xml:id", "xlink:title", "xml:space", "xmlns:xlink"])
          , Tc = Object.hasOwnProperty
          , Uc = Object.setPrototypeOf
          , Vc = ("undefined" !== typeof Reflect && Reflect).apply;
        Vc || (Vc = function(a, b, c) {
            return a.apply(b, c)
        }
        );
        function L(a, b) {
            Uc && Uc(a, null);
            for (var c = b.length; c--; ) {
                var d = b[c];
                if ("string" === typeof d) {
                    var e = d.toLowerCase();
                    e !== d && (Object.isFrozen(b) || (b[c] = e),
                    d = e)
                }
                a[d] = !0
            }
            return a
        }
        function Wc(a) {
            var b = {}
              , c = void 0;
            for (c in a)
                Vc(Tc, a, [c]) && (b[c] = a[c]);
            return b
        }
        var M = Object.seal || function(a) {
            return a
        }
          , Xc = M(/\{\{[\s\S]*|[\s\S]*\}\}/gm)
          , Yc = M(/<%[\s\S]*|[\s\S]*%>/gm)
          , Zc = M(/^data-[\-\w.\u00B7-\uFFFF]/)
          , $c = M(/^aria-[\-\w]+$/)
          , ad = M(/^(?:(?:(?:f|ht)tps?|mailto|tel|callto|cid|xmpp):|[^a-z]|[a-z+.\-]+(?:[^a-z+.\-:]|$))/i)
          , bd = M(/^(?:\w+script|data):/i)
          , cd = M(/[\u0000-\u0020\u00A0\u1680\u180E\u2000-\u2029\u205f\u3000]/g);
        u();
        u();
        ea();
        var Q = "function" === typeof Symbol && "symbol" === typeof Symbol.iterator ? function(a) {
            return typeof a
        }
        : function(a) {
            u();
            u();
            u();
            return a && "function" === typeof Symbol && a.constructor === Symbol && a !== Symbol.prototype ? "symbol" : typeof a
        }
        ;
        function R(a) {
            if (Array.isArray(a)) {
                for (var b = 0, c = Array(a.length); b < a.length; b++)
                    c[b] = a[b];
                return c
            }
            return Array.from(a)
        }
        var dd = ("undefined" !== typeof Reflect && Reflect).apply
          , ed = Array.prototype.slice
          , fd = Object.freeze;
        dd || (dd = function(a, b, c) {
            return a.apply(b, c)
        }
        );
        function gd(a, b) {
            if ("object" !== ("undefined" === typeof a ? "undefined" : Q(a)) || "function" !== typeof a.createPolicy)
                return null;
            var c = null
              , d = "data-tt-policy-suffix";
            b.currentScript && b.currentScript.hasAttribute(d) && (c = b.currentScript.getAttribute(d));
            var e = "dompurify" + (c ? "#" + c : "");
            try {
                return a.createPolicy(e, {
                    createHTML: function(a) {
                        return a
                    }
                })
            } catch (f) {
                return console.warn("TrustedTypes policy " + e + " could not be created."),
                null
            }
        }
        function hd() {
            function a(a) {
                var c, e = void 0, f;
                d("beforeSanitizeAttributes", a, null);
                var D = a.attributes;
                if (D) {
                    var h = {
                        attrName: "",
                        attrValue: "",
                        keepAttr: !0,
                        allowedAttributes: r
                    };
                    for (f = D.length; f--; ) {
                        var g = c = D[f]
                          , p = g.name
                          , n = g.namespaceURI;
                        c = c.value.trim();
                        var m = p.toLowerCase();
                        h.attrName = m;
                        h.attrValue = c;
                        h.keepAttr = !0;
                        d("uponSanitizeAttribute", a, h);
                        c = h.attrValue;
                        if ("name" === m && "IMG" === a.nodeName && D.id)
                            e = D.id,
                            D = dd(ed, D, []),
                            l("id", a),
                            l(p, a),
                            D.indexOf(e) > f && a.setAttribute("id", e.value);
                        else if ("INPUT" !== a.nodeName || "type" !== m || "file" !== c || !h.keepAttr || !r[m] && Pa[m])
                            "id" === p && a.setAttribute(p, ""),
                            l(p, a);
                        else
                            continue;
                        if (h.keepAttr)
                            if (/svg|math/i.test(a.namespaceURI) && (new RegExp("</(" + Object.keys(Ub).join("|") + ")","i")).test(c))
                                l(p, a);
                            else {
                                N && (c = c.replace(Xc, " "),
                                c = c.replace(Yc, " "));
                                var F = a.nodeName.toLowerCase();
                                if (b(F, m, c))
                                    try {
                                        n ? a.setAttributeNS(n, p, c) : a.setAttribute(p, c),
                                        k.removed.pop()
                                    } catch (oe) {}
                            }
                    }
                    d("afterSanitizeAttributes", a, null)
                }
            }
            function b(a, b, c) {
                if (Vb && ("id" === b || "name" === b) && (c in x || c in Ld))
                    return !1;
                if (!Qa || !Zc.test(b))
                    if (!Wb || !$c.test(b))
                        if (!r[b] || Pa[b] || !(Ra[b] || Sa.test(c.replace(cd, "")) || ("src" === b || "xlink:href" === b || "href" === b) && "script" !== a && 0 === c.indexOf("data:") && Md[a] || Xb && !bd.test(c.replace(cd, ""))) && c)
                            return !1;
                return !0
            }
            function c(a) {
                d("beforeSanitizeElements", a, null);
                if (f(a))
                    return m(a),
                    !0;
                var b = a.nodeName.toLowerCase();
                d("uponSanitizeElement", a, {
                    tagName: b,
                    allowedTags: v
                });
                if (("svg" === b || "math" === b) && 0 !== a.querySelectorAll("p, br").length)
                    return m(a),
                    !0;
                if (!v[b] || ia[b]) {
                    if (Ta && !Ub[b] && "function" === typeof a.insertAdjacentHTML)
                        try {
                            var c = a.innerHTML;
                            a.insertAdjacentHTML("AfterEnd", B ? B.createHTML(c) : c)
                        } catch (me) {}
                    m(a);
                    return !0
                }
                if ("noscript" === b && /<\/noscript/i.test(a.innerHTML) || "noembed" === b && /<\/noembed/i.test(a.innerHTML))
                    return m(a),
                    !0;
                !Yb || a.firstElementChild || a.content && a.content.firstElementChild || !/</g.test(a.textContent) || (k.removed.push({
                    element: a.cloneNode()
                }),
                a.innerHTML = a.innerHTML ? a.innerHTML.replace(/</g, "&lt;") : a.textContent.replace(/</g, "&lt;"));
                N && 3 === a.nodeType && (b = a.textContent,
                b = b.replace(Xc, " "),
                b = b.replace(Yc, " "),
                a.textContent !== b && (k.removed.push({
                    element: a.cloneNode()
                }),
                a.textContent = b));
                d("afterSanitizeElements", a, null);
                return !1
            }
            function d(a, b, c) {
                C[a] && C[a].forEach(function(a) {
                    a.call(k, b, c, O)
                })
            }
            function e(a) {
                return "object" === ("undefined" === typeof ja ? "undefined" : Q(ja)) ? a instanceof ja : a && "object" === ("undefined" === typeof a ? "undefined" : Q(a)) && "number" === typeof a.nodeType && "string" === typeof a.nodeName
            }
            function f(a) {
                return a instanceof Nd || a instanceof Od ? !1 : "string" === typeof a.nodeName && "string" === typeof a.textContent && "function" === typeof a.removeChild && a.attributes instanceof Pd && "function" === typeof a.removeAttribute && "function" === typeof a.setAttribute && "string" === typeof a.namespaceURI ? !1 : !0
            }
            function h(a) {
                return Qd.call(a.ownerDocument || a, a, ka.SHOW_ELEMENT | ka.SHOW_COMMENT | ka.SHOW_TEXT, function() {
                    return ka.FILTER_ACCEPT
                }, !1)
            }
            function g(a) {
                var b = void 0
                  , c = void 0;
                if (Ua)
                    a = "<remove></remove>" + a;
                else {
                    var d = a.match(/^[\s]+/);
                    (c = d && d[0]) && (a = a.slice(c.length))
                }
                if (Z)
                    try {
                        b = (new Rd).parseFromString(a, "text/html")
                    } catch (ne) {}
                F && L(ia, ["title"]);
                if (!b || !b.documentElement) {
                    var e = b = Va.createHTMLDocument("");
                    d = e.body;
                    d.parentNode.removeChild(d.parentNode.firstElementChild);
                    d.outerHTML = B ? B.createHTML(a) : a
                }
                a && c && b.body.insertBefore(x.createTextNode(c), b.body.childNodes[0] || null);
                return Sd.call(b, U ? "html" : "body")[0]
            }
            function l(a, b) {
                try {
                    k.removed.push({
                        attribute: b.getAttributeNode(a),
                        from: b
                    })
                } catch (Tb) {
                    k.removed.push({
                        attribute: null,
                        from: b
                    })
                }
                b.removeAttribute(a)
            }
            function m(a) {
                k.removed.push({
                    element: a
                });
                try {
                    a.parentNode.removeChild(a)
                } catch (Oa) {
                    a.outerHTML = Zb
                }
            }
            function n(a) {
                O && O === a || (a && "object" === ("undefined" === typeof a ? "undefined" : Q(a)) || (a = {}),
                v = "ALLOWED_TAGS"in a ? L({}, a.ALLOWED_TAGS) : $b,
                r = "ALLOWED_ATTR"in a ? L({}, a.ALLOWED_ATTR) : ac,
                Ra = "ADD_URI_SAFE_ATTR"in a ? L(Wc(bc), a.ADD_URI_SAFE_ATTR) : bc,
                ia = "FORBID_TAGS"in a ? L({}, a.FORBID_TAGS) : {},
                Pa = "FORBID_ATTR"in a ? L({}, a.FORBID_ATTR) : {},
                P = "USE_PROFILES"in a ? a.USE_PROFILES : !1,
                Wb = !1 !== a.ALLOW_ARIA_ATTR,
                Qa = !1 !== a.ALLOW_DATA_ATTR,
                Xb = a.ALLOW_UNKNOWN_PROTOCOLS || !1,
                Yb = a.SAFE_FOR_JQUERY || !1,
                N = a.SAFE_FOR_TEMPLATES || !1,
                U = a.WHOLE_DOCUMENT || !1,
                V = a.RETURN_DOM || !1,
                Wa = a.RETURN_DOM_FRAGMENT || !1,
                cc = a.RETURN_DOM_IMPORT || !1,
                Xa = a.RETURN_TRUSTED_TYPE || !1,
                Ua = a.FORCE_BODY || !1,
                Vb = !1 !== a.SANITIZE_DOM,
                Ta = !1 !== a.KEEP_CONTENT,
                la = a.IN_PLACE || !1,
                Sa = a.ALLOWED_URI_REGEXP || Sa,
                N && (Qa = !1),
                Wa && (V = !0),
                P && (v = L({}, [].concat(R(Nc))),
                r = [],
                !0 === P.html && (L(v, Jc),
                L(r, Pc)),
                !0 === P.svg && (L(v, Kc),
                L(r, Qc),
                L(r, Sc)),
                !0 === P.svgFilters && (L(v, Lc),
                L(r, Qc),
                L(r, Sc)),
                !0 === P.mathMl && (L(v, Mc),
                L(r, Rc),
                L(r, Sc))),
                a.ADD_TAGS && (v === $b && (v = Wc(v)),
                L(v, a.ADD_TAGS)),
                a.ADD_ATTR && (r === ac && (r = Wc(r)),
                L(r, a.ADD_ATTR)),
                a.ADD_URI_SAFE_ATTR && L(Ra, a.ADD_URI_SAFE_ATTR),
                Ta && (v["#text"] = !0),
                U && L(v, ["html", "head", "body"]),
                v.table && (L(v, ["tbody"]),
                delete ia.tbody),
                fd && fd(a),
                O = a)
            }
            function k(a) {
                return hd(a)
            }
            var p = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "undefined" === typeof window ? null : window;
            k.version = "2.0.7";
            k.removed = [];
            if (!p || !p.document || 9 !== p.document.nodeType)
                return k.isSupported = !1,
                k;
            var xa = p.document
              , Z = !1
              , F = !1
              , x = p.document
              , pb = p.DocumentFragment
              , ja = p.Node
              , ka = p.NodeFilter
              , ma = p.NamedNodeMap
              , Pd = void 0 === ma ? p.NamedNodeMap || p.MozNamedAttrMap : ma
              , Nd = p.Text
              , Od = p.Comment
              , Rd = p.DOMParser;
            ma = p.TrustedTypes;
            if ("function" === typeof p.HTMLTemplateElement) {
                var Ya = x.createElement("template");
                Ya.content && Ya.content.ownerDocument && (x = Ya.content.ownerDocument)
            }
            var B = gd(ma, xa)
              , Zb = B ? B.createHTML("") : ""
              , na = x
              , Va = na.implementation
              , Qd = na.createNodeIterator
              , Sd = na.getElementsByTagName
              , Td = na.createDocumentFragment
              , Ud = xa.importNode
              , C = {};
            k.isSupported = Va && "undefined" !== typeof Va.createHTMLDocument && 9 !== x.documentMode;
            var Sa = ad
              , v = null
              , $b = L({}, [].concat(R(Jc), R(Kc), R(Lc), R(Mc), R(Nc)))
              , r = null
              , ac = L({}, [].concat(R(Pc), R(Qc), R(Rc), R(Sc)))
              , ia = null
              , Pa = null
              , Wb = !0
              , Qa = !0
              , Xb = !1
              , Yb = !1
              , N = !1
              , U = !1
              , Za = !1
              , Ua = !1
              , V = !1
              , Wa = !1
              , cc = !1
              , Xa = !1
              , Vb = !0
              , Ta = !0
              , la = !1
              , P = {}
              , Ub = L({}, "annotation-xml audio colgroup desc foreignobject head iframe math mi mn mo ms mtext noembed noframes plaintext script style svg template thead title video xmp".split(" "))
              , Md = L({}, ["audio", "video", "img", "source", "image"])
              , Ra = null
              , bc = L({}, "alt class for id label name pattern placeholder summary title value style xmlns".split(" "))
              , O = null
              , Ld = x.createElement("form");
            k.isSupported && (function() {
                try {
                    g('<svg><p><textarea><img src="</textarea><img src=x abc=1//">').querySelector("svg img") && (Z = !0)
                } catch (D) {}
            }(),
            function() {
                try {
                    var a = g("<x/><title>&lt;/title&gt;&lt;img&gt;");
                    /<\/title/.test(a.querySelector("title").innerHTML) && (F = !0)
                } catch (Oa) {}
            }());
            var Vd = function Tb(b) {
                var e = void 0
                  , f = h(b);
                for (d("beforeSanitizeShadowDOM", b, null); e = f.nextNode(); )
                    d("uponSanitizeShadowNode", e, null),
                    c(e) || (e.content instanceof pb && Tb(e.content),
                    a(e));
                d("afterSanitizeShadowDOM", b, null)
            };
            k.sanitize = function(b, d) {
                var f = void 0
                  , l = void 0
                  , F = void 0
                  , x = void 0;
                b || (b = "\x3c!--\x3e");
                if ("string" !== typeof b && !e(b)) {
                    if ("function" !== typeof b.toString)
                        throw new TypeError("toString is not a function");
                    b = b.toString();
                    if ("string" !== typeof b)
                        throw new TypeError("dirty is not a string, aborting");
                }
                if (!k.isSupported) {
                    if ("object" === Q(p.toStaticHTML) || "function" === typeof p.toStaticHTML) {
                        if ("string" === typeof b)
                            return p.toStaticHTML(b);
                        if (e(b))
                            return p.toStaticHTML(b.outerHTML)
                    }
                    return b
                }
                Za || n(d);
                k.removed = [];
                if (!la)
                    if (b instanceof ja)
                        f = g("\x3c!--\x3e"),
                        l = f.ownerDocument.importNode(b, !0),
                        1 === l.nodeType && "BODY" === l.nodeName ? f = l : "HTML" === l.nodeName ? f = l : f.appendChild(l);
                    else {
                        if (!V && !N && !U && Xa && -1 === b.indexOf("<"))
                            return B ? B.createHTML(b) : b;
                        f = g(b);
                        if (!f)
                            return V ? null : Zb
                    }
                f && Ua && m(f.firstChild);
                for (var Oa = h(la ? b : f); d = Oa.nextNode(); )
                    3 === d.nodeType && d === F || c(d) || (d.content instanceof pb && Vd(d.content),
                    a(d),
                    F = d);
                F = null;
                if (la)
                    return b;
                if (V) {
                    if (Wa)
                        for (x = Td.call(f.ownerDocument); f.firstChild; )
                            x.appendChild(f.firstChild);
                    else
                        x = f;
                    cc && (x = Ud.call(xa, x, !0));
                    return x
                }
                var r = U ? f.outerHTML : f.innerHTML;
                N && (r = r.replace(Xc, " "),
                r = r.replace(Yc, " "));
                return B && Xa ? B.createHTML(r) : r
            }
            ;
            k.setConfig = function(a) {
                n(a);
                Za = !0
            }
            ;
            k.clearConfig = function() {
                O = null;
                Za = !1
            }
            ;
            k.isValidAttribute = function(a, c, d) {
                O || n({});
                a = a.toLowerCase();
                c = c.toLowerCase();
                return b(a, c, d)
            }
            ;
            k.addHook = function(a, b) {
                "function" === typeof b && (C[a] = C[a] || [],
                C[a].push(b))
            }
            ;
            k.removeHook = function(a) {
                C[a] && C[a].pop()
            }
            ;
            k.removeHooks = function(a) {
                C[a] && (C[a] = [])
            }
            ;
            k.removeAllHooks = function() {
                C = {}
            }
            ;
            return k
        }
        var id = hd();
        var jd = id(self)
          , kd = {
            script: {
                attribute: "type",
                values: ["application/json", "application/ld+json"]
            }
        }
          , ld = {
            USE_PROFILES: {
                html: !0,
                svg: !0,
                svgFilters: !0
            }
        }
          , md = 1;
        function nd(a) {
            var b = z({
                IN_PLACE: !0
            })
              , c = id(self)
              , d = Object.assign(b || {}, Object.assign({}, ld, {
                ADD_ATTR: qc,
                ADD_TAGS: ["use"],
                FORBID_TAGS: Object.keys(oc),
                FORCE_BODY: !0,
                RETURN_DOM: !0,
                ALLOW_UNKNOWN_PROTOCOLS: !0
            }));
            c.setConfig(d);
            od(c, a);
            return c
        }
        function pd() {
            var a = {};
            jd.addHook("uponSanitizeElement", function(b, c) {
                Object.assign(a, c.allowedTags)
            });
            jd.sanitize("<p></p>");
            Object.keys(oc).forEach(function(b) {
                a[b] = !1
            });
            jd.removeHook("uponSanitizeElement");
            return a
        }
        function od(a, b) {
            function c(a) {
                nc(a, function() {
                    return String(md++)
                });
                n.forEach(function(a) {
                    delete m[a]
                });
                n.length = 0;
                "use" === a.nodeName.toLowerCase() && ["href", "xlink:href"].forEach(function(b) {
                    a.hasAttribute(b) && !I(a.getAttribute(b), "#") && (a.parentElement && a.parentElement.removeChild(a),
                    E().error("purifier", 'Removed invalid <use>. use[href] must start with "#".'))
                })
            }
            function d(a, c) {
                function d() {
                    m[f] || (m[f] = !0,
                    n.push(f))
                }
                var e = a.nodeName.toLowerCase()
                  , f = c.attrName
                  , h = c.attrValue;
                m = c.allowedAttributes;
                if (I(e, "amp-"))
                    d();
                else {
                    if ("a" == e && "target" == f) {
                        var g = h.toLowerCase();
                        h = sc.includes(g) ? g : "_top"
                    }
                    var k = rc[e];
                    k && k.includes(f) && d()
                }
                var l = qd(f);
                if (l === rd) {
                    var p = f.substring(1, f.length - 1);
                    a.setAttribute("data-amp-bind-" + p, h)
                }
                l !== sd && a.setAttribute("i-amphtml-binding", "");
                Cc(e, f, h, b) ? h && !I(f, "data-amp-bind-") && (h = ic(e, f, h)) : (c.keepAttr = !1,
                E().error("purifier", 'Removed invalid attribute %s[%s="%s"].', e, f, h));
                c.attrValue = h
            }
            function e() {
                l.forEach(function(a) {
                    delete g[a]
                });
                l.length = 0
            }
            function f(a, b) {
                var c = b.tagName;
                g = b.allowedTags;
                I(c, "amp-") && (g[c] = !h || pc[c]);
                "a" === c && a.hasAttribute("href") && !a.hasAttribute("target") && a.setAttribute("target", "_top");
                var d = kd[c];
                if (d) {
                    var e = d;
                    b = e.attribute;
                    e = e.values;
                    a.hasAttribute(b) && e.includes(a.getAttribute(b)) && (g[c] = !0,
                    l.push(c))
                }
            }
            var h = zb(b), g, l = [], m, n = [];
            a.addHook("uponSanitizeElement", f);
            a.addHook("afterSanitizeElements", e);
            a.addHook("uponSanitizeAttribute", d);
            a.addHook("afterSanitizeAttributes", c)
        }
        var sd = 0
          , rd = 1
          , td = 2;
        function qd(a) {
            return "[" == a[0] && "]" == a[a.length - 1] ? rd : I(a, "data-amp-bind-") ? td : sd
        }
        function ud(a, b, c, d) {
            var e = b.nodeName.toLowerCase()
              , f = kd[e];
            if (f) {
                var h = f.values;
                if (f.attribute === c && (null == d || !h.includes(d)))
                    return !1
            }
            if ("a" === e && "target" === c && (null == d || !sc.includes(d)))
                return !1;
            if (null == d)
                return !0;
            if (qd(c) !== sd)
                return !1;
            var g = a.isValidAttribute(e, c, d);
            if (!g) {
                var l = (a = rc[e]) && a.includes(c);
                if (!l && !I(e, "amp-"))
                    return !1
            }
            b = b.ownerDocument ? b.ownerDocument : b;
            return d && !Cc(e, c, d, b) ? !1 : !0
        }
        ;function vd(a) {
            return "fixed" == a || "fixed-height" == a || "responsive" == a || "fill" == a || "flex-item" == a || "fluid" == a || "intrinsic" == a
        }
        ;function wd(a, b) {
            if (a.__AMP__EXPERIMENT_TOGGLES)
                var c = a.__AMP__EXPERIMENT_TOGGLES;
            else {
                a.__AMP__EXPERIMENT_TOGGLES = Object.create(null);
                c = a.__AMP__EXPERIMENT_TOGGLES;
                if (a.AMP_CONFIG)
                    for (var d in a.AMP_CONFIG) {
                        var e = a.AMP_CONFIG[d];
                        "number" === typeof e && 0 <= e && 1 >= e && (c[d] = Math.random() < e)
                    }
                if (a.AMP_CONFIG && Array.isArray(a.AMP_CONFIG["allow-doc-opt-in"]) && 0 < a.AMP_CONFIG["allow-doc-opt-in"].length && (d = a.AMP_CONFIG["allow-doc-opt-in"],
                e = a.document.head.querySelector('meta[name="amp-experiments-opt-in"]'))) {
                    e = e.getAttribute("content").split(",");
                    for (var f = 0; f < e.length; f++)
                        -1 != d.indexOf(e[f]) && (c[e[f]] = !0)
                }
                Object.assign(c, xd(a));
                if (a.AMP_CONFIG && Array.isArray(a.AMP_CONFIG["allow-url-opt-in"]) && 0 < a.AMP_CONFIG["allow-url-opt-in"].length)
                    for (d = a.AMP_CONFIG["allow-url-opt-in"],
                    a = Cb(a.location.originalHash || a.location.hash),
                    e = 0; e < d.length; e++)
                        f = a["e-" + d[e]],
                        "1" == f && (c[d[e]] = !0),
                        "0" == f && (c[d[e]] = !1)
            }
            var h = c;
            return !!h[b]
        }
        function xd(a) {
            var b = "";
            try {
                "localStorage"in a && (b = a.localStorage.getItem("amp-experiment-toggles"))
            } catch (e) {
                G().warn("EXPERIMENTS", "Failed to retrieve experiments from localStorage.")
            }
            var c = b ? b.split(/\s*,\s*/g) : [];
            a = Object.create(null);
            for (var d = 0; d < c.length; d++)
                0 != c[d].length && ("-" == c[d][0] ? a[c[d].substr(1)] = !1 : a[c[d]] = !0);
            return a
        }
        ;var yd = {}
          , S = (yd["ampdoc-fie"] = {
            isTrafficEligible: function() {
                return !0
            },
            branches: [["21065001"], ["21065002"]]
        },
        yd);
        function zd(a, b) {
            var c = a.ownerDocument.defaultView
              , d = c.__AMP_TOP || (c.__AMP_TOP = c)
              , e = c != d;
            var f = d;
            if (wd(f, "ampdoc-fie")) {
                f.__AMP_EXPERIMENT_BRANCHES = f.__AMP_EXPERIMENT_BRANCHES || {};
                for (var h in S)
                    if (xb.call(S, h) && !xb.call(f.__AMP_EXPERIMENT_BRANCHES, h))
                        if (S[h].isTrafficEligible && S[h].isTrafficEligible(f)) {
                            if (!f.__AMP_EXPERIMENT_BRANCHES[h] && wd(f, h)) {
                                var g = S[h].branches;
                                f.__AMP_EXPERIMENT_BRANCHES[h] = g[Math.floor(Math.random() * g.length)] || null
                            }
                        } else
                            f.__AMP_EXPERIMENT_BRANCHES[h] = null;
                f = "21065002" === (f.__AMP_EXPERIMENT_BRANCHES ? f.__AMP_EXPERIMENT_BRANCHES["ampdoc-fie"] : null)
            } else
                f = !1;
            var l = f;
            e && !l ? b = Ad(c, b) ? Bd(c, b) : null : (a = Cd(a),
            a = Dd(a),
            b = Ad(a, b) ? Bd(a, b) : null);
            return b
        }
        function T(a, b) {
            a = a.__AMP_TOP || (a.__AMP_TOP = a);
            return Bd(a, b)
        }
        function Ed(a, b) {
            return Fd(Dd(a), b)
        }
        function Cd(a) {
            return a.nodeType ? T((a.ownerDocument || a).defaultView, "ampdoc").getAmpDoc(a) : a
        }
        function Dd(a) {
            a = Cd(a);
            return a.isSingleDoc() ? a.win : a
        }
        function Bd(a, b) {
            Ad(a, b);
            var c = Gd(a);
            a = c[b];
            a.obj || (a.obj = new a.ctor(a.context),
            a.ctor = null,
            a.context = null,
            a.resolve && a.resolve(a.obj));
            return a.obj
        }
        function Hd(a, b) {
            var c = Fd(a, b);
            if (c)
                return c;
            a = Gd(a);
            a[b] = Id();
            return a[b].promise
        }
        function Fd(a, b) {
            var c = Gd(a)[b];
            if (c) {
                if (c.promise)
                    return c.promise;
                Bd(a, b);
                return c.promise = Promise.resolve(c.obj)
            }
            return null
        }
        function Gd(a) {
            var b = a.__AMP_SERVICES;
            b || (b = a.__AMP_SERVICES = {});
            return b
        }
        function Ad(a, b) {
            a = a.__AMP_SERVICES && a.__AMP_SERVICES[b];
            return !(!a || !a.ctor && !a.obj)
        }
        function Id() {
            var a = new Dc
              , b = a.promise
              , c = a.resolve;
            a = a.reject;
            b.catch(function() {});
            return {
                obj: null,
                promise: b,
                resolve: c,
                reject: a,
                context: null,
                ctor: null
            }
        }
        ;function Jd(a) {
            return Kd(a, "amp-script", "amp-script", void 0).then(function(a) {
                return H(a, "Service %s was requested to be provided through %s, but %s is not loaded in the current page. To fix this problem load the JavaScript file for %s in this page.", "amp-script", "amp-script", "amp-script", "amp-script")
            })
        }
        function Kd(a, b, c, d) {
            var e = Ed(a, b);
            if (e)
                return e;
            var f = Cd(a);
            return f.waitForBodyOpen().then(function() {
                return Wd(f.win, c, f.win.document.head)
            }).then(function() {
                if (d)
                    var e = Ed(a, b);
                else
                    e = f.win,
                    e = e.__AMP_EXTENDED_ELEMENTS && e.__AMP_EXTENDED_ELEMENTS[c] ? Hd(Dd(a), b) : null;
                return e
            })
        }
        function Xd(a) {
            var b = zd(a, "bind");
            if (b)
                return Promise.resolve(b);
            b = a.ownerDocument.defaultView;
            var c = b.__AMP_TOP || (b.__AMP_TOP = b);
            return b !== c ? Yd(b) : Kd(a, "bind", "amp-bind")
        }
        function Wd(a, b, c) {
            if (c) {
                var d = {};
                c = c.querySelectorAll("script[custom-element],script[custom-template]");
                for (var e = 0; e < c.length; e++) {
                    var f = c[e];
                    f = f.getAttribute("custom-element") || f.getAttribute("custom-template");
                    d[f] = !0
                }
                d = Object.keys(d)
            } else
                d = [];
            if (!d.includes(b))
                return Promise.resolve();
            var h = T(a, "extensions");
            return h.waitForExtension(a, b)
        }
        function Yd(a) {
            return Gc(a.document).then(function() {
                return Wd(a, "amp-bind", a.document.head)
            }).then(function() {
                return a.__AMP_EXTENDED_ELEMENTS && a.__AMP_EXTENDED_ELEMENTS["amp-bind"] ? Hd(a, "bind") : null
            })
        }
        ;var Zd = ["click", "input", "dblclick", "keypress", "submit"];
        function $d(a) {
            var b = this;
            this.M = a;
            this.J = this.S.bind(this);
            this.l = 0;
            this.A = !1;
            Zd.forEach(function(a) {
                b.M.addEventListener(a, b.J, !0)
            })
        }
        q = $d.prototype;
        q.dispose = function() {
            var a = this;
            Zd.forEach(function(b) {
                a.M.removeEventListener(b, a.J, !0)
            })
        }
        ;
        q.hasBeenActive = function() {
            return 0 < this.l
        }
        ;
        q.isActive = function() {
            return 0 < this.l && 5E3 >= Date.now() - this.l || this.A
        }
        ;
        q.getLastActivationTime = function() {
            return this.l
        }
        ;
        q.expandLongTask = function(a) {
            var b = this;
            this.isActive() && (this.A = !0,
            a.catch(function() {}).then(function() {
                b.A = !1;
                b.l = Date.now()
            }))
        }
        ;
        q.isInLongTask = function() {
            return this.A
        }
        ;
        q.S = function(a) {
            a.isTrusted && (this.l = Date.now())
        }
        ;
        function ae(a, b) {
            var c = "0.1";
            b ? (b = a.protocol + "//" + a.host,
            "about:" == a.protocol && (b = ""),
            a = b + "/dist") : a = Eb.cdn;
            b = self;
            if (b.__AMP_MODE)
                b = b.__AMP_MODE;
            else {
                var d = self.AMP_CONFIG || {};
                var e = !!d.test || !1;
                var f = Cb(b.location.originalHash || b.location.hash);
                d = d.spt;
                var h = Cb(b.location.search);
                Db || (Db = b.AMP_CONFIG && b.AMP_CONFIG.v ? b.AMP_CONFIG.v : "011911191835190");
                e = {
                    localDev: !1,
                    development: !!(0 <= ["1", "actions", "amp", "amp4ads", "amp4email"].indexOf(f.development) || b.AMP_DEV_MODE),
                    examiner: "2" == f.development,
                    geoOverride: f["amp-geo"],
                    userLocationOverride: f["amp-user-location"],
                    minified: !0,
                    lite: void 0 != h.amp_lite,
                    test: e,
                    log: f.log,
                    version: "1911191835190",
                    rtvVersion: Db,
                    singlePassType: d
                };
                b = b.__AMP_MODE = e
            }
            var g = b.rtvVersion;
            null == c && (c = "0.1");
            var l = c ? "-" + c : "";
            return a + "/rtv/" + g + "/v0/amp-script-worker" + l + ".js"
        }
        ;self.__AMP_ERRORS = self.__AMP_ERRORS || [];
        function be(a, b) {
            try {
                return JSON.parse(a)
            } catch (c) {
                return b && b(c),
                null
            }
        }
        ;function ce(a) {
            if ("undefined" !== typeof TextEncoder)
                a = (new TextEncoder("utf-8")).encode(a);
            else {
                a = unescape(encodeURIComponent(a));
                for (var b = new Uint8Array(a.length), c = 0; c < a.length; c++) {
                    var d = a.charCodeAt(c);
                    b[c] = d
                }
                a = b
            }
            return a
        }
        ;function W(a) {
            a = AMP.BaseElement.call(this, a) || this;
            a.N = T(a.win, "vsync");
            a.P = null;
            a.H = null;
            a.B = null;
            a.m = "amp-script[unknown].js";
            a.o = !1;
            return a
        }
        var X = AMP.BaseElement;
        W.prototype = pa(X.prototype);
        W.prototype.constructor = W;
        if (ua)
            ua(W, X);
        else
            for (var Y in X)
                if ("prototype" != Y)
                    if (Object.defineProperties) {
                        var de = Object.getOwnPropertyDescriptor(X, Y);
                        de && Object.defineProperty(W, Y, de)
                    } else
                        W[Y] = X[Y];
        W.W = X.prototype;
        q = W.prototype;
        q.isLayoutSupported = function(a) {
            return "container" == a || vd(a)
        }
        ;
        q.buildCallback = function() {
            var a = this
              , b = this.element.ownerDocument.documentElement;
            if (this.o = this.element.hasAttribute("development") || b.hasAttribute("data-ampdevmode") && Ic(this.element) != b)
                E().warn("amp-script", "JavaScript size and script hash requirements are disabled in development mode.", this.element),
                this.element.hasAttribute("development") && E().warn("amp-script", "The 'development' flag is deprecated. Please use 'data-ampdevmode' on the root html element instead", this.element);
            return Jd(this.element).then(function(b) {
                a.setService(b)
            })
        }
        ;
        q.setService = function(a) {
            this.B = a
        }
        ;
        q.layoutCallback = function() {
            var a = this;
            this.H = new $d(this.element);
            this.m = this.element.hasAttribute("src") ? 'amp-script[src="' + this.element.getAttribute("src") + '"].js' : 'amp-script[script="' + this.element.getAttribute("script") + '"].js';
            var b = ee(this, this.m);
            if (!b)
                return E().error("amp-script", '"src" or "script" attribute is required.'),
                Promise.reject(Error("CANCELLED"));
            var c = Promise.all([fe(this), b]).then(function(b) {
                var c = b[0]
                  , d = b[1];
                return !a.o && a.B.sizeLimitExceeded(d.length) ? (E().error("amp-script", "Maximum total script size exceeded (%s). %s is disabled. See https://amp.dev/documentation/components/amp-script/#size-of-javascript-code.", 15E4, a.m),
                a.element.classList.add("i-amphtml-broken"),
                []) : [c, d]
            })
              , d = this.element.getAttribute("sandbox") || ""
              , e = d.split(" ").map(function(a) {
                return a.trim()
            })
              , f = {
                authorURL: this.m,
                mutationPump: this.V.bind(this),
                longTask: function(b) {
                    a.H.expandLongTask(b)
                },
                sanitizer: new ge(this.win,this.element,e),
                onCreateWorker: function(a) {
                    G().info("amp-script", "Create worker:", a)
                },
                onSendMessage: function(a) {
                    G().info("amp-script", "To worker:", a)
                },
                onReceiveMessage: function(a) {
                    G().info("amp-script", "From worker:", a)
                }
            };
            wb(this.element, c, f).then(function(b) {
                a.P = b
            });
            return c
        }
        ;
        function fe(a) {
            var b = !1
              , c = ae(a.win.location, b);
            return T(a.win, "xhr").fetchText(c, {
                ampCors: !1
            }).then(function(a) {
                return a.text()
            })
        }
        function ee(a, b) {
            var c = a.element.getAttribute("src");
            if (c)
                return he(a, c, b);
            var d = a.element.getAttribute("script");
            if (d) {
                var e = a.getAmpDoc().getElementById(d);
                H(e, "[%s] %s could not find element with #%s.", "amp-script", b, d);
                var f = e.getAttribute("target");
                H("amp-script" === f, '[%s] script#%s must have target="amp-script".', "amp-script", d);
                var h = e.textContent;
                return a.o ? Promise.resolve(h) : a.B.checkSha384(h, b).then(function() {
                    return h
                })
            }
            return null
        }
        function he(a, b, c) {
            return T(a.win, "xhr").fetchText(b, {
                ampCors: !1
            }).then(function(b) {
                if (b.url && ie(a, b.url)) {
                    var d = b.headers.get("Content-Type");
                    if (!d || !I(d, "application/javascript"))
                        throw E().error("amp-script", 'Same-origin "src" requires "Content-Type: application/javascript". Fetched source for %s has "Content-Type: %s". See https://amp.dev/documentation/components/amp-script/#security-features.', c, d),
                        Error();
                    return b.text()
                }
                return a.o ? b.text() : b.text().then(function(b) {
                    return a.B.checkSha384(b, c).then(function() {
                        return b
                    })
                })
            })
        }
        function ie(a, b) {
            var c = zd(a.element, "url")
              , d = c.getSourceOrigin(a.getAmpDoc().getUrl())
              , e = c.parse(b).origin;
            return d === e
        }
        q.V = function(a, b) {
            var c = this;
            1 == b && this.N.mutate(function() {
                return c.element.classList.add("i-amphtml-hydrated")
            });
            var d = 2 != b || this.H.isActive() || vd(this.getLayout()) && 300 >= this.getLayoutBox().height;
            d ? this.N.mutate(a) : (this.P.terminate(),
            this.element.classList.remove("i-amphtml-hydrated"),
            this.element.classList.add("i-amphtml-broken"),
            E().error("amp-script", "%s was terminated due to illegal mutation.", this.m))
        }
        ;
        function je(a) {
            this.F = 0;
            this.h = [];
            var b = a.getHeadNode().querySelector('meta[name="amp-script-src"]');
            b && b.hasAttribute("content") && (this.h = b.getAttribute("content").split(" ").map(function(a) {
                return a.trim()
            }).filter(function(a) {
                return a.length
            }));
            this.U = T(a.win, "crypto")
        }
        je.prototype.checkSha384 = function(a, b) {
            var c = this;
            a = ce(a);
            return this.U.sha384Base64(a).then(function(a) {
                if (!a || !c.h.includes("sha384-" + a))
                    throw E().error("amp-script", 'Script hash not found. %s must have "sha384-%s" in meta[name="amp-script-src"]. See https://amp.dev/documentation/components/amp-script/#security-features.', b, a),
                    Error();
            })
        }
        ;
        je.prototype.sizeLimitExceeded = function(a) {
            this.F += a;
            return 15E4 < this.F
        }
        ;
        var ke = "form button fieldset input object output select textarea".split(" ");
        function ge(a, b, c) {
            var d = this;
            this.C = a;
            this.K = b;
            this.L = nd(a.document);
            this.j = pd();
            this.j["amp-img"] = !0;
            this.j["amp-layout"] = !0;
            this.j["amp-pixel"] = !1;
            this.I = c.includes("allow-forms");
            ke.forEach(function(a) {
                d.j[a] = d.I
            })
        }
        q = ge.prototype;
        q.sanitize = function(a) {
            var b = a.nodeName.toLowerCase()
              , c = this.j[b];
            c || le(this, b) || E().warn("amp-script", "Sanitized node:", a);
            return c
        }
        ;
        q.setAttribute = function(a, b, c) {
            var d = a.nodeName.toLowerCase();
            if (this.j[d]) {
                var e = b.toLowerCase();
                if (ud(this.L, a, e, c))
                    return null == c ? a.removeAttribute(e) : (b = ic(d, e, c),
                    a.setAttribute(e, b)),
                    "a" === d && a.hasAttribute("href") && !a.hasAttribute("target") && a.setAttribute("target", "_top"),
                    !0
            }
            le(this, d) || E().warn("amp-script", 'Sanitized [%s]="%s":', b, c, a);
            return !1
        }
        ;
        function le(a, b) {
            return !a.I && ke.includes(b) ? (E().warn("amp-script", 'Form elements (%s) are not allowed without sandbox="allow-forms".', b),
            !0) : !1
        }
        q.setProperty = function(a, b, c) {
            var d = b.toLowerCase();
            return ud(this.L, a, d, c) ? (a[b] = c,
            !0) : !1
        }
        ;
        q.getStorage = function(a, b) {
            if (2 === a)
                return Xd(this.K).then(function(a) {
                    if (a)
                        return a.getStateValue(b || ".")
                });
            a = 0 === a ? this.C.localStorage : 1 === a ? this.C.sessionStorage : null;
            for (var c = {}, d = 0; d < a.length; d++) {
                var e = a.key(d);
                e && !I(e, "amp-") && (c[e] = a.getItem(e))
            }
            return c
        }
        ;
        q.setStorage = function(a, b, c) {
            if (2 === a)
                return Xd(this.K).then(function(a) {
                    if (a) {
                        var b = be(c, function() {
                            G().error("amp-script", "Invalid AMP.setState() argument: %s", c)
                        });
                        b && a.setState(b, !0, !1)
                    }
                });
            a = 0 === a ? this.C.localStorage : 1 === a ? this.C.sessionStorage : null;
            null === b ? null === c && E().error("amp-script", "Storage.clear() is not supported in amp-script.") : I(b, "amp-") ? E().error("amp-script", 'Invalid "amp-" prefix for storage key: %s', b) : null === c ? a.removeItem(b) : a.setItem(b, c);
            return Promise.resolve()
        }
        ;
        (function(a) {
            a.registerServiceForDoc("amp-script", je);
            a.registerElement("amp-script", W, "amp-script{opacity:0.7}amp-script.i-amphtml-hydrated{opacity:1}\n/*# sourceURL=/extensions/amp-script/0.1/amp-script.css*/")
        }
        )(self.AMP);
    }
    )
});

//# sourceMappingURL=amp-script-0.1.js.map

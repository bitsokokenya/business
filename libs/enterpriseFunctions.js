var exports = module.exports = {};


// We need this to build our post string

function getBitsWinOpt(str, aKey) {
    try {
        var ps = str.split("?")[1];
        var pairs = ps.split("&");
    } catch (e) {
        return false;
    }


    for (var i = 0, aKey = aKey; i < pairs.length; ++i) {
        var key = pairs[i].split("=")[0];

        var value = pairs[i].split("=")[1];
        if (key == aKey) {

            return value;

        }

    }
}


 exports.httpGet = function httpGet(url) {
  return new Promise((resolve, reject) => {
    const http = require('http'),
      https = require('https');

    let client = http;

    if (url.toString().indexOf("https") === 0) {
      client = https;
    }

    client.get(url, (resp) => {
      let chunks = [];

      // A chunk of data has been recieved.
      resp.on('data', (chunk) => {
        chunks.push(chunk);
      });

      // The whole response has been received. Print out the result.
      resp.on('end', () => {
          
  
  var response = Buffer.concat(chunks).toString('utf-8');
        resolve(response);
      });

    }).on("error", (err) => {
      reject(err);
    });
  });
}





exports.storeDepsLoaded = function(location,object) {
    var stPth = '/root/business/bitsAssets/js/storeAll.js';
            
	try{     
var source = fs.readFileSync(stPth, {encoding:'utf8', flag:'r'});

	const script = new vm.Script(source);

   //fs.accessSync(stPth, fs.F_OK);
	
        }catch(er){
            console.warn('INFO! store packed js is invalid. rebuilding..',er);
      
var conctd = [
    '/bitsAssets/js/jquery/jquery-3.3.1.min.js',
'/socket.io/socket.io.js',
'/bitsAssets/js/moment.js',
'/bitsAssets/js/browserDetect.js',
'/bitsAssets/js/storeManager.js',
'/bitsAssets/js/globalVariables.js',
'/bitsAssets/js/globalServices.js',
    '/bitsAssets/js/chat.js',
    '/bitsAssets/js/locationManager.js',
    '/bitsAssets/js/broadcastChannel.js',
    '/bitsAssets/js/connect.js',
    '/bitsAssets/js/materialize/materialize.min.js',
'/bitsAssets/js/raphQR.js',
'/bitsAssets/js/qrcodesvg.js',
    '/bitsAssets/js/firebase-app.js',
    '/bitsAssets/js/firebase-messaging.js',
    '/bits/js/bits-addMobiVeri.js',
    '/bitsAssets/js/md5.min.js',
    '/bits/js/bits_auth.js',
    '/bits/js/products-promo-match.js',
    '/bits/js/delRate.js',
    '/bits/js/bits_promos.js',
    '/bits/js/bits-wallets-sort.js',
    '/bits/js/Bits-closest-shop.js',
    '/bits/js/bits-FeatureDiscovery.js',
    '/bits/js/bits-follow.js',
    '/bits/js/bits-wallet-on.js',
    '/bits/js/bits-wallet-functions.js',
    '/bits/js/bits-oder-functions.js',
    '/bits/js/bits-theme.js',
    '/bits/js/bits-animate.js',
    '/bits/js/update_services.js',
    '/bits/js/bitsCore.js',
    '/bits/js/init.js',
    '/bits/js/feedback.js',
    '/bits/js/screenOrientation.js',
    '/bits/js/functions.js',
    '/bits/js/plugins/sweetalert/dist/sweetalert.min.js',
    '/bits/js/plugins.js',
    '/bits/js/jquery.star.rating.js',
    '/bits/js/custom-script.js',
    '/bitsAssets/js/google.js'
    ];


stModed=[];

for(var i = 0; i < conctd.length; i++) {
    if(conctd[i].includes('.js')){
        
      
    //svModed[i] = "/root/business" + serverFiles[i];
    stModed.push("/root/business" + conctd[i]);
        
    }
}
   return new Promise((resolve, reject) => {
      require('concat-files')(stModed, stPth, function(err) {
    if (err){
         console.error(err);
         
    reject({err: err}) ;
    }
    
    resolve({msg: 'concd'}) ;
  });
        
    });
 
      }
    
}






exports.adminDepsLoaded = function(location,object) {
    var stPth = '/root/business/bitsAssets/js/adminAll.js';
            
	try{     
var source = fs.readFileSync(stPth, {encoding:'utf8', flag:'r'});

	const script = new vm.Script(source);

   //fs.accessSync(stPth, fs.F_OK);
	
        }catch(er){
            console.warn('INFO! store packed js is invalid. rebuilding..',er);
      
var conctd = [
    '/bitsAssets/js/jquery/jquery-3.3.1.min.js',
'/socket.io/socket.io.js',
'/bitsAssets/js/moment.js',
'/bitsAssets/js/browserDetect.js',
'/bitsAssets/js/storeManager.js',
'/bitsAssets/js/globalVariables.js',
'/bitsAssets/js/globalServices.js',
    '/bitsAssets/js/chat.js',
    '/bitsAssets/js/locationManager.js',
    '/bitsAssets/js/broadcastChannel.js',
    '/bitsAssets/js/connect.js',
    '/bitsAssets/js/materialize/materialize.min.js',
'/bitsAssets/js/raphQR.js',
'/bitsAssets/js/qrcodesvg.js',
    '/bitsAssets/js/firebase-app.js',
    '/bitsAssets/js/firebase-messaging.js',
    '/bitsAssets/js/google.js'
    ];


stModed=[];

for(var i = 0; i < conctd.length; i++) {
    if(conctd[i].includes('.js')){
        
      
    //svModed[i] = "/root/business" + serverFiles[i];
    stModed.push("/root/business" + conctd[i]);
        
    }
}
   return new Promise((resolve, reject) => {
      require('concat-files')(stModed, stPth, function(err) {
    if (err){
         console.error(err);
         
    reject({err: err}) ;
    }
    
    resolve({msg: 'concd'}) ;
  });
        
    });
 
      }
    
}



exports.saveToJsonFile = function(location,object) {
	   
jsonfile.writeFile(location, object, {spaces: 2}, function(err) {
 	if(err){
	
	//console.log('err! Unable to create store manifest',err);
	}else{
	  
	  console.log('INFO! created store manifest for ');
	 
	   
	   
	   }
});


}




exports.createManifest = function(store) {
	   
var file = '/root/business/bits/web-manifest.json';
var obj = {
  short_name: store.name,
  name: store.name,
  icons: [
    {
      "src": "../bitsAssets/images/icon.png",
      "sizes": "256x256",
      "type": "image/png"
    }
  ],
  start_url: "/bits/index-"+store.id+'.html',
  display: "standalone",
  orientation: "portrait",
  background_color: "#ffffff",
  theme_color: store.id,
  scope: "/bits/",
  gcm_sender_id: "103953800507",  
  gcm_user_visible_only: true  
};

entFunc.saveToJsonFile(file, obj);

}


exports.getOrderManager = function(oid,act) {
    console.log(oid,act);
  var deferred = new Deferred();
    try{
	    //either complete or cancel this order
       if(act=='complete'){
       
     
bsConn.mysql.query('UPDATE orders SET state = ? WHERE id = ?',['complete',parseInt(oid)],
       
        function(err, results) { 

    deferred.resolve({ action : act, "status" : "ok"});  
});   
       }else if(act=='reject'){
       
     
bsConn.mysql.query('UPDATE orders SET state = ? AND deliveredBy = ? WHERE id = ?',['pending',0,parseInt(oid)],
       
        function(err, results) { 

    deferred.resolve({ action : act, "status" : "ok"});  
});   
       }else if(act=='cancel'){
           
var sql = "DELETE from orders WHERE id = ?";
 
var query = bsConn.mysql.query(sql, [parseInt(oid)], function(err, result) {
    if(err){
        deferred.resolve({ action : act, "status" : "ok"});  
       }else{
    deferred.resolve({ action : act, "status" : "bad"});    
       }
});
	       
       } 
    }catch(e){
       deferred.reject(e); 
    }
    
    
    return deferred;
}


exports.getAllServices = function(req) {

    var deferred = new Deferred();
	console.log('which domain??!!   ',allDomains[0].url);
if(allDomains[0].url=='supplies.bitsoko.org'){
	
	
	bsConn.mysql.query('SELECT * FROM services',
        function(err, results) {
            if (err) {
	
            deferred.reject('no services');	    
	    }else{
	    
            deferred.resolve(results);
	    }
	});

}else if(allDomains[0].url=='dooca.net'){
	
	
	bsConn.mysql.query('SELECT * FROM services WHERE status=?', ['active'],
        function(err, results) {
            if (err) {
	
            deferred.reject('no services '+err);	    
	    }else{
	    
            deferred.resolve(results);
	    }
	});

}else{
	
	bsConn.mysql.query('SELECT * FROM services WHERE owner=? AND status=?', [allInfo.uid,'active'],
        function(err, results) {
            if (err) {
	
            deferred.reject('no services');	    
	    }else{
	    
            deferred.resolve(results);
	    }
	});
}
  return deferred;

}

exports.createEnterprisePage = async function(req,allInfo) {

    
    var orgId = getBitsWinOpt(req.url,'org');
    
    if (orgId == false) {
         
    var orgName = allInfo.name;  
         
    }else{
         
    try{
        
  var response = await exports.httpGet(mainDomain+'/doocaOrg?org='+orgId);

 
    var orgName = response.data.name;
        
    }catch(e){
        
       var orgName = allInfo.name;  
        
    }
      
         
    }
    
    
    try{
        
        fs.accessSync(__dirname.slice(0, -5) + '/themes/'+ UIDtemp +'/templates/index.amp.pug', fs.F_OK);
    
    var pugFile = __dirname.slice(0, -5) + '/themes/'+ UIDtemp +'/templates/index.amp.pug';
    
    
    
    
    }catch(err){
        
    console.log('ERR! creating theme from amp trying non-amp theme =========================>>>' + err);
        
        
        try{
        
        fs.accessSync(__dirname.slice(0, -5) + '/themes/'+ UIDtemp +'/templates/index.pug', fs.F_OK);
    
    var pugFile = __dirname.slice(0, -5) + '/themes/'+ UIDtemp +'/templates/index.pug';
    
    }catch(err){
        
    console.log('ERR! creating theme from default=========================>>>' + err);
        
    var pugFile = __dirname.slice(0, -5) + '/themes/default/templates/index.amp.pug';
    
    }
    
    
    
    }
    
    
	// for demos only!!!!
    // var pugFile = __dirname.slice(0, -5) + '/themes/simple/templates/index-demo.amp.pug';

var source = fs.readFileSync(pugFile,
            {encoding:'utf8', flag:'r'});

        //select 10 random stores to display
       var selectedStores =  (allServices.sort(() => 0.5 - Math.random())).slice(0, 10);
        var data = {
            name: orgName,
            cover: allInfo.cover,
            coverDisabled: allInfo.coverDisabled,
            tagline: allInfo.tagline,
            socialLinks: ['/bits/images/facebook.png', '/bits/images/twitter.png', '/bits/images/linkedin.png'],
            reviews: [{
                revIcon: '/bits/images/facebook.png',
                revName: 'john doe',
                revMsg: 'good service'
            }, {
                revIcon: '/bits/images/facebook.png',
                revName: 'john doe',
                revMsg: 'good service'
            }],
            productCat: allProdCat,
            phone: allInfo.phone,
            email: allInfo.email,
            managerState: allInfo.managerState,
            rate: allInfo.contractRate,
            entContract: entContract,
            desc: 'desc',
            img: allInfo.icon,
            stores: selectedStores,
            promos: allPromos,
            entSettings: entSettings,
            managers: allNewManagers,
            cid: '000'

        }
        data.body = process.argv[2];
        
        /*
        console.log('INFO! Gulping files..');
svModed=[];

for(var i = 0; i < serverFiles.length; i++) {
    if(serverFiles[i].includes('.js')){
        
      
    //svModed[i] = "/root/business" + serverFiles[i];
    svModed.push("/root/business" + serverFiles[i]);
        
    }
}
   
       require('concat-files')(svModed, '/root/business/bitsAssets/js/entAll.js', function(err) {
    if (err) console.error(err);
    
    console.log('concd');
  });
  */
        //jade.render
        var template = jade.compile(source);
        var html = template(data);
        
        
//html = await entFunc.addFileToHtml('bitsAssets/js/globalVariables.js', html);
//html = await entFunc.addFileToHtml('bitsAssets/js/globalServices.js', html);
//html = await entFunc.addFileToHtml('bitsAssets/js/connect.js', html);
//html = await entFunc.addFileToHtml('bitsAssets/js/audioManager.js', html);
//html = await entFunc.addFileToHtml('bitsAssets/js/paymentRequester.js', html);
//html = await entFunc.addFileToHtml('bitsAssets/js/browserDetect.js', html);
//html = await entFunc.addFileToHtml('bitsAssets/js/connect.js', html);
//html = await entFunc.addFileToHtml('bitsAssets/js/storeManager.js', html);
//html = await entFunc.addFileToHtml('bitsAssets/js/globalServices.js', html);
//html = await entFunc.addFileToHtml('bitsAssets/js/doocaPay.js', html);
        //res.writeHead(200);
        /*
        writeFile('business/index.html', html, function(err) {
            if (err) console.log('!ERR unable to create enterprise page', err);

            deferred.resolve(html);
        });

*/


    return html;

}


exports.getAllProducts = function() {

    var deferred = new Deferred();

    var sql = 'SELECT * FROM products WHERE owner=?';
    var sqlA = [];
    sqlA.push(stores[0]);
    //console.log('!INFO MANY PRODUCTS ',sqlA,sql,prodsArr);
    if (stores.length > 1) {
        for (var j = 1, sqlA = sqlA, sql = sql; j < stores.length; ++j) {



            sql = sql + ' OR owner = ?';
            sqlA.push(stores[j]);

        }

        bsConn.mysql.query(sql, sqlA,
            function(err, results) {
                if (err) {
                    console.log('!ERR ', err);

                    deferred.reject(err);
                } else {

                    deferred.resolve(results);
                }


            })


        return deferred;

    }
}


exports.addFileToHtml = async function(file, htmlStr) {
    // only works for files in business folder
    //
    // file = "/bitsAssets/js/materialize/materialize.min.js";
    var type = file.split('.').pop();
    
    if ( type == 'js'){
        
    var source = fs.readFileSync('business/'+file, {encoding:'utf8', flag:'r'});
    
    // TO-DO. create regex for html tag matching
   
	var htmlStr = htmlStr.replace('<script async="" src="'+file+'" type="text/javascript"></script>','<script>'+source+'</script>');
	
	var htmlStr = htmlStr.replace('<script async src="'+file+'"></script>','<script>'+source+'</script>');
	var htmlStr = htmlStr.replace('<script async="" src="'+file+'"></script>','<script>'+source+'</script>');
	
	var htmlStr = htmlStr.replace('<script src="'+file+'" defer=""></script>','<script>'+source+'</script>');
	var htmlStr = htmlStr.replace('<script src="'+file+'" defer></script>','<script>'+source+'</script>');
	
	
    return htmlStr;
    
    }else if(type == 'css'){
        
    var source = fs.readFileSync('business/'+file, {encoding:'utf8', flag:'r'});
   
    console.log(htmlStr);
    
   	var htmlStr = htmlStr.replace('<link href="'+file+'" rel="stylesheet" type="text/css">', '<style>' + source + '</style>')

    return htmlStr;
        
    }else if(type == 'html'){
        
    var source = fs.readFileSync('business/'+file, {encoding:'utf8', flag:'r'});
   
	var htmlStr = htmlStr.replace('<link href="'+file+'" rel="import">', source);
	
    return htmlStr;
        
    }
}


exports.createStorePage = function(req, accountInfo) {

return new Promise(async function(resolve, reject) {

    

var sid=req.url.slice(12).replace('.html','').split('?')[0];



    var indxPth = '/bits/index.html';

    console.log('creating store page 1 ',sid,indxPth);
    
    var r = await entFunc.returnMerchantServices('', {
        service: sid,
        id: sid
    });
     
     r.res.chainContract = {};
     
     try{
        
	 var chainAssetDataFile = '/root/business/config/A:bitsoko.org:'+sid+':B.json';
	 
    fs.accessSync( chainAssetDataFile, fs.F_OK);
	
        r.res.chainContract.poolID = JSON.parse(fs.readFileSync(chainAssetDataFile, 'utf8'));
        }catch(er){
            console.warn('ERR! creating page. business does not hae an associated asset! skipping..',er,sid);
        }
    
    r.res.chainContract.account = accountObject.address;

        //console.log('creating store page 2 ',sid,indxPth);
//TODO submit store and product and prommo images to banner array
	r.res.bannerPath = [r.res.bannerPath,r.res.bannerPath,r.res.bannerPath,r.res.bannerPath,r.res.bannerPath];

/*
        var thePrds = r.res.list;
        r.res.list = [];
        var ii = 0;

        for (var ix in thePrds) {

            if (thePrds[ix].productCategory == null || thePrds[ix].productCategory == 'null') {
                ++ii;
                r.res.list.push(thePrds[ix])
                //if (ii > 6) {
                //    break;
                //}
            }
        }
  */      
        
        for(var als in allServices){
            
            if(parseInt(allServices[als].id)===parseInt(sid)){
           rr =  allServices[als];    
            }
        }
        
        

try{
//var rr = await entFunc.bitsStoreDet(sid);

             //       console.log('setting page info '+JSON.stringify(rr));

            //console.log('creating store page from '+'bitsoko' + indxPth);
            fs.readFile('/root/business' + indxPth, function(error, source) {
                
                if (getBitsWinOpt(req.url, 'pid')) {
        //this is a promotion
        var pid = getBitsWinOpt(req.url, 'pid');
        console.log('INFO! this is a promo link! '+pid);
    } else {
        //this is not a promotion
        var pid = false;
        
        console.log('INFO! this is not a promo link! '+getBitsWinOpt(req.url, 'pid'));
    }

                if (error) console.log(error);
                
                if(pid){
                    
                   // console.log(JSON.stringify(r.res.shopPromos));
                    
                    for(var i in r.res.shopPromos){
                        
                        if(JSON.stringify(r.res.shopPromos[i].id) == pid){
                            
                    var sName = r.res.shopPromos[i].promoName;
                    var sDesc = r.res.shopPromos[i].promoDesc;
                    var sImg = r.res.shopPromos[i].promoBanner;
                    
                        }
                        
                    }
                    
                    
                }else{
                   
                    var sName = rr.name;
                    var sDesc = rr.description;
                    var sImg = rr.banner;
                     
                }
//console.log(sName, sDesc, sImg);
                html2jade.convertHtml(source, {}, async function(err, jd) {
                    var thm = rr.theme;
                    if (thm == null || thm == 'null' || thm == '') {
                        thm = primaryColor;
                    }
                    var data = {
                        name: sName,
                        desc: sDesc,
                        img: sImg,
                        theme: thm,
                        stMeta: JSON.stringify(r),
                        cid: Cid
                    }
                    data.body = process.argv[2];
                    //jade.render
                    var template = jade.compile(jd);
                    var html = template(data);
                    //res.writeHead(200);

                    html = html.replace(/https:\/\/bitsoko.org\/bitsAssets/g, "/bitsAssets");
                    html = html.replace(/#{img}/g, sImg);
                    html = html.replace(/#{name}/g, sName);
                    html = html.replace(/#{desc}/g, sDesc);
                    html = html.replace(/#{theme}/g, thm);
                    
                    var store = data;
                
                    store.id = sid;
                  entFunc.createManifest(store);
  
   await entFunc.storeDepsLoaded();     
                
                 console.log('created manifest ',sid);


//html = await entFunc.addFileToHtml('/bitsAssets/js/jquery/jquery-3.3.1.min.js', html);

//html = await entFunc.addFileToHtml('/bits/js/bits_jspdf.js', html);

// lets inline the materializejs
	//	fs.readFile('business/bitsAssets/js/materialize/materialize.min.js', function(error, source){
			
		//	if(error)console.log(error);
	//	html = html.replace('<script async="" src="/bitsAssets/js/materialize/materialize.min.js"></script>','<script>'+source+'</script>')


                    // lets inline the stylesheets to improve performance
                   // fs.readFile('business/bitsAssets/css/materialize/materialize.min.css', function(error, source) {
//
                        //if (error) console.log(error);
                       // html = html.replace('<link href="/bitsAssets/css/materialize/materialize.min.css" rel="stylesheet" type="text/css">', '<style>' + source + '</style>')

                        // lets inline the stylesheets to improve performance
                        //fs.readFile('business/bitsAssets/html/connect.html', function(error, source) {

                           // if (error) console.log(error);
                           // html = html.replace('<link href="/bitsAssets/html/connect.html" rel="import">', source)

                            // lets inline the stylesheets to improve performance
                            fs.readFile('business/bits/css/style.css', function(error, source) {

                                if (error) console.log(error);
                                html = html.replace('<link href="css/style.css" media="screen,projection" rel="stylesheet" type="text/css">', '<style>' + source + '</style>')



                                console.log('!info saving new store page to ' + '/root/business/bits/index-' + sid + '.html');
                                //writeFile('/root/business/bits/index-' + sid + '.html', html, function(err) {
                                //    if (err) console.log('!ERR unable to write database client key', err);
                                resolve(html);
                                //});


                            })





                       // })




                   // })

                   // })



                });
            });
            
    
}catch(err){
    console.log('err! cannot show merchant share info! merchant ' + sid + ' not found!', err);
}


});

}


exports.createAdminPage = function(req, accountInfo) {

return new Promise(async function(resolve, reject) {

    

var sid=req.url.slice(12).replace('.html','').split('?')[0];



    var indxPth = '/soko/index.html';

    console.log('creating admin page ',sid,indxPth);
    
    var r = await entFunc.returnMerchantServices('', {
        service: sid,
        id: sid
    });
     
     r.res.chainContract = {};
     
     try{
        
	 var chainAssetDataFile = '/root/business/config/A:bitsoko.org:'+sid+':B.json';
	 
    fs.accessSync( chainAssetDataFile, fs.F_OK);
	
        r.res.chainContract.poolID = JSON.parse(fs.readFileSync(chainAssetDataFile, 'utf8'));
        }catch(er){
            console.warn('ERR! creating page. business does not hae an associated asset! skipping..',er,sid);
        }
    
    r.res.chainContract.account = accountObject.address;

	r.res.bannerPath = [r.res.bannerPath,r.res.bannerPath,r.res.bannerPath,r.res.bannerPath,r.res.bannerPath];

/*
  */      
        
        for(var als in allServices){
            
            if(parseInt(allServices[als].id)===parseInt(sid)){
           rr =  allServices[als];    
            }
        }
        
        

try{
//var rr = await entFunc.bitsStoreDet(sid);

             //       console.log('setting page info '+JSON.stringify(rr));

            //console.log('creating store page from '+'bitsoko' + indxPth);
            fs.readFile('/root/business' + indxPth, function(error, source) {
                
                if (getBitsWinOpt(req.url, 'pid')) {
        //this is a promotion
        var pid = getBitsWinOpt(req.url, 'pid');
        console.log('INFO! this is a promo link! '+pid);
    } else {
        //this is not a promotion
        var pid = false;
        
        console.log('INFO! this is not a promo link! '+getBitsWinOpt(req.url, 'pid'));
    }

                if (error) console.log(error);
                
                if(pid){
                    
                   // console.log(JSON.stringify(r.res.shopPromos));
                    
                    for(var i in r.res.shopPromos){
                        
                        if(JSON.stringify(r.res.shopPromos[i].id) == pid){
                            
                    var sName = r.res.shopPromos[i].promoName;
                    var sDesc = r.res.shopPromos[i].promoDesc;
                    var sImg = r.res.shopPromos[i].promoBanner;
                    
                        }
                        
                    }
                    
                    
                }else{
                   
                    var sName = rr.name;
                    var sDesc = rr.description;
                    var sImg = rr.banner;
                     
                }
//console.log(sName, sDesc, sImg);
                html2jade.convertHtml(source, {}, async function(err, jd) {
                    var thm = rr.theme;
                    if (thm == null || thm == 'null' || thm == '') {
                        thm = primaryColor;
                    }
                    var data = {
                        name: sName,
                        desc: sDesc,
                        img: sImg,
                        theme: thm,
                        stMeta: JSON.stringify(r),
                        cid: Cid
                    }
                    data.body = process.argv[2];
                    //jade.render
                    var template = jade.compile(jd);
                    var html = template(data);
                    //res.writeHead(200);

                    html = html.replace(/https:\/\/bitsoko.org\/bitsAssets/g, "/bitsAssets");
                    html = html.replace(/#{img}/g, sImg);
                    html = html.replace(/#{name}/g, sName);
                    html = html.replace(/#{desc}/g, sDesc);
                    html = html.replace(/#{theme}/g, thm);
                    
                    var store = data;
                
                    store.id = sid;
                  //entFunc.createManifest(store);
  
                await entFunc.adminDepsLoaded();     
                
                console.log('created manifest ',sid);


                            // lets inline the stylesheets to improve performance
                            fs.readFile('business/soko/css/style.css', function(error, source) {

                                if (error) console.log(error);
                                html = html.replace('<link href="css/style.css" media="screen,projection" rel="stylesheet" type="text/css">', '<style>' + source + '</style>')



                                console.log('!info saving admin page to ' + '/root/business/soko/index-' + sid + '.html');
                                //writeFile('/root/business/bits/index-' + sid + '.html', html, function(err) {
                                //    if (err) console.log('!ERR unable to write database client key', err);
                                resolve(html);
                                //});


                            })







                });
            });
            
    
}catch(err){
    console.log('err! cannot show merchant share info! merchant ' + sid + ' not found!', err);
}


});

}

exports.returnMerchantServices = function(bb, dataa) {

    var deferred = new Deferred();

    var priServ = parseInt(dataa.service);
    var priAcc = parseInt(dataa.id);
    if (priServ == 3) {
        var priServ = priAcc;
    }

for(sv in allServices){
    
    if(allServices[sv].id == priServ){
    
   // results = allServices[sv];

                    resu = allServices[sv];
                    try {
                        delete resu.banner
                    } catch (err) {
                        console.log(dataa.action + " error: banner image for " + priServ + " not removed");
                    }

                    ee=allInfo;
                    
                        //when(entFunc.productsByStore(resu), function(e) {
                            var e = allServices[sv].products;
                            var pAry = [];
                            for (var i in e) {
                                pAry.push(e[i].id);
                                delete e[i].image;
                            }


                            when(entFunc.merchantPromos(priServ, pAry), function(ep) {

                                var epp = [];

                                console.log('INFO: functions.js 165', ep);
                                for (var ix in ep) {
                                    try {
                                        if (ep[ix].promoStatus == 'active') {
                                            epp.push(ep[ix]);
                                        }
                                    } catch (err) {
                                        console.log(err);

                                    }

                                }
                                console.log('INFO: functions.js 176', epp);

                                delete resu.pushID;
                                resu.list = e;
                                resu.promotions = epp;

                                resu.icon = ee.icon;

                                resu.eName = ee.name;

                                resu.eDesc = ee.tagline;
                                resu.storeAddress = ee.address;
                                try {
                                    var dm = JSON.parse(resu.deliveryMembers);
                                    var locDt = resu.lonlat.split(',');
                                } catch (err) {
                                    var dm = [];
                                }
                                if (dm.length > 0 && resu.deliveryRate > 0 && locDt.length == 2) {

                                    resu.deliveries = "true";


                                } else {
                                    resu.deliveries = "false";

                                }
                                
                                //set deliveries to true by default
                                
                                    resu.deliveries = "true";

                                deferred.resolve({
                                    ret: bb,
                                    res: resu
                                });

                            }, function(error) {
                                console.log(error);
                            });


                       // }, function(error) {
                       //     console.log('error getting products at bits ', error);
                      //  });

                   




                //console.log(resu);

                //var result = {address: data.pubkey, privhash: data.privkey, userid: uid, type: data.type, username: data.name, pwallet: 'true'};



        }

        }

    return deferred;

}


exports.productsByStore = function(e) {

    var deferred = new Deferred();
    try {



        var sql = 'SELECT * FROM products WHERE owner=?';
        var sqlA = [parseInt(e.id)];
        try {
            var inclP = JSON.parse(e.retailing);
        } catch (err) {
            var inclP = [];
        }

        for (var j = 0, sqlA = sqlA, sql = sql; j < inclP.length; ++j) {

            sql = sql + ' OR id = ?';
            sqlA.push(inclP[j].id);

        }


        bsConn.mysql.query(sql, sqlA,
            function(err, results) {

                if (err) {
                    console.log(err);
                    return;
                }
                var retProducts = [];

                for (var j = 0; j < results.length; ++j) {

                    delete results[j].image
                    retProducts.push(results[j]);

                }

                if (retProducts.length > 0) {

                    deferred.resolve(retProducts);


                } else {
                    deferred.reject('no products!!?!');
                }

            });


    } catch (e) {
        deferred.reject(e);
    }


    return deferred;
}


exports.merchantOwner = function(d, e) {
	var deferred = new Deferred();
	try {
		d = parseInt(d);
		if (isNaN(d)) {
			deferred.reject({
				ret: e
			});
			return;
		}
		bsConn.mysql.query('SELECT * FROM users WHERE id = ?', [d], function(err, results) {
			if (err) {
				deferred.reject({
					ret: e
				});
				return;
			} else if (results.length == 0) {
				deferred.reject({
					ret: e
				});
				return;
			}
			//console.log(results);
			var c = {};
			var removeNewline = require('newline-remove');
			//removeNewline(results[0].googleMeta)
			//var str = JSON.stringify(results[0].googleMeta).replace(/\n/g, "\\\\n").replace(/\r/g, "\\\\r").replace(/\t/g, "\\\\t");
			try {
				//console.log(str);
				var str = removeNewline(results[0].googleMeta);
				try {
					c.cover = results[0].entBanner;
					//c.tagline = JSON.parse(str).tagline;
				} catch (err) {
					c.cover = 'not available';
					c.tagline = results[0].tagLine;
				}
				c.icon = JSON.parse(str).image;
				c.email = JSON.parse(str).email;
				c.address = results[0].wallets;
				c.contractRate = results[0].contractRate;
				c.phone = results[0].phoneNumber;
				c.name = JSON.parse(str).name;
				c.showTokens = results[0].showTokens;
				c.showManagers = results[0].showManagers;
				c.entIconList = results[0].entIconList;
				c.entImageList = results[0].entImageList;
				c.entAboutBody = results[0].entAboutBody;
				c.entAboutTitle = results[0].entAboutTitle;
				c.enterprised = results[0].enterprised;
				c.dbClientKey = results[0].dbClientKey;
				c.dbClientCert = results[0].dbClientCert;
				c.dbServerCA = results[0].dbServerCA;
				try {
					c.domains = JSON.parse(results[0].domains);
				} catch (err) {
					c.domains = JSON.parse('[]');
				}
				//console.log(str);
			} catch (e) {
				deferred.reject({
					ret: e
				});
				return;
			}
			deferred.resolve({
				res: c,
				ret: e
			});
		});
	} catch (e) {
		deferred.reject({
			ret: e
		});
	}
	return deferred;
}


exports.bitsStoreDet = function(d, e) {


    var deferred = new Deferred();

    //console.log(d);
    if (d == undefined) {
        deferred.reject('no store found');
        return;
    }

    bsConn.mysql.query('SELECT * FROM services WHERE id = ?',
        [JSON.parse(d)],
        function(err, results) {

            if (err || results.length == 0) {
                console.log(err);

                deferred.reject('no store found');
                return;
            }
            // console.log(results);
            var ret = results[0];
            ret.ret = e;
            deferred.resolve(ret);

        });



    return deferred;
}



exports.merchantPromos = function(e,f) {
	
	// e = store ID
    	// f = an array of retailing product ids to include as sponsored promos
	
	//TO-DO stop returning promotions with sponsored items
    
return new Promise(function(resolve, reject) {
  
  if(!Array.isArray(aPs)){
      aPs=[]
  }
  
    try{
       	
		
		try{
			if(f.length>0){
				var inclP=f;
			}else{
			var inclP=[];
			}
		}catch(err){
		var inclP=[];
		}
	     
   var retPromotions=[];
	 if (aPs.length==0){
	     var err="cant find any promotions";
	 console.log(err);
		 
		 
      resolve(retPromotions);
		 return;
	 }
	 
	
	
	 
	 var results=[]; 	 
		 
	for(var iii in aPs){
		var hasSpPr=false;
	
		try{
		
		var hldSpPro=JSON.parse(aPs[iii].items);
		}catch(e){
		var hldSpPro=[];
			
		console.log('skipped adding promo ',aPs[iii].items,e);
		continue;
			
		}
		
		function idd(a, b){
			var d = {};
    var results = [];
    for (var i = 0; i < b.length; i++) {
        d[b[i]] = true;
    }
    for (var j = 0; j < a.length; j++) {
        if (d[a[j]]) 
            results.push(a[j]);
    }
    return results;
			
}
		
		if(inclP==undefined || inclP=='undefined' || hldSpPro==undefined || hldSpPro=='undefined' || aPs[iii].status=='inactive' ){
		    //some error checking, checks if included items or promo items are invalid, or if promo is inactive
		continue;
		}
	
		//if( parseInt(resul[iii].owner)==parseInt(e) || idd(JSON.parse(hldSpPro),inclP).length > 0){
		if( parseInt(aPs[iii].owner)==parseInt(e)){
		//console.log('matched ',idd(hldSpPro,inclP));
	results.push(aPs[iii]);
			
		}
	
		
	}	 
		 
	
     	 
      if(results.length==0){
      resolve(retPromotions);
		 return;  
      } 
	
     for(var i = 0,retPromotions=retPromotions; i < results.length; ++i) {
	     	
		
         var proBanner={};
			var idiscount=results[i].discount;
			   if(idiscount=='' || idiscount== 0 || idiscount== '0'){
			 idiscount=0;
			 discountDisabled=false;
			   }else{
			       
			 discountDisabled=true;
			   }
			   var msg=results[i].msg;
			
			   var nm=results[i].name;
			//   if(nm==''){
			 //nm=f.names;  
			 //  }
			var sbs = results[i].subscribers;
			
			try{
			sbs=JSON.parse(sbs);
				if(sbs.length==0)sbs = [];
			}catch(err){
				var sbs = [];
			}
			
         proBanner.id=results[i].id;
         proBanner.discountDisabled=discountDisabled;
         proBanner.discount=idiscount;
         proBanner.promoName=nm;
         proBanner.promoBanner='https://bitsoko.org'+results[i].customImagePath;
         proBanner.promoPrice=results[i].totPrice;
         proBanner.promoOwner=results[i].owner;
         proBanner.promoDeliverable=results[i].deliverable;
         proBanner.promoStart=results[i].startDate;
         proBanner.promoStop=results[i].stopDate;
	     //start TODO: fix this hack 
         proBanner.promoItems=results[i].items.replace('"','').replace('"','');
	     //end TODO:
         proBanner.promoDesc=msg;
         proBanner.promoSubs=sbs;
         proBanner.promoStatus=results[i].status;
         proBanner.promoLogo='https://bitsoko.org/bitsAssets/img/promologo.png';
             
         retPromotions.push(proBanner);  
    if(results.length==retPromotions.length){
    
     resolve(retPromotions);
    }
             /* 
	    
    
    
}, function(error){
    
      reject("error fetching promotions: "+error); 
}); 
      
	
	
	
	
	
	
         deferred.resolve(results); 
	 */       
     } 
     		
		      
          
    }catch(e){
	    console.log(e);
       reject(e); 
    }
    
});
}



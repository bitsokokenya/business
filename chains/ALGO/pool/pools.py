from tinyman.v1.client import TinymanTestnetClient, TinymanMainnetClient
from algosdk import account, encoding , mnemonic
from bootstrap import prepare_bootstrap_transactions
from algosdk.v2client import algod

from tinyman.v1.pools import Pool
from tinyman.assets import Asset
from tinyman.utils import wait_for_confirmation
from algosdk.v2client.algod import AlgodClient
#from tinyman.v1.client import TinymanClient

from algosdk.v2client.algod import AlgodClient
from client import TinymanClient

import sys


def add_liquidity(client, account, amount, aID1, aID2):
    
    #client = TinymanTestnetClient(user_address=account['address'])
    # By default all subsequent operations are on behalf of user_address
    
    # Fetch our two assets of interest
    BTC = client.fetch_asset(aID1)
    ASA = client.fetch_asset(aID2)
    
    # Fetch the pool we will work with
    pool = client.fetch_pool(ASA, BTC)
    # Get a quote for supplying liquidity 10 token and 500 satoshis
    quote = pool.fetch_mint_quote(ASA(1_000_000_0), BTC(1_0), slippage=0.01)
    print(quote)
    #return
    excess = pool.fetch_excess_amounts()
    if pool.liquidity_asset in excess:
        amount = excess[pool.liquidity_asset]
        print(f'Excess: {amount}')
    
    # Check if we are happy with the quote..
    #if quote.amounts_in[BTC] < 5_000_000:
    # Prepare the mint transactions from the quote and sign them
    transaction_group = pool.prepare_mint_transactions_from_quote(quote, account['address'])
    transaction_group.sign_with_private_key(account['address'], account['private_key'])
    #result = client.submit(transaction_group, wait=True)
    txid = client.algod.send_transactions(transaction_group.signed_transactions)
    #wait_for_confirmation(txid)
    
            # Check if any excess liquidity asset remaining after the mint
    excess = pool.fetch_excess_amounts()
    if pool.liquidity_asset in excess:
        amount = excess[pool.liquidity_asset]
        transaction_group = pool.prepare_redeem_transactions(amount)
        transaction_group.sign_with_private_key(account['address'], account['private_key'])
        result = client.submit(transaction_group, wait=True)
    info = pool.fetch_pool_position()
    share = info['share'] * 100
    print(f'Pool Tokens: {info[pool.liquidity_asset]}')
    print(f'Assets: {info[TINYUSDC]}, {info[ALGO]}')
    print(f'Share of pool: {share:.3f}%')

action = sys.argv[1]
#

if action == "boot":
    pooler_address = sys.argv[4]
    aID1 = int(sys.argv[2])
    aID2 =  int(sys.argv[3])
    mne = sys.argv[5]
    priv_key = mnemonic.to_private_key(mne)
    account = {
        'address': pooler_address,
        'private_key': priv_key,
    }
    client = TinymanMainnetClient(user_address=account['address'])
    if(not client.is_opted_in(account['address'])):
        print('Account not opted into app, opting in now..')
        transaction_group = client.prepare_app_optin_transactions(account['address'])
        for i, txn in enumerate(transaction_group.transactions):
            if txn.sender == account['address']:
                transaction_group.signed_transactions[i] = txn.sign(account['private_key'])
        txid = client.algod.send_transactions(transaction_group.signed_transactions)
        #wait_for_confirmation(txid)
        
    if(not client.asset_is_opted_in(aID1, account['address'])):
        print('Account not opted into asset {aID1}, opting in now..')
        transaction_group = client.prepare_asset_optin_transactions(aID1, account['address'])
        for i, txn in enumerate(transaction_group.transactions):
            if txn.sender == account['address']:
                transaction_group.signed_transactions[i] = txn.sign(account['private_key'])
        txid = client.algod.send_transactions(transaction_group.signed_transactions)
        #wait_for_confirmation(txid)
    '''    
    if(not client.asset_is_opted_in(aID2)):
        print('Account not opted into asset {aID2}, opting in now..')
        transaction_group = client.prepare_asset_optin_transactions(aID1, account['address'])
        for i, txn in enumerate(transaction_group.transactions):
            if txn.sender == account['address']:
                transaction_group.signed_transactions[i] = txn.sign(account['private_key'])
        txid = client.algod.send_transactions(transaction_group.signed_transactions)
        #wait_for_confirmation(txid)
    '''
    # Fetch our two assets of interest
    TINYUSDC = client.fetch_asset(aID1)
    ALGO = client.fetch_asset(aID2)
    
    
    # Fetch the pool we will work with
    pool = client.fetch_pool(TINYUSDC, ALGO)
    transaction_group = pool.prepare_bootstrap_transactions(pooler_address=account['address'])
    transaction_group.sign_with_private_key(account['address'], account['private_key'])
    result = client.submit(transaction_group, wait=True)
    
    pool = client.fetch_pool(TINYUSDC, ALGO)
    pool.refresh()
    r = pool.info()
    if(not client.asset_is_opted_in(r['liquidity_asset_id'], account['address'])):
        print(f"Account not opted into asset {r['liquidity_asset_id']}, opting in now..")
        transaction_group = client.prepare_asset_optin_transactions(r['liquidity_asset_id'], account['address'])
        transaction_group.sign_with_private_key(account['address'], account['private_key'])
        client.submit(transaction_group, wait=True)
        #wait_for_confirmation(txid)
    print(r)

if action == "poolState":
    pooler_address = sys.argv[4]
    aID1 = int(sys.argv[2])
    aID2 = int(sys.argv[3])
    #Pool.fetch_state(AlgodTestnetClient, pooler_address)
    #Pool.__init__(AlgodTestnetClient, '', aID1, aID2, pooler_address)
    print(get_pool_info(AlgodTestnetClient, Pool.fetch_state(AlgodTestnetClient, pooler_address), aID1, aID2 ))
    #Pool.fetch_pool_position(AlgodTestnetClient, pooler_address)
if action == "accState":
    pooler_address = sys.argv[2]
    Pool.prep_acc_state(AlgodTestnetClient, pooler_address)
if action == "swap":
    pooler_address = sys.argv[5]
    mne = sys.argv[6]
    aID1 = int(sys.argv[2])
    aID2 =  int(sys.argv[3])
    amount =  sys.argv[4]
    #swap(aID1,aID2,pooler_address,mne,amount)
    
if action == "quote":
    client = TinymanMainnetClient()
    
    aID1 = int(sys.argv[2])
    aID2 =  int(sys.argv[3])

    # Fetch our two assets of interest
    ASA = client.fetch_asset(aID1)
    BTC = client.fetch_asset(aID2)
    
    # Fetch the pool we will work with
    pool = client.fetch_pool(ASA, BTC)

    # Get a quote for a swap of 1 ALGO to TINYUSDC with 1% slippage tolerance
    quote = pool.fetch_fixed_input_swap_quote(ALGO(1_000_000), slippage=0.01)
    print(quote)
    print(f'ASA per BTC: {quote.price}')
    print(f'ASA per BTC (worst case): {quote.price_with_slippage}')
if action == "pooling":
    pooler_address = sys.argv[4]
    mne = sys.argv[5]
    aID1 = int(sys.argv[2])
    aID2 =  int(sys.argv[3])
    
    priv_key = mnemonic.to_private_key(mne)
    account = {
        'address': pooler_address,
        'private_key': priv_key,
    }


    client = TinymanMainnetClient(user_address=account['address'])
    # By default all subsequent operations are on behalf of user_address
    if(not client.is_opted_in(account['address'])):
        print('Account not opted into app, opting in now..')
        transaction_group = client.prepare_app_optin_transactions(account['address'])
        for i, txn in enumerate(transaction_group.transactions):
            if txn.sender == account['address']:
                transaction_group.signed_transactions[i] = txn.sign(account['private_key'])
        txid = client.algod.send_transactions(transaction_group.signed_transactions)
        wait_for_confirmation(txid)
    
    # Fetch our two assets of interest
    ASA = client.fetch_asset(aID1)
    BTC = client.fetch_asset(aID2)
    
    # Fetch the pool we will work with
    pool = client.fetch_pool(ASA, BTC)
    
    info = pool.fetch_pool_position()
    share = info['share'] * 100
    print(f'Pool Tokens: {info[pool.liquidity_asset]}')
    print(f'Assets: {info[ASA]}, {info[BTC]}')
    print(f'Share of pool: {share:.3f}%')
if action == "addLiq":
    aID1 = int(sys.argv[2])
    aID2 =  int(sys.argv[3])
    pooler_address = sys.argv[4]
    mne = sys.argv[5]
    amount = int(sys.argv[6])
    
    priv_key = mnemonic.to_private_key(mne)
    account = {
        'address': pooler_address,
        'private_key': priv_key,
    }
    client = TinymanMainnetClient(user_address=account['address'])
    

    # Fetch our two assets of interest
    ALGO = client.fetch_asset(aID2)
    TINYUSDC = client.fetch_asset(aID1)
    
    
    # Fetch the pool we will work with
    pool = client.fetch_pool(TINYUSDC, ALGO)
    if(not client.is_opted_in(account['address'])):
        print('Account not opted into app, opting in now..')
        transaction_group = client.prepare_app_optin_transactions(account['address'])
        for i, txn in enumerate(transaction_group.transactions):
            if txn.sender == account['address']:
                transaction_group.signed_transactions[i] = txn.sign(account['private_key'])
        txid = client.algod.send_transactions(transaction_group.signed_transactions)
        #wait_for_confirmation(txid)
        
    if(not client.asset_is_opted_in(aID1, account['address'])):
        print('Account not opted into asset {aID1}, opting in now..')
        transaction_group = client.prepare_asset_optin_transactions(aID1, account['address'])
        for i, txn in enumerate(transaction_group.transactions):
            if txn.sender == account['address']:
                transaction_group.signed_transactions[i] = txn.sign(account['private_key'])
        txid = client.algod.send_transactions(transaction_group.signed_transactions)
        #wait_for_confirmation(txid)
        
    if(not client.asset_is_opted_in(aID2)):
        #print('Account not opted into asset {aID2}, opting in now..')
        transaction_group = client.prepare_asset_optin_transactions(aID1, account['address'])
        for i, txn in enumerate(transaction_group.transactions):
            if txn.sender == account['address']:
                transaction_group.signed_transactions[i] = txn.sign(account['private_key'])
        txid = client.algod.send_transactions(transaction_group.signed_transactions)
        #wait_for_confirmation(txid)
        
        
    
    

    
    #transaction_group = pool.prepare_bootstrap_transactions()
    #transaction_group.sign_with_private_key(account['address'], account['private_key'])
    #result = client.submit(transaction_group, wait=True)
    #print(result)
    add_liquidity(client, account, amount, aID1, aID2)

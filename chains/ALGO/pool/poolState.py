from tinyman.v1.client import TinymanTestnetClient
from algosdk import mnemonic

import logging

logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
  
logger.debug(mnemonic)

import sys

import re

#split string by single space
#mnem = re.split(' +', mnemonic)

# Hardcoding account keys is not a great practice. This is for demonstration purposes only.
# See the README & Docs for alternative signing methods.
account = {
    'address': sys.argv[1],
#    'private_key': mnem.split(' ').to_private_key(sys.argv[1]),
}

client = TinymanTestnetClient(user_address=account['address'])
# By default all subsequent operations are on behalf of user_address

# Fetch our two assets of interest
ASA = client.fetch_asset(int(sys.argv[3]))
ALGO = client.fetch_asset(0)

# Fetch the pool we will work with
pool = client.fetch_pool(ASA, ALGO)

info = pool.fetch_pool_position()
share = info['share'] * 100
print(f'Pool Tokens: {info[pool.liquidity_asset]}')
print(f'Assets: {info[ASA]}, {info[ALGO]}')
print(f'Share of pool: {share:.3f}%')

# This sample is provided for demonstration purposes only.
# It is not intended for production use.
# This example does not constitute trading advice.

from tinyman.v1.client import TinymanTestnetClient
import sys
from algosdk import mnemonic

# Hardcoding account keys is not a great practice. This is for demonstration purposes only.
# See the README & Docs for alternative signing methods.
account = {
    'address': sys.argv[1],
    'private_key': mnemonic.to_private_key(sys.argv[2]), 
}

client = TinymanTestnetClient(user_address=account['address'])
# By default all subsequent operations are on behalf of user_address

# Fetch our two assets of interest
ASA = client.fetch_asset(int(sys.argv[3]))
ALGO = client.fetch_asset(0)

# Fetch the pool we will work with
pool = client.fetch_pool(ASA, ALGO)

# Get a quote for supplying 1000.0 TinyUSDC
quote = pool.fetch_mint_quote(ALGO(1), slippage=0.01)

print(quote)

# Check if we are happy with the quote..
##if quote.amounts_in[ALGO] < 5_000_000:
    # Prepare the mint transactions from the quote and sign them
##    transaction_group = pool.prepare_mint_transactions_from_quote(quote)
##    transaction_group.sign_with_private_key(account['address'], account['private_key'])
##    result = client.submit(transaction_group, wait=True)

    # Check if any excess liquidity asset remaining after the mint
##    excess = pool.fetch_excess_amounts()
##    if pool.liquidity_asset in excess:
##        amount = excess[pool.liquidity_asset]
##        print(f'Excess: {amount}')
##        if amount > 1_000_000:
##            transaction_group = pool.prepare_redeem_transactions(amount)
##            transaction_group.sign_with_private_key(account['address'], account['private_key'])
##            result = client.submit(transaction_group, wait=True)

info = pool.fetch_pool_position()
share = info['share'] * 100
print(f'Pool Tokens: {info[pool.liquidity_asset]}')
print(f'Assets: {info[ASA]}, {info[ALGO]}')
print(f'Share of pool: {share:.3f}%')
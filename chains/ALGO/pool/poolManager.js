var exports = module.exports = {};

let {PythonShell} = require('python-shell');

algosdk = require("algosdk");
    /*

    const key = algosdk.generateAccount();
    const mnemonic = algosdk.secretKeyToMnemonic(key.sk);

    console.log(key.addr);
    return { address: key.addr, mnemonic, sk: key.sk };

const baseServer = "https://testnet-algorand.api.purestake.io/idx2";
const port = "";

const token = {
    'X-API-key' : 'qe3CNhIJ3C5DXrGHlfxAU1aIUt7SrYzNMwHz8gKb ',
}

let indexerClient = new algosdk.Indexer(token, baseServer, port);

    let blockInfo = await indexerClient.lookupBlock(5).do()
    console.log(blockInfo)
    console.log(indexerClient)
*/


exports.boot = async function(aID1, aID2, pub, mnemonic) {

return new Promise(async function(resolve, reject) {

  let options = {
 // mode: 'text',
  //pythonPath: 'path/to/python',
 // pythonOptions: ['-u'], // get print results in real-time
 // scriptPath: 'path/to/my/scripts',
  args: ['boot', aID1, aID2, pub, mnemonic]
};

PythonShell.run('/root/business/chains/ALGO/pool/pools.py', options, function (err, results) {
   // console.log(typeof err.message);
  if (err && !err.message.includes('has already opted in to app')) {
     
      console.error('ERROR! unable to boot a new pool! '+err);
      reject(err);
      
  }else if (err && err.message.includes('has already opted in to app')){
      
       console.log('INFO! pool initialized/bootstrapped succesfully! ');
  // results is an array consisting of messages collected during execution
  
    resolve(err);
  }else{
      
       console.log('INFO! pool initialized/bootstrapped succesfully! ');
  // results is an array consisting of messages collected during execution
  console.log('results: %j', results);
  
    resolve(results);
  }
  
});

});


}


exports.bootB = async function(aID1, aID2, pub, mnemonic, assetNM) {
//   ['/data/lightning-bits/src/chains/algo/pools.py', 'pooling', 21582668, 0, store.settings.chains.algo.account.address ]

return new Promise(async function(resolve, reject) {

  let options = {
 // mode: 'text',
  //pythonPath: 'path/to/python',
 // pythonOptions: ['-u'], // get print results in real-time
 // scriptPath: 'path/to/my/scripts',
  args: ['bootb', aID1, aID2, assetNM, 'BTC:sats', pub]
};

PythonShell.run('/root/business/chains/ALGO/pool/poolCreate.py', options, function (err, results) {
   // console.log(typeof err.message);
  if (err && !err.message.includes('has already opted in to app')) {
     
      console.error('ERROR! unable to boot a new pool! '+err);
      reject(err);
      
  }else if (err && err.message.includes('has already opted in to app')){
      
       console.log('INFO! pool initialized/bootstrapped succesfully! ');
  // results is an array consisting of messages collected during execution
  
    resolve(err);
  }else{
      
       console.log('INFO! pool initialized/bootstrapped succesfully! ');
  // results is an array consisting of messages collected during execution
  console.log('results: %j', results);
  
    resolve(results);
  }
  
});

});

}

exports.get = async function(pub, mnemonic, assetID) {
return new Promise(async function(resolve, reject) {

  let options = {
 // mode: 'text',
  //pythonPath: 'path/to/python',
 // pythonOptions: ['-u'], // get print results in real-time
 // scriptPath: 'path/to/my/scripts',
 // args: [validator_app_id, asset1_id, asset2_id, asset1_unit_name, 'BTC', sender, suggested_params]
//  args: ['pooling', assetID, 1942776, pub, mnemonic]
  args: ['pooling', assetID, 0, pub, mnemonic]
};

PythonShell.run('/root/business/chains/ALGO/pool/pools.py', options, function (err, results) {
  if (err) {
     
      console.error('ERROR! unable to boot a new pool! '+err);
      reject(err);
      
  }else{
      
       console.log('INFO! added pool');
  // results is an array consisting of messages collected during execution
  console.log('results: %j', results);
  
    resolve(results);
  }
  
});

});
}


/*
exports.createe = async function(pub, mnemonic, assetID, assetNM, vID) {

const baseServer = "https://testnet-algorand.api.purestake.io/idx2";
const port = "";

const token = {
    'X-API-key' : 'qe3CNhIJ3C5DXrGHlfxAU1aIUt7SrYzNMwHz8gKb ',
}

let indexerClient = new algosdk.Indexer(token, baseServer, port);

    let blockInfo = await indexerClient.lookupBlock(5).do()
    console.log(blockInfo)
    console.log(indexerClient)

return new Promise(async function(resolve, reject) {

  let options = {
 // mode: 'text',
  //pythonPath: 'path/to/python',
 // pythonOptions: ['-u'], // get print results in real-time
 // scriptPath: 'path/to/my/scripts',
 // args: [validator_app_id, asset1_id, asset2_id, asset1_unit_name, 'BTC', sender, suggested_params]
  args: [vID, assetID, 1942777, assetNM, 'BTC', pub]
};

console.log(options.args);
PythonShell.run('/root/business/chains/ALGO/pool/poolCreate.py', options, function (err, results) {
  if (err) {
     
      console.error('ERROR! unable to build a new pool! '+err);
      reject(err);
      
  }else{
      
       console.log('INFO! added pool');
  // results is an array consisting of messages collected during execution
  console.log('results: %j', results);
  
    resolve(results);
  }
  
});

});


}


*/
exports.get = async function(pub, mnemonic, assetID) {

  


return new Promise(async function(resolve, reject) {

  let options = {
 // mode: 'text',
  //pythonPath: 'path/to/python',
 // pythonOptions: ['-u'], // get print results in real-time
 // scriptPath: 'path/to/my/scripts',
  args: [pub, mnemonic, assetID, 1]
};

PythonShell.run('/root/business/chains/ALGO/pool/poolState.py', options, function (err, results) {
  if (err) {
      console.log('INFO! pool not bootstrapped: '+err);
     
    PythonShell.run('/root/business/chains/ALGO/pool/poolAuth.py', options, function (err, results) {
         if (err) {
             console.log('INFO! ',err);
                reject(err);
          }else{
      
      
         // results is an array consisting of messages collected during execution
         console.log('results: %j', results);
         
         // retring to get state
    PythonShell.run('/root/business/chains/ALGO/pool/poolState.py', options, function (err, results) {
         if (err) {
             console.log('ERROR! unable to initiate pool');
      reject(err);
          }else{
      
      
         // results is an array consisting of messages collected during execution
         console.log('results: %j', results);
  
           resolve(results);
         }
  
        });

         
         
         
         
         
         
  
           return results;
         }
  
        });
      
  }else{
      
      
  // results is an array consisting of messages collected during execution
  console.log('results: %j', results);
  
    return results;
  }
  
});

});

}

exports.add = async function(pub, mnemonic, assetID, amount) {
    

return new Promise(async function(resolve, reject) {
    let options = {
 // mode: 'text',
  //pythonPath: 'path/to/python',
 // pythonOptions: ['-u'], // get print results in real-time
 // scriptPath: 'path/to/my/scripts',
  args: ['addLiq', 386192725, assetID, pub, mnemonic, amount]
};

PythonShell.run('/root/business/chains/ALGO/pool/pools.py', options, function (err, results) {
  if (err) {
     
      console.log('INFO! pool not found');
      
      reject(err)
      
  }else{
      
       console.log('INFO! adding first pooler');
  // results is an array consisting of messages collected during execution
  console.log('results: %j', results);
  
    resolve(results);
  }
  
});

});


}

exports.remove = async function(pub, mnemonic, assetID, amount) {

    let options = {
 // mode: 'text',
  //pythonPath: 'path/to/python',
 // pythonOptions: ['-u'], // get print results in real-time
 // scriptPath: 'path/to/my/scripts',
  args: [pub, mnemonic, assetID, amount]
};

PythonShell.run('/root/business/chains/ALGO/pool/poolRemove.py', options, function (err, results) {
  if (err) throw err;
  // results is an array consisting of messages collected during execution
  console.log('results: %j', results);
  
    return results;
});


}

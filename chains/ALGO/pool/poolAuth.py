from algosdk.error import AlgodHTTPError
from algosdk.future.transaction import wait_for_confirmation
from algosdk.v2client import algod

from tinyman.v1.client import TinymanTestnetClient
import sys

tinyman_client = TinymanTestnetClient(user_address=sys.argv[1])
from algosdk import mnemonic
print(sys.argv[1])

def get_quote(pool, asset_amount, slipagge):
    return pool.fetch_fixed_input_swap_quote(asset_amount, slipagge)
    
def connect():
    """Instantiate and return Algod client object""" 
    algod_address = "https://testnet-algorand.api.purestake.io/ps2"
    
    #algod_token = os.environ(['PURESTAKE_API_TOKEN'])
    algod_token = "qe3CNhIJ3C5DXrGHlfxAU1aIUt7SrYzNMwHz8gKb"
    headers = {
        "X-API-Key": algod_token,
    }
    return algod.AlgodClient(algod_token, algod_address, headers)



AlgodTestnetClient = connect()

def redeem_excess_amount(pool, asset):
    excess = pool.fetch_excess_amounts()
    if asset in excess:
        amount = excess[asset]
        txn_group = pool.prepare_redeem_transactions(amount)
        txn_group.sign_with_private_key(sys.argv[1], mnemonic.to_private_key(sys.argv[2]))
        result = tinyman_client.submit(txn_group, wait=True)
        return result
             
def submit_transaction(txn_group):
    try:
        txid = AlgodTestnetClient.send_transactions(txn_group.signed_transactions)
    except AlgodHTTPError as e:
        return str(e)
    return wait_for_confirmation(AlgodTestnetClient, txid, 10)


if(not tinyman_client.is_opted_in):
    txn_group = AlgodTestnetClient.prepare_app_optin_transactions()
    txn_group.sign_with_private_key(sys.argv[1], mnemonic.to_private_key(sys.argv[2]))
    submit_transaction(txn_group)
        
if not tinyman_client.asset_is_opted_in(int(sys.argv[3])):
    txn_group = tinyman_client.prepare_asset_optin_transactions(int(sys.argv[3]))
    txn_group.sign_with_private_key(sys.argv[1], mnemonic.to_private_key(sys.argv[2]))
    submit_transaction(txn_group)

ASA = tinyman_client.fetch_asset(int(sys.argv[3]))
ALGO = tinyman_client.fetch_asset(0)
        
pool = tinyman_client.fetch_pool(ASA, ALGO)
slippage_percentage=0.01

print(pool, ALGO(int(sys.argv[4])), slippage_percentage)
quote = get_quote(pool, ALGO(int(sys.argv[4])), slippage_percentage)
print(quote)
#txn_group = prepare_swap_transaction(quote, pool)
#txn_group = submit_transaction(txn_group)

redeem_excess_amount(pool, ASA)
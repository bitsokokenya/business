from tinyman.v1.client import TinymanTestnetClient
import sys
print(sys.argv[0])

# Hardcoding account keys is not a great practice. This is for demonstration purposes only.
# See the README & Docs for alternative signing methods.
account = {
    'address': sys.argv[0],
    'private_key': sys.argv[1], # Use algosdk.mnemonic.to_private_key(mnemonic) if necessary
}

client = TinymanTestnetClient(user_address=account['address'])
# By default all subsequent operations are on behalf of user_address

# Fetch our two assets of interest
TINYUSDC = client.fetch_asset(sys.argv[2])
ALGO = client.fetch_asset(0)

# Fetch the pool we will work with
pool = client.fetch_pool(TINYUSDC, ALGO)

info = pool.fetch_pool_position()
share = info['share'] * 100
print(f'Pool Tokens: {info[pool.liquidity_asset]}')
print(f'Assets: {info[TINYUSDC]}, {info[ALGO]}')
print(f'Share of pool: {share:.3f}%')
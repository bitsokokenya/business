var exports = module.exports = {};

algosdk = require("algosdk");


exports.create = async function() {
    

    const key = algosdk.generateAccount();
    const mnemonic = algosdk.secretKeyToMnemonic(key.sk);

    console.log(key.addr);
    return { address: key.addr, mnemonic, sk: key.sk };

}



exports.get = async function(mnemonic) {

const key = algosdk.mnemonicToSecretKey(mnemonic);

    if (!algosdk.isValidAddress(key.addr)) {
      throw new Error("Invalid wallet mnemonic!");
    }

    return { address: key.addr, mnemonic, sk: key.sk };
}
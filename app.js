['log', 'warn', 'error'].forEach((methodName) => {
  const originalMethod = console[methodName];
  console[methodName] = (...args) => {
    let initiator = 'unknown place';
    try {
      throw new Error();
    } catch (e) {
      if (typeof e.stack === 'string') {
        let isFirst = true;
        for (const line of e.stack.split('\n')) {
          const matches = line.match(/^\s+at\s+(.*)/);
          if (matches) {
            if (!isFirst) { // first line - current function
                            // second line - caller (what we are looking for)
              initiator = matches[1];
              break;
            }
            isFirst = false;
          }
        }
      }
    }
    originalMethod.apply(console, [...args, '\n', `  at ${initiator}`]);
  };
});


vm = require('vm');
fetch = require('node-fetch');
moment = require('moment');
var express = require('express');
var app = express();
jade = require('pug');
//var LE = require('greenlock');
var insPORT = 18083;
var PORT = 443;
allDomains = [];
accountObject={};
invoiceArray =[];

primaryColor = '#0f5f76';

heartBeat = 20000;
var bitsokoEmail = 'bitsokokenya@gmail.com';

mainTorDomain = 'http://5vqqy3gbgjbjyxn5xz2uoqqol7vkzidqarvxti7wfrluc6sfedo577qd.onion';
mainDomain = 'https://gateway.bitsoko.org';
mainTorPipe = 'http://127.0.0.1:18080';
var dbDomain = '172.17.0.1';
var compress = require('compression');
nCmd = require('node-cmd');
https = require('https');
jsonfile = require('jsonfile');
ffmpg = require('@ffmpeg/ffmpeg');

axios = require('axios');
//gulp = require('gulp');
//conCat = require('gulp-concat');


waitPort = require('wait-port');


RosApi = require('node-routeros').RouterOSAPI;
mysql = require('mysql');
request = require("request");
imgDownloader = require('image-downloader');
var forceSSL = require('express-force-ssl');
fs = require('fs');
fsA = require('fs/promises');
when = require("promised-io/promise").when;
Deferred = require("promised-io/promise").Deferred;
gcm = require('node-gcm');
googlePushKey = 'AAAAbt9hX9o:APA91bE-V876epaCGolDrXSsbb0gXlnLM46BqAU-3H9MudpCru6pbEXaNHW5OBiNgDvDbNShnQo3Q3PMRicmp2itH7tW0IYU83i3WNgPdW_5zZHjVrJlGy9RwhUA7aX-PAMYWhrqh7qP5yF9LRseM34ILObz9V4vYA';
html2jade = require('html2jade');
Promise = require('promise');
//bleno = require('bleno');
var util = require('util');
Downloader = require("nodejs-file-downloader");

let {PythonShell} = require('python-shell');
var connectedUsers = {};

function bleInit(){
    

var BlenoPrimaryService = bleno.PrimaryService;

var BlenoCharacteristic = bleno.Characteristic;

var EchoCharacteristic = function() {
  EchoCharacteristic.super_.call(this, {
    uuid: 'ec0e',
    properties: ['read', 'write', 'notify'],
    value: null
  });

  this._value = new Buffer(0);
  this._updateValueCallback = null;
};

util.inherits(EchoCharacteristic, BlenoCharacteristic);

EchoCharacteristic.prototype.onReadRequest = function(offset, callback) {
  console.log('EchoCharacteristic - onReadRequest: value = ' + this._value.toString('hex'));

  callback(this.RESULT_SUCCESS, this._value);
};

EchoCharacteristic.prototype.onWriteRequest = function(data, offset, withoutResponse, callback) {
  this._value = data;

  console.log('EchoCharacteristic - onWriteRequest: value = ' + this._value.toString('hex'));

  if (this._updateValueCallback) {
    console.log('EchoCharacteristic - onWriteRequest: notifying');

    this._updateValueCallback(this._value);
  }

  callback(this.RESULT_SUCCESS);
};

EchoCharacteristic.prototype.onSubscribe = function(maxValueSize, updateValueCallback) {
  console.log('EchoCharacteristic - onSubscribe');

  this._updateValueCallback = updateValueCallback;
};

EchoCharacteristic.prototype.onUnsubscribe = function() {
  console.log('EchoCharacteristic - onUnsubscribe');

  this._updateValueCallback = null;
};


bleno.on('stateChange', function(state) {
  console.log('on -> stateChange: ' + state);

  if (state === 'poweredOn') {
    ble.nostartAdvertising('echo', ['ec00']);
  } else {
    bleno.stopAdvertising();
  }
});

bleno.on('advertisingStart', function(error) {
  console.log('on -> advertisingStart: ' + (error ? 'error ' + error : 'success'));

  if (!error) {
    bleno.setServices([
      new BlenoPrimaryService({
        uuid: 'ec00',
        characteristics: [
          new EchoCharacteristic()
        ]
      })
    ]);
  }
});
    
    
}

//bleInit()

try{
//disable database function
throw 'database disabled';
//database credentials
//TO-DO
//add support for mongo and postgres

//these are default and will be changed on first install
dbHost = dbDomain;
dbUser = 'root';
dbPass = 'password';
dbName = 'bitsoko';


//Database support
 connectionSQL = require("/root/business/libs/database.js").getClient();
bsConn = {
      /*                          
  maria: function (){

     var q= new Client({
  host: dbHost,
  user: dbUser,
  password: dbPass,
  db: 'bitsoko'
});
      q.on("error", function (err) {
    console.log('connection error:', err);
    });
      q.connect();
      return q;

                    }(),
    */
    mysql: connectionSQL

 }
 
 bsConn.mysql.query('SELECT 1');
 
}catch(err){
      // Well, we tried. The database is unavailable.
          console.log('database failed:',err);
       // process.exit(1);
      
}
//Global variables
stores = []; //all stores
aPs=[]; //all promotions

allInfo={};
                


var socketC = require('socket.io-client').connect(mainDomain,{secure: true, rejectUnauthorized: false});


console.log(process.argv);

sokoFlags = JSON.parse(decodeURIComponent(process.argv[2]));
//to enable google logins
Cid = '';//eg 476.............................ls.apps.googleusercontent.com
writeFile = require('write');
suppliesAssets=[{"name":"example item","added":1512852341380}];

//variables
allManagers = [];
allNewManagers = [];
allPromos = [];
allProdCat = [];
allServices= [];
var doocaPG = '';

// Change to default ip 10.0.0.1
//
vRosIP = '192.168.88.1'; 


// open tor to bitsoko



/*
 try{
    var udCmd=
        `
            export GOPATH=/go && export PATH=$GOPATH/bin:/usr/local/go/bin:$PATH ; onionpipe client new soko ; onionpipe 5vqqy3gbgjbjyxn5xz2uoqqol7vkzidqarvxti7wfrluc6sfedo577qd.onion:80~0.0.0.0:18080
        `; 
	 
	 
	    
    nCmd.get(udCmd,function(data, err, stderr){
            if (!err) {
		    var hMsg ='INFO! bitsoko tor online';
               console.log(hMsg,data);
		    return;
	   
            } else {
       console.warn('ERROR! Bitsoko Tor offline!!! ',err); 
            }
            
        });
 }catch(err){
    
       console.warn('ERROR! Bitsoko Tor error!!! ',err);  
 }

*/
// console.log('INFO! dooca gateway password: '+sokoFlags.dgp);
RosConn = new RosApi({
    host: vRosIP,
    user: 'admin',
    //password: sokoFlags.dgp,
    password: '',
    keepalive: true
});
//Load Service Dependencies

//Globally available helper scripts
entFunc = require("/root/business/libs/enterpriseFunctions.js");

//Doocanet Gateway Manager - updates gateway for managing internet access for customers
serverManager = require("/root/business/bots/doocaManager.js").init();

//Server Cleaner - manages logs and reporting
serverManager = require("/root/business/bots/serverManager.js").init();

//Messaging support - sms and notifications
messageManager = require("/root/business/libs/messageManager.js");

//Contract support - contract information
contractManager = require("/root/business/libs/messageManager.js");
//Load bots

//this bots manages the pending and delivering orders
orderManager = require("/root/business/bots/orderManager.js").init();

serverFiles = [
    '/bitsAssets/images/icon.png',
	'/bitsAssets/js/webcomponents.js',
	'/bitsAssets/js/md5.min.js',
	'/bitsAssets/js/google.js',
	//'/reliable-signaler/signaler.js',
	//'/bitsAssets/js/ethjs-provider-signer.js',
	//'/bitsAssets/js/ethereumjs-tx.js',
	'/sw.js',
	'/bitsAssets/js/webcomponents.js',
	'/bitsAssets/js/storeManager.js',
	'/bitsAssets/js/web3/web3.js',
	'/bitsAssets/js/hooked-web3-provider/build/hooked-web3-provider.js',
	'/bitsAssets/js/browserDetect.js',
	'/bitsAssets/js/materialize/materialize.min.js',
	'/bitsAssets/css/materialize/materialize.min.css',
	'/socket.io/socket.io.js',
	'/bitsAssets/js/lightwallet/lightwallet.min.js',
	'/bitsAssets/js/async/lib/async.js',
	'/bitsAssets/js/jquery/jquery-3.3.1.min.js',
	'/bitsAssets/js/chat.js',
	'/bitsAssets/js/firebase-app.js',
	'/bitsAssets/js/firebase-messaging.js',
	'/bitsAssets/js/connect.js',
	'/bitsAssets/js/exchange-v1.js',
	'/bitsAssets/js/exchange-v2.js',
	'/bitsAssets/js/broadcastChannel.js',
	'/bitsAssets/js/qrcodesvg.js',
	'/bitsAssets/js/audioManager.js',
	'/bitsAssets/js/mp3Process.js',
	'/bitsAssets/js/globalVariables.js',
	'/bitsAssets/js/bits-addMobiVeri.js',
	'/bitsAssets/js/pushManager/google-fcm.js',
	'/bitsAssets/js/jspdf/jspdf.js',
	'/bitsAssets/js/jspdf/jspdf.min.js',
	'/bitsAssets/js/moment.js',
	'/bitsAssets/js/raphQR.js',
	'/bitsAssets/js/locationManager.js',
	'/bitsAssets/js/jspdf/jspdf.plugin.autotable.js',
	'/bitsAssets/js/globalServices.js',
	'/bitsAssets/js/knockout-3.1.0.js',
	'/bitsAssets/js/paymentRequester.js',
	'/bitsAssets/js/paymentHandler.js',
	'/bitsAssets/html/connect.html',
	'/bitsAssets/html/token-market-widget.html',
	'/bitsAssets/html/rlogin.html',
	'/bitsAssets/html/alogin.html',
	'/bitsAssets/html/rstatus.html',
	'/bitsAssets/html/status.html',
	'/bitsAssets/js/doocaPay.js',
	'/bitsAssets/js/payWorker.js',
	'/bitsAssets/font-awesome/css/all.css',
	'/bitsAssets/font-awesome/css/brands.css',
    '/bitsAssets/font-awesome/webfonts/fa-brands-400.woff2',
	'/bitsAssets/font-awesome/webfonts/fa-brands-400.woff',
	'/bitsAssets/font-awesome/webfonts/fa-brands-400.ttf',
	'/bitsAssets/font-awesome/webfonts/fa-solid-900.ttf',
	'/bitsAssets/font-awesome/webfonts/fa-solid-900.woff',
	'/bitsAssets/font-awesome/webfonts/fa-solid-900.woff2',
	'/bitsAssets/js/exchange/allMods.js',
	'/bitsAssets/js/exchange/exchange-selector.js',
	'/bitsAssets/js/exchange/viewModeTabs.js',
	'/bitsAssets/js/exchange/viewModeCollapsible.js',
	'/bitsAssets/js/exchange/viewModeCarousel.js'
];

try{
    defChain = 'ALGO';
    chain = sokoFlags.chain;
}catch(err){
    console.warn('ERROR! unable to detemine chain. switching to default '+defChain);
    chain = defChain;
}

chainManager = {};
try{
require("/root/business/chains/"+chain+"/libs/connect.js").connector().then(function(c){
  chainManager =  c; 
});

}catch(err){
  console.log(err);      
    }

  
async function init(accountObject){
    

var asset = {id:'test', owner:'te', name:'st', domain: 'https://test.test', hash: 'stte', meta: '000', supply: 1000, decimals: 0};


//TO-DO
// get the store id from the process command
 storeId = sokoFlags.account;
 

UIDtemp = sokoFlags.theme;

entDevID = sokoFlags.account;

bitsUpdated = false;
sokoUpdated = false;
tmUpdated = false;
entSettings = {};
serverRunning = false;


async function loadMainDomain(url){
    
//(async(url) => {
     console.log('talking to: '+url+', saving to '+__dirname + '/soko.conf');
      
  var response = await entFunc.httpGet(url);
  
  // console.log(response);
      writeFile(__dirname + '/config/soko.conf', response, function(err) {
            if (err) console.log('!ERR unable backup enterprise info', err);
            procEntInfo(JSON.parse(response), accountObject); 
    });
  
//})(dmnIP);
    
}


// uses clearnet as fallback to initial tor connection
//

waitPort({
  port: 18080,
  timeout: 40000
})
  .then(({ open, ipVersion }) => {
    if (open) {
        
   console.log(`Bitsoko Tor port is open on IPv${ipVersion}!`);     
   
var dmnIP=mainTorPipe + "/getEnterprise/?servEntID=" + entDevID;
     
      
loadMainDomain(dmnIP)

    } else{
        
        console.log('Bitsoko Tor port did not open before timeout.. trying clearnet');
        
var dmnIP=mainDomain + "/getEnterprise/?servEntID=" + entDevID;

    loadMainDomain(dmnIP);
    
    }
    
  })
  .catch((err) => {
    console.err(`Error occured while waiting for Bitsoko Tor port .. trying clearnet: ${err}`);
    
var dmnIP=mainDomain + "/getEnterprise/?servEntID=" + entDevID;

    loadMainDomain(dmnIP);
    
  });
      
      
      
      
/*
  .then(response => {
       
      writeFile(__dirname + '/soko.conf', JSON.stringify(response.data), function(err) {
            if (err) console.log('!ERR unable backup enterprise info', err);
            procEntInfo(response.data, accountObject); 
    });

  })
  .catch(error => {
     process.exit({err:error});
    
    
    fs.readFile(__dirname + '/soko.conf', function (error, source) {
    //fs.readFile('/root/business/config/soko.conf', function (error, source) {
        
        if(error){
    
    
                var err='ERR! critical error unable to load shop data ';
                console.log(error);
                process.exit({err:error});
            
            
        }else{
            
            var shDat=JSON.parse(source);
    procEntInfo(shDat, accountObject); 
    
    
            
        }
        
        
    });
    */
    
    
 // });




  
    
    
}
  
createChainAccount().then(function(res){
    accountObject = res;
   init(res);
}).catch(function(er){
    
    console.error('FATAL! Unable to Initialize blockchain services '+er);
    process.exit({err:er});
    
});

        
     

var bitsC = `
            cd business/bits
            git pull
        `;
var sokoC = `
            cd business/soko
            git pull
        `;

var tmC = `
            cd business/tm
            git pull
        `;


async function procEntInfo(body, accountObject){
    
   try{

       allServices = body.services;
                allSettings = body.settings;
                allInfo = body.enterpriseInfo;
                entContract = body.enterpriseContract;
                //  console.log(allInfo, allSettings);
                
doDBStuff(accountObject);




                if (allInfo.showManagers == 'true') {

                    entSettings.managersDisabled = false;
                } else {

                    entSettings.managersDisabled = true;
                }

                if (allInfo.showTokens == 'true') {

                    entSettings.tokensDisabled = false;
                } else {

                    entSettings.tokensDisabled = true;
                }

                //----------------------------------//



                //---------add the about Title------//
                try {

                    if (allInfo.entAboutTitle.length < 3) {

                        entSettings.entAboutTitle = allInfo.entAboutTitle;
                    } else {

                        entSettings.entAboutTitle = allInfo.entAboutTitle;
                    }

                } catch (err) {
                    console.log('!INFO unable to get about title', err);
                }
                //-----------------------------------------//

                //---------add the about body------//
                try {

                    if (allInfo.entAboutBody.length < 3) {

                        entSettings.entAboutBody = allInfo.entAboutBody;
                    } else {

                        entSettings.entAboutBody = allInfo.entAboutBody;
                    }

                } catch (err) {
                    console.log('!INFO unable to get about body', err);
                }
                //-----------------------------------------//

				//Check cover image
				if(allInfo.cover == ""){
					allInfo.coverDisabled = true;
				}else{
					allInfo.coverDisabled = false;
				}
				//---------add the section images------//
                try {

                    //                    console.log('!INFO section1 data ', allInfo.entImageList);
					console.log("============================allInfo.entImageList");
					entImageList = JSON.parse(allInfo.entImageList);
                    
                    if (entImageList.length > 0) {

                        entSettings.entImageListDisabled = false;
                        entSettings.entImageList = entImageList;

                    } else {

                        entSettings.entImageListDisabled = true;
						entSettings.entImageList = [];
                    }

                } catch (err) {
                    console.log('!INFO unable to get images section', err);

                    entSettings.entImageListDisabled = true;
					entSettings.entImageList = [];
				}
                //-----------------------------------------//

                //---------add the section icons------//
                try {
                    
                    // console.log('!INFO section1 data ', allInfo.entIconList);
                    
                    var icnItms = JSON.parse(allInfo.entIconList);
                    
                    if (icnItms.length > 0) {

                        entSettings.entIconListDisabled = false;
						entSettings.entIconList = icnItms;

                    } else {

						entSettings.entIconList = [];
    					entSettings.entIconListDisabled = true;
                    }

                } catch (err) {
                    console.log('!INFO unable to get icons section', err);
					entSettings.entIconList = [];

                    entSettings.entIconListDisabled = true;
                }
                
                //-----------------------------------------//



                aPs = body.enterprisePromos;
                  console.log(aPs);
                allDomains = allInfo.domains;
               

                matchShops();
                for (var pcc in allProdCat) {
                    if (allProdCat[pcc]['servList'].length == 0) {
                        // remove allProdCat[pcc]


                        var index = allProdCat.indexOf(allProdCat[pcc]);
                        if (index > -1) {
                            allProdCat.splice(index, 1);
                        }
                    }

                }
                
                //============================================   server setup complete, start serving content   ========//



                    
                    
                    //TO-DO complete transition to https 
                   
                    try {
                     OpenInsecure();
                     
                       // OpenSecure();
                    } catch (err) {
                        console.log(err);
                        console.log('security certificates not found! initiating letsencrypt..', err);
                        installCerts();
                    }
                    

                    //update apps with main branch every 30 days
                    var upInt=(1000 * 60 * 60 * 1 * 24 * 30);

                    await updateApps();
                    //setInterval(function () {
                    //    updateApps();
                    //}, upInt);
                    
}catch(er){
    console.error(er);
}
}




function getBitsWinOpt(str, aKey) {
    try {
        var ps = str.split("?")[1];
        var pairs = ps.split("&");
    } catch (e) {
        return false;
    }


    for (var i = 0, aKey = aKey; i < pairs.length; ++i) {
        var key = pairs[i].split("=")[0];

        var value = pairs[i].split("=")[1];
        if (key == aKey) {

            return value;

        }

    }
}


/*

*/


var bitsLoader = function (){
    return new Promise(async function(resolve, reject) {
    console.log('updating bits..');
    nCmd.get(bitsC, function (data, err, stderr) {
        if (!err) {
            var hMsg = 'updated bits';
            console.log(hMsg);
            bitsUpdated = true;
            resolve(true);
            if (bitsUpdated && sokoUpdated && tmUpdated) {
                //OpenInsecure();
                if (!serverRunning) {
                    try {
                        OpenSecure();
                    } catch (err) {
                        console.log(err);
                        console.log('security certificates not found! initiating letsencrypt..', err);
                        installCerts();
                    }
                }
            }
        } else {
            console.log(err);
            
                //process.exit({err:err});
        }
    });
    
});
}
var sokoLoader = function (){
    return new Promise(async function(resolve, reject) {
    
    console.log('updating soko..');
    nCmd.get(sokoC, function (data, err, stderr) {
        if (!err) {
            var hMsg = 'updated soko';
            console.log(hMsg);
            sokoUpdated = true;
            resolve(true);
            if (bitsUpdated && sokoUpdated && tmUpdated) {
                //OpenInsecure();
                if (!serverRunning) {
                    try {
                        OpenSecure();
                    } catch (err) {
                        console.log(err);
                        console.log('security certificates not found! initiating letsencrypt..', err);
                        installCerts();
                    }
                }
            }
        } else {
            console.log(err);
                //process.exit({err:err});
        }
    });
    
});
}
var tmLoader = function (){
    return new Promise(async function(resolve, reject) {
    
    console.log('updating token market..');
    nCmd.get(tmC, function (data, err, stderr) {
        if (!err) {
            var hMsg = 'updated token market';
            console.log(hMsg);
            tmUpdated = true;
            resolve(true);
            if (bitsUpdated && sokoUpdated && tmUpdated) {
                //OpenInsecure();
                if (!serverRunning) {
                    try {
                        OpenSecure();
                    } catch (err) {
                        console.log(err);
                        console.log('security certificates not found! initiating letsencrypt..', err);
                        installCerts();
                    }
                }
            }
        } else {
            console.log(err);
               // process.exit({err:err});
        }
    });
});
}

async function updateApps() {
    //update server dependecies
    await loadServerDeps();
    await bitsLoader();
    await sokoLoader();
    //await tmLoader;
    await entFunc.serverDepsLoaded();

}


ReqRes = async function ReqRes(req, res) {
    try {
        console.log(req.params[0]);
        if (req.params[0] == '/getEnterprise/') {
            res.header('Access-Control-Allow-Origin', '*');

                res.status(200);
    
                
 try{
      
    fs.accessSync( '/root/business/soko.conf', fs.F_OK);
	
    var data = fs.readFileSync('/root/business/soko.conf', 'utf8');
    
    data = JSON.parse(data);
    
    for(var i in data.services){
        try{
        
	 var chainAssetDataFile = '/root/business/config/A:bitsoko.org:'+data.services[i].id+':B.json';
	 
    fs.accessSync( chainAssetDataFile, fs.F_OK);
	
        data.services[i].poolID = JSON.parse(fs.readFileSync(chainAssetDataFile, 'utf8'));
        }catch(er){
            console.warn('INFO! business does not hae an associated asset! skipping..',er,data.services[i].id);
        }
    }
    
    
   res.end(JSON.stringify(data));
   return;

        }catch(err){
            
        console.log(err);    
        }
            
            
        } else if (req.params[0] == '/updateINV/') {
            
            res.header('Access-Control-Allow-Origin', '*');

            // TODO!! Validate node address and API key before updating invoice array
            // merchantNodeID & getBitsWinOpt(req.url,'invAPI')
            //start VALIDATION
            
            invoiceArray = JSON.parse(decodeURI(getBitsWinOpt(req.url,'invIDs')));
            
            // end VALIDATION
            
            //responds with 200 even if the request fails!
                res.status(200);
                
                res.end('ok');
                return;
            
        } else if (req.params[0] == '/getINV/') {
            
            res.header('Access-Control-Allow-Origin', '*');

            // TODO!! pick any unused invoice from array
            // 
            //
            
            invResp = invoiceArray[0];
            
                res.status(200);
                res.end(invResp);
                return;
            
        } else if (req.params[0] == '/index.html' || req.params[0] == '/') {
            console.log('serving homepage')
    /*
            try {

                //getting store index page information
                var sendFl = __dirname + '/index.html';

                fs.accessSync(sendFl, fs.F_OK);
                console.log('info! serving cache enterprise page');
                res.sendFile(sendFl);



            } catch (e) {
    */
                // cannot find store page. creating it so we can save to cache and reload faster next time
              //  console.log('info! cant find ' + sendFl + ' creating new enterprise page from ',JSON.stringify(allInfo));
                when(entFunc.createEnterprisePage(req,allInfo), function (r) {
                    return res.end(r);

                }, function (err) {
                    console.log('err! Unable to create store page', err);
                })


    //      }

        } else if(req.url.includes('/orderManager/?')){
	
	    
//res.setHeader('Access-Control-Allow-Origin', 'https://bitsoko.io');
return when(entFunc.getOrderManager(getBitsWinOpt(req.url,'oid'),getBitsWinOpt(req.url,'act')),function(r){ 
 //console.log('resping '+JSON.stringify(r));
	
		 res.setHeader('content-type', 'application/json'); 
	res.end(JSON.stringify(r));
},function(err){
	console.log('error in managing order',err);
});
}else if (req.url.includes('/bits/index-')) {

/*
            try {

                //getting store index page information
                var sendFl = __dirname + req.url;
                
                console.log(sendFl);
                
                fs.accessSync(sendFl, fs.F_OK);
                
                res.sendFile(sendFl);


            } catch (e) {
            
     */       
                // cannot find store page. creating it so we can save to cache and reload faster next time
                var r = await entFunc.createStorePage(req);
                 res.flush();
                   res.end(r);
                   

    //        }



            /*

			     if(getBitsWinOpt(req.url,'s')=='3'){
				     //this is a sokopos link
				var sid=getBitsWinOpt(req.url,'a');
				}else{
					//this is a enterprise store link
				var sid=getBitsWinOpt(req.url,'s');
				}

         //getting store information
		   if(req.url.includes('/bitsBeta/')) {
	var indxPth='/bitsBeta/index.html';
				     }else{
	var indxPth='/bits/index.html';
				     }


		console.log('SERVICEID!!!!!!!!!',req.url,sid);

	      when(entFunc.returnMerchantServices('',{service:sid,id:sid}), function(r){




	    when(entFunc.bitsStoreDet(sid),function(rr){
		fs.readFile(__dirname + indxPth, function(error, source){

html2jade.convertHtml(source, {}, function (err, jd) {
	var thm = rr.theme;
	if(thm== null || thm== 'null' || thm== ''){
	thm = primaryColor;
	}
	//console.log('setting page theme '+thm);
  var data = {
  name: rr.name,
  desc: rr.description,
    img: rr.bannerPath.replace('.png','-128.png'),
    theme: thm,
    stMeta: JSON.stringify(r),
      cid: Cid
}
data.body = process.argv[2];
//jade.render
    var template = jade.compile(jd);
    var html = template(data);
    //res.writeHead(200);                                                                   
    res.end(html);
});
});


	    },function(err){
	    console.log('err! cannot show merchant share info! merchant '+sid+' not found!');
	    })

	console.log('INFO!!!! ',JSON.stringify(r.res));



}, function(error){
		console.log('ERR: service profile error',error);

});

*/
            //console.log('SOKO Request, ', req.params[0]);
            //fs.accessSync(__dirname + req.params[0], fs.F_OK);
            //return res.sendFile(__dirname + req.params[0]);


        } else if (req.url.includes('/soko/index.html')) {

              var r = await entFunc.createAdminPage(req);
                 res.flush();
                   res.end(r);
                   

        } else if (req.url.includes('/soko')) {

            console.log('SOKO Request, ', req.params[0]);
            fs.accessSync(__dirname + req.params[0], fs.F_OK);
            return res.sendFile(__dirname + req.params[0]);


        } else if (req.url.includes('/tm/')) {
            

                return res.end('error');
/*
            console.log('Token Market Request, ', req.params[0]);

            if (req.url.includes('/tm/?')) {



                //if(getBitsWinOpt(req.url,'cid')){
                when(messageManager.contByAdr(getBitsWinOpt(req.url, 'cid'), ''), function (r) {


                    when(messageManager.merchantOwner(r.res.contractCreator), function (result) {

                        fs.readFile(__dirname + '/tm/index.html', function (error, source) {

                            html2jade.convertHtml(source, {}, function (err, jd) {
                                var data = {
                                    name: 'Invest with ' + result.res.name,
                                    desc: 'earn upto ' + result.res.contractRate + '% profits every week by buying ' + r.res.name + ' token',
                                    img: result.res.icon,
                                    cid: Cid
                                }

                                data.body = process.argv[2];
                                //console.log(JSON.stringify(data));
                                var template = jade.compile(jd);
                                var html = template(data);
                                //res.writeHead(200);
                                return res.end(html);

                            });
                        });

                    }, function (error) {
                        console.log(error);
                    });

                }, function (error) {
                    console.log(error);
                });

            } else {

                fs.accessSync(__dirname + req.params[0], fs.F_OK);
                return res.sendFile(__dirname + req.params[0]);
            }

*/

        } else {
            try {
                fs.accessSync(__dirname + req.params[0], fs.F_OK);
                res.sendFile(__dirname + req.params[0]);
            } catch (err) {
                //console.log(err);
                res.status(500);

                return res.end('error');

                //res.writeHead(301, {
                //   location: "/bits/index.html"
                //});
                //return res.end();
            }
        }
    } catch (err) {
        console.log('ERR loading response ', err)
    }
}

installCerts = function () {
    console.log('initiating cert installer');
    'use strict';
    var LE = require('greenlock');
    /*
    // Storage Backend
    var leStore = require('le-store-certbot').create({
      configDir: '/root/certs'                          // or /etc/letsencrypt or wherever
    , debug: true
    });
    // ACME Challenge Handlers
    var leChallenge = require('le-challenge-fs').create({
      webrootPath: '/root/letsencrypt/var'                       // or template string such as
    , debug: true                                            // '/srv/www/:hostname/.well-known/acme-challenge'
    });
    */
    function leAgree(opts, agreeCb) {
        // opts = { email, domains, tosUrl }
        opts = {
            domains: allDomains,
            email: bitsokoEmail // user@example.com
                ,
            agreeTos: true
        }
        agreeCb(null, opts.tosUrl);
    }
    le = LE.create({
        agreeToTerms: leAgree // hook to allow user to view and accept LE TOS
            ,
        server: LE.productionServerUrl // or LE.productionServerUrl
            //, server: LE.stagingServerUrl
            //, store: leStore
            ,
        challenges: {
            'http-01': require('le-challenge-fs').create({
                webrootPath: '/root/business'
            })
        },
        store: require('le-store-certbot').create({
                webrootPath: '/root/business'
            })
            // handles saving of config, accounts, and certificates
            //, challenges: { 'http-01': leChallenge }                  // handles /.well-known/acme-challege keys and tokens
            ,
        challengeType: 'http-01' // default to this challenge type
            //, sni: require('le-sni-auto').create({})                // handles sni callback
            ,
        debug: true
    });
    critiMSG = "RUNNING ON INSECURE ENVIROMENT!!";
    /*

    insapp = express();
    insapp.use(compress());
    // If using express you should use the middleware

    insapp.use('/', le.middleware(require('redirect-https')()));
    http = require('http');
    inserver = http.createServer(insapp);
    io = require('socket.io')(inserver);
    insapp.get(/^(.+)$/, function (req, res) {
      ReqRes(req, res);

    });
     inserver.listen(insPORT, '127.0.0.1', function(err) {
      if (err) throw err;
     console.log("Listening for ACME http-01 challenges on", this.address());
    });
    */
    // handles acme-challenge and redirects to https
    var app = require('express')();
    app.use('/', function (req, res) {
        res.end('waiting to become secure');
    });
    //
    // Otherwise you should see the test file for usage of this:
    // le.challenges['http-01'].get(opts.domain, key, val, done)
    // Check in-memory cache of certificates for the named domain
    le.check({
        domains: allDomains
    }).then(function (results) {
        if (results) {
            // we already have certificates
            console.log(results);
            return;
        }
        // Register Certificate manually
        console.log('failed to register LE Certificate automatically, now trying manual registration');
        le.register({
            domains: allDomains // CHANGE TO YOUR DOMAIN (list for SANS)
                ,
            email: bitsokoEmail // CHANGE TO YOUR EMAIL
                ,
            agreeTos: true // set to tosUrl string (or true) to pre-approve (and skip agreeToTerms)
                ,
            rsaKeySize: 2048 // 2048 or higher
                ,
            challengeType: 'http-01' // http-01, tls-sni-01, or dns-01
        }).then(function (results) {
            console.log('success: certificates installed. Restarting service');
            throw 'success: certificates installed. Restarting service';
        }, function (err) {
            // Note: you must either use le.middleware() with express,
            // manually use le.challenges['http-01'].get(opts, domain, key, val, done)
            // or have a webserver running and responding
            // to /.well-known/acme-challenge at `webrootPath`
            console.error(err);
            console.error('[Error]: node-letsencrypt/examples/standalone');
            console.error(err.stack);
            //console.log('certification failed. will try again in one hour');
            //setTimeout(installCerts(), (60 * 60 * 1000));
        });
    });
}

function leAgree(opts, agreeCb) {
    // opts = { email, domains, tosUrl }
    opts = {
        domains: allDomains,
        email: bitsokoEmail // user@example.com
            ,
        agreeTos: true
    }
    agreeCb(null, opts.tosUrl);
}

function createCert() {
    console.log('INFO: starting certificate generation..');
    
    le = LE.create({
    agreeToTerms: leAgree // hook to allow user to view and accept LE TOS
        ,
    server: LE.productionServerUrl // or LE.productionServerUrl
        //        ,
        //    server: LE.stagingServerUrl
        //, store: leStore
        ,
    challenges: {
        'http-01': require('le-challenge-fs').create({
            webrootPath: '/root/business'
        })
    },
    store: require('le-store-certbot').create({
            webrootPath: '/root/business'
        })
        // handles saving of config, accounts, and certificates
        //, challenges: { 'http-01': leChallenge }                  // handles /.well-known/acme-challege keys and tokens
        ,
    challengeType: 'http-01' // default to this challenge type
        //, sni: require('le-sni-auto').create({})                // handles sni callback
        ,
    debug: true
});

    critiMSG = "RUNNING ON INSECURE ENVIROMENT!!";
    // handles acme-challenge and redirects to https
    require('http').createServer(le.middleware(require('redirect-https')())).listen(insPORT, function () {
        console.log("Listening for ACME http-01 challenges on", this.address());
    });
    var app = require('express')();
    app.use('/', function (req, res) {
        res.end('waiting to become secure');
    });
    //
    // Otherwise you should see the test file for usage of this:
    // le.challenges['http-01'].get(opts.domain, key, val, done)
    // Check in-memory cache of certificates for the named domain
    le.check({
        domains: allDomains
    }).then(function (results) {
        if (results) {
            // we already have certificates
            console.log(results);
            return;
        }
        // Register Certificate manually
        console.log('failed to register LE Certificate automatically, now trying manual registration');
        le.register({
            domains: allDomains // CHANGE TO YOUR DOMAIN (list for SANS)
                ,
            email: bitsokoEmail // CHANGE TO YOUR EMAIL
                ,
            agreeTos: true // set to tosUrl string (or true) to pre-approve (and skip agreeToTerms)
                ,
            rsaKeySize: 2048 // 2048 or higher
                ,
            challengeType: 'http-01' // http-01, tls-sni-01, or dns-01
        }).then(function (results) {
            console.log('success: certificates installed. Restarting service');
            throw 'success: certificates installed. Restarting service';
        }, function (err) {
            // Note: you must either use le.middleware() with express,
            // manually use le.challenges['http-01'].get(opts, domain, key, val, done)
            // or have a webserver running and responding
            // to /.well-known/acme-challenge at `webrootPath`
            console.error(err);
            console.error('[Error]: node-letsencrypt/examples/standalone');
            console.error(err.stack);
        });
    });
}

function OpenSecure() {
    servCerts = {
        key: fs.readFileSync('/root/letsencrypt/etc/live/' + allDomains[0] + '/privkey.pem'),
        cert: fs.readFileSync('/root/letsencrypt/etc/live/' + allDomains[0] + '/fullchain.pem'),
        ca: [fs.readFileSync('/root/letsencrypt/etc/live/' + allDomains[0] + '/chain.pem')] // <----- note this part
    };
    app.use(compress());
    server = https.createServer(servCerts, app);
    io = require('socket.io')(server);
    server.setTimeout(0, socketTimeout);
    app.get(/^(.+)$/, function (req, res) {
        ReqRes(req, res);
    });
    server.listen(PORT, function (err) {
        if (err) throw err;
        console.log('Secure now online at https://localhost:' + PORT);
        
    require('http').createServer(le.middleware(require('redirect-https')())).listen(insPORT, function () {
        console.log("Listening for ACME http-01 challenges on", this.address());
    });

        serverRunning = true;
    });
}

function OpenInsecure() {
    
    
                    console.log('starting insecure server');
    insapp = express();
    insapp.use(compress());
    //insapp.use(forceSSL);
    // If using express you should use the middleware
    //insapp.use('/', le.middleware());
    http = require('http');
    inserver = http.createServer(insapp);
    
io = require('socket.io')(inserver);
    

io.on('connection', function(socket) {
	console.log('A user is connected.');

	socket.on('disconnect', function() {
		var userData = connectedUsers[socket.id];
		if (typeof userData !== 'undefined') {
			socket.leave(connectedUsers[socket.id]);
			io.to(userData.room).emit('message', {
				username: 'System',
				text: userData.username + ' has left!',
				timestamp: moment().valueOf()
			});
			delete connectedUsers[socket.id];
		}
	});

	socket.on('joinRoom', function(req, callback) {
		if (req.room.replace(/\s/g, "").length > 0 && req.username.replace(/\s/g, "").length > 0) {
			var nameTaken = false;

			Object.keys(connectedUsers).forEach(function(socketId) {
				var userInfo = connectedUsers[socketId];
				if (userInfo.username.toUpperCase() === req.username.toUpperCase()) {
					nameTaken = true;
				}
			});

			if (nameTaken) {
				callback({
					nameAvailable: false,
					error: 'Sorry this username is taken!'
				});
			} else {
				connectedUsers[socket.id] = req;
				socket.join(req.room);
				socket.broadcast.to(req.room).emit('message', {
					username: 'System',
					text: req.username + ' has joined at ' +req.path,
					timestamp: moment().valueOf()
				});
				console.log(connectedUsers);
				callback({
					nameAvailable: true
				});
			}
		} else {
			callback({
				nameAvailable: false,
				error: 'Hey, please fill out the form!'
			});
		}
	});

	socket.on('setAudioTranscript', function(message) {
		console.log(message);
		message.timestamp = moment().valueOf();
		socketC.emit('setAudioTranscript', {audio: message.audio, audioID: message.audioID}); 
		
 
	});
		
	socketC.on('getAudioTranscript', function(m) {
		console.log(m);
		socket.emit('getAudioTranscript', {audio: m.audio, audioID: m.audioID});
		//io.to(connectedUsers[socket.id].room).emit('message', message);
	});

	socket.on('message', function(message) {
		message.timestamp = moment().valueOf();
		console.log(message);
		//io.to(connectedUsers[socket.id].room).emit('message', message);
	});

	socket.emit('message', {
		username: 'System',
		text: 'New visitor!',
		timestamp: moment().valueOf()
	});

});



    insapp.get(/^(.+)$/, function (req, res) {
        ReqRes(req, res);
    });
    inserver.listen(insPORT, function (err) {
        if (err) console.log('CANT start error ',err);
        console.log('insec port online at http://localhost:' + insPORT);
        
        serverRunning = true;
        //start web transport
        //initWebT();

    });
}

function socketTimeout() {
    console.log('sockets timed out: not receiving connecions!!')
};


function matchShops() {
    //matching shops to their managers
    // create empty array for the reconstructed manager array.
    managersShop = new Object();
    managersShop.manager = new Array();
    //step one loop thu managers list and get m.name, shopID and userID
    for (var iv in allManagers) {
        //console.log("looping managers",allManagers[iv].sID,allManagers[iv].uid,allManagers[iv].name)
        for (var iiiv in allServices) {
            // console.log("looping services ",allServices[iiiv].title,allManagers[iv].uid,allManagers[iv].name )
            if (allManagers[iv].sID == allServices[iiiv].id) {
                //console.log("******** managers *******", allServices[iiiv].title, allManagers[iv].uid, allManagers[iv].name);
                var nm = new Object();
                nm.shop = allServices[iiiv].title;
                nm.id = allManagers[iv].uid
                nm.name = allManagers[iv].name
                nm.icon = allManagers[iv].icon
                managersShop.manager.push(nm);
            }
        }
    }
    var obj = {};
    for (var i = 0, len = managersShop.manager.length; i < len; i++) obj[managersShop.manager[i]['name']] = managersShop.manager[i];
    managersShop.manager = new Array();
    allNewManagers = new Array();
    for (var key in obj) allNewManagers.push(obj[key]); // managersShop.manager.push(obj[key]);
   //console.log(allNewManagers, "******** new managers *******")

}

async function loadServerDeps() {
 
 

 /* 
    Promise.all(serverFiles.map(url =>
    fetch(mainDomain + url).then(async function(resp){
        var arr = url.split('/');
        
        var text = await resp.text();
        
        var path = 'business' + arr.join('/');
        
      try {
          fs.writeFileSync(path, text);
    //await fsA.appendFile(path, text);
  } catch (err) {
    console.log(err);
    throw err
  }
  
  
    })
)).then(f => {
    console.log('All bitsAssets Deps files Written!');
}).catch(err => {
    console.error('INFO! Could not load all bitsAssets! '+err);
    
     process.exit({err:err});
});
 

*/

    for (var url in serverFiles) {


(async () => {
    
    var arr = serverFiles[url].split('/');
    arr.pop();
    var path = '/root/business' + arr.join('/');
      

  //Wrapping the code with an async function, just for the sake of example.

  const downloader = new Downloader({
    url: mainDomain + serverFiles[url], 
    directory: path,
    maxAttempts: 5,
    cloneFiles: false,
  });
  
  try {
                
    const {filePath,downloadStatus} = await downloader.download(); //Downloader.download() resolves with some useful properties.
return {filePath,downloadStatus};
  } catch (error) {
    //IMPORTANT: Handle a possible error. An error is thrown in case of network errors, or status codes of 400 and above.
    //Note that if the maxAttempts is set to higher than 1, the error is thrown only if all attempts fail.
    console.log("Download failed", error);
     process.exit({err:error});
  }
})();
/*
        var arr = serverFiles[url].split('/');
        arr.pop();


        var options = {
            directory: 'business/' + arr.join('/'),
            timeout: 60000
        }
        // console.log(arr, mainDomain + serverFiles[url]);
        
        var downLnk=mainDomain + serverFiles[url];

        fileDownloader(downLnk, options, function (err) {
            if (err) {
                err='Fatal! Cant fetch enterprise dependency: '+err+' '+downLnk;
                console.warn(err);
                process.exit({err:err});
                //throw err;
            } else {
                
                // console.log('saved ', serverFiles[url], ' to ', arr.join('/'))
                
                
            }
            
        })
*/
    }
 
return true;
}




                function squashByName(arr) {
                    var tmp = [];
                    var tmpID = [];
                    for (var i = 0; i < arr.length; i++) {
                        if (tmpID.indexOf(arr[i].name) == -1) {
                            tmp.push(arr[i]);
                            tmpID.push(arr[i].name);
                        }
                    }
                    return tmp;
                }


async function doDBStuff(accountObject){
    
    

//  when(entFunc.getAllServices(), function (allServices) {
   //              allServices=allServices;
                stores = new Array();

allPromos = [];	
               for (var ii in allServices) {
			
	 allPromos = allPromos.concat(allServices[ii].shopPromos);
     
     
      try{
          
//var asset =  await createChainAsset(allServices[ii].id+':B', accountObject, allServices[ii].name, 'bitsoko.org', allServices[ii].description);
var asset =  await createChainAsset(allServices[ii].id+':B', accountObject, allServices[ii].id+':BITS:POOL1', 'bitsoko.org', allServices[ii].description);
var pool = await createChainPool(accountObject, asset, allServices[ii]);
  
      }catch(err){
          
       console.warn('INFO: Failed to create contracts! Please ensure this enterprise has minimum '+chain+' balance! ');
       
       continue;
          
      }
    console.log('INFO! fetching store.. '+allServices[ii].id);	
    
    // TO-DO 
    // save to local DB after
    // start
    
    allServices[ii].chainContract = {asset: asset, pool: pool, type: defChain};
    
    // stop
	
	var allStoProds = allServices[ii].products;
	
	
	
	
	//download all product images
	for(var ix in allStoProds){
	    try{
	        var uPth=mainDomain + allStoProds[ix].imagePath;
	        
	        if(!uPth.includes('.png')){
	            continue;
	        }else{
	            
	            console.log('real image');
	        }
	        
	    // Download to a directory and save with the original filename
                    var options = {
                        url: mainDomain + allStoProds[ix].imagePath,
                        dest: 'business/bitsAssets/tmp/products/',
                        //dest: '/'
                    }
                    imgDownloader.image(options).then(function (filename, image) {
                        //console.log('File saved to', filename)
                    }).catch(function (err) {
                        console.log(err)
                    })
         
	    // Download to a directory and save with the original filename
                    var options = {
                        url: mainDomain + allStoProds[ix].imagePath.replace('.png', '.webp').replace('.webp', '-224.webp'),
                        dest: 'business/bitsAssets/tmp/products/',
                        //dest: '/'
                    }
                    imgDownloader.image(options).then(function (filename, image) {
                        //console.log('File saved to', filename)
                    }).catch(function (err) {
                        console.log(err)
                    })
                    
	    }catch(e){
	        console.log(e)
	    }
	    
	}
	
			
			
			stores.push(allServices[ii].id);
		
			if(allDomains[0]=='supplies.bitsoko.co.ke'){
					
                    try {
                        var eaCat = JSON.parse(allServices[ii].inventoryCategory);
                    } catch (e) {
                       var eaCat = [];
                    }
				
			}else{
				
                    try {
                        var eaCat = JSON.parse(allServices[ii].productCategory);
                    } catch (e) {
                        var eaCat = [];
                    }
                    
                   // delete allServices[ii].productCategory;
			}

			   
                    for (var ix in eaCat) {
			 
                        eaCat[ix]['servList'] = [];
                        allProdCat.push(eaCat[ix]);
			    
                    }
                    
                    
                    allServices[ii].banner = allServices[ii].bannerPath;
                    allServices[ii].desc = allServices[ii].description;
                    allServices[ii].title = allServices[ii].name;
                    //console.log(allServices[ii].promotions,"======== Promotions ====");
                    var aMans = allServices[ii].managers;
			
			if(allDomains[0]=='supplies.bitsoko.co.ke'){
			
                    try {

                        var cats = JSON.parse(allServices[ii].inventoryCategory);
                    } catch (e) {

                        var cats = [];
                    }
			}else{
			
			
                    try {

                        var cats = JSON.parse(allServices[ii].productCategory);
                    } catch (e) {

                        var cats = [];
                    }
                    
                   // delete allServices[ii].productCategory;
			}
			
                allProdCat = squashByName(allProdCat);
			
			//console.log('INFO! loaded product categories.. ',eaCat.length);
              
			if(allDomains[0]=='supplies.bitsoko.co.ke'){
			//add extra categories
				
				cats=suppliesAssets;
				
			}
			
                    for (var pcc in allProdCat) {
                        for (var pc in cats) {

                            if (allProdCat[pcc].name == cats[pc].name) {
                                allProdCat[pcc]['servList'].push(allServices[ii]);
                            }
                        }

                    }
			
			



                    for (var iii in aMans) {
                        aMans[iii].sID = allServices[ii].id;
                        allManagers.push(aMans[iii]);
                    }
                    
                    try{
                        
                          var uPth=mainDomain + allServices[ii].banner;
	        console.log(uPth);
	        
	        if(!uPth.includes('.png')){
	            console.log('not a real image');
	            continue;
	        }else{
	            
	            console.log('real image');
	        }
	        

                    
                    // Download to a directory and save with the original filename
                    var options = {
                        url: mainDomain + allServices[ii].banner,
                        dest: 'business/bitsAssets/tmp/services/',
                        //dest: '/'
                    }
                    imgDownloader.image(options).then(function (filename, image) {
                        //console.log('File saved to', filename)
                    }).catch(function (err) {
                        console.log(err)
                    })
                    
                    // Download medium image
                    var options = {
                        url: mainDomain + allServices[ii].banner.replace(".png", "-224.webp"),
                        dest: 'business/bitsAssets/tmp/services/',
                        //dest: '/'
                    }
                    imgDownloader.image(options).then(function (filename, image) {
                        //console.log('File saved to', filename)
                    }).catch(function (err) {
                        console.log(err)
                    });
                    
                    
                    // Download smaller image
                    var options = {
                        url: mainDomain + allServices[ii].banner.replace(".png", "-35.webp"),
                        dest: 'business/bitsAssets/tmp/services/',
                        //dest: '/'
                    }
                    imgDownloader.image(options).then(function (filename, image) {
                        //console.log('File saved to', filename)
                    }).catch(function (err) {
                        console.log(err)
                    });
                    
                    
                    
                    }catch(er){
                        console.log(er);
                    }
                    
                    
                    //-------------------------add promotions start
                
                
		
		try{
		var retail = JSON.parse(allServices[ii].retailing);
		}catch(er){
		var retail = [];
		}
	
	
	svCnt=allServices.length;
	thVarN=0;
	
	
	/*
	
	entFunc.merchantPromos(aPs,allServices[ii].id,retail).then(function(e){
	    
	    thVarN++;
	    
	 allPromos=allPromos.concat(e);
	 
	// console.log(svCnt,thVarN);
	 console.log('adding this promo'+e+' to array '+aPs);
	 if((svCnt-thVarN)==0){
	     
	     publishedPromos();
	 }
	 
			
	}).catch(function(err){
	    
	    svCnt=svCnt-1;
	  
		 console.log('error in adding promo to list ',err);  
	   if((svCnt-thVarN)==0){
	     
	     publishedPromos();
	 }
	 
	    
	});    
       */    
                    //-------------------------add promotions end         
                    
                    
                }
                //console.log(allServices);


               // }, function (err) {
                //    console.log('err! Unable to get services', err);
                //});
                
            
	

}

/*

function publishedPromos(){
    
			
                try {
                    //var aPs = allPromos;
                    for (var iiii in aPs) {


                        if (aPs[iiii].promoStatus == "active") {

                            allPromos.push(aPs[iiii]);
                        }

                        // enable or disable the promotions section

                        if (allPromos.length > 1) {

                            entSettings.promotionsDisabled = false;
                        } else {

                            entSettings.promotionsDisabled = true;
                        }

                        // Download promo picture and save with the original filename
                        var options = {
                            url: mainDomain + aPs[iiii].promoBanner,
                            dest: 'business/bitsAssets/tmp/promotions/',
                            //dest: '/'
                        }
                        imgDownloader.image(options).then(function (filename, image) {
                            //console.log('Promo File saved to', filename)
                        }).catch(function (err) {
                            console.log(err)
                        })
                    }
                } catch (err) {
                    console.log(err)
                }
}
  */
  
  /*
  Find blockchain info
  POOL and TOKEN details
  
  
  
  
  
  
  
  */
  
  async function createChainAccount() {

  try{
      
      var chainAccountDataFile = '/root/business/config/chainAccountData.json';
	 
    fs.accessSync( chainAccountDataFile, fs.F_OK);
   
    var data = fs.readFileSync(chainAccountDataFile, 'utf8');
    var accountObject = JSON.parse(data);
         console.log('INFO! found old '+chain+' account with ID: '+accountObject.address);
    return accountObject;




        //resolve(data);
        }catch(err){
         
      // create token asset and pool
      // save info to file then return chain data
      console.warn('INFO! no chain data found, creating new instance: '+err);
        
      
        // ACCOUNT CREATION
        //try importing first before creating new
        //
        
      
        accountManager = require("/root/business/chains/"+chain+"/account/accountManager.js");
      var importAccountDataFile = '/root/business/config/importedAccount.json';
	   
        
  try{
      

    fs.accessSync( importAccountDataFile, fs.F_OK);
   
    var data = fs.readFileSync(importAccountDataFile, 'utf8');
    
    var data = JSON.parse(data).mnemonic;
    
         console.log('INFO! importing key! '+data);
         
    var accountObject = await accountManager.get(data);
         
         console.log('INFO! importing account! '+accountObject.address);
        if(accountObject.address){
           entFunc.saveToJsonFile(chainAccountDataFile, accountObject);
         
         console.log('INFO! imported '+chain+' account with ID: '+accountObject.address);
         
          return accountObject;
        }

        //resolve(data);
        }catch(err){
        // IMPORT FAILED! CREATING NEW!
        
        console.warn(err);
        
        accountObject = await accountManager.create();
        
        if(accountObject.address){
           entFunc.saveToJsonFile(chainAccountDataFile, accountObject);
         
         console.log('INFO! created new '+chain+' account with ID: '+accountObject.address);
         
          return accountObject;
        }
        }
        }
  }    
        
  async function createChainAsset(assetName, accountObject, storeName, assetDomain, assetMeta) {
 
 try{
      
	 var chainAssetDataFile = '/root/business/config/A:'+assetDomain+':'+assetName+'.json';
	 
    fs.accessSync( chainAssetDataFile, fs.F_OK);
	
    var data = fs.readFileSync(chainAssetDataFile, 'utf8');
  
	 return JSON.parse(data);

        }catch(err){
         
      // create token asset and pool
      // save info to file then return chain data
      console.warn('INFO! creating new asset instance: '+err);
        
      
        // ASSET CREATION
      
      //assetObject = await assetManager.create(assetID, accountObject, assetName, assetDomain, assetMeta, new Uint8Array([21,31]), 1000, 0);
      var assetObject = await require("/root/business/chains/"+chain+"/asset/assetManager.js")
      .create(assetName, accountObject, storeName, assetDomain, new Uint8Array(assetName+':'+assetDomain), new Uint8Array(assetMeta), 1000000000, 6);
       if(assetObject.id){
           
         entFunc.saveToJsonFile(chainAssetDataFile, assetObject);
         
         console.log('INFO! created new '+chain+' asset with ID: '+assetObject.id);
         
         return assetObject;
       }else{
           
         var err = 'ERROR! cannot create asset! does owner wallet :'+accountObject.address+' have enough  '+chain+' tokens? ';
         
         console.log(err);
       
           
       }
         
        
         
             
        }
   
        }

  async function createChainPool(accountObject, assetObject, store) {
 var validatorID = "vID-01";
 
        poolManager = require("/root/business/chains/"+chain+"/pool/poolManager.js");
 
      
	 var chainPoolDataFile = '/root/business/config/P:'+store.id+':'+validatorID+'.json';
	 var chainPoolDataFileB = '/root/business/config/Pb:'+store.id+':'+validatorID+'.json';
	 
    //fs.accessSync( chainPoolDataFile, fs.F_OK);
	
    //var data = fs.readFileSync(chainPoolDataFile, 'utf8');
    
        
   
  
	 //return JSON.parse(data);
  
  
      // create token pool
      // save info to file then return chain data
      //console.warn('INFO! no chain data found, creating new pool instance: '+err);
        
      
        // POOL CREATION
      
        
/*
        try{
            
      console.log('INFO! creating pool for the first time');
            var poolObject = await poolManager.create(accountObject.address, accountObject.mnemonic, assetObject.id, store.id+':BITS', store.id+':BITS:vID-03');
      
         entFunc.saveToJsonFile(chainPoolDataFile, poolObject);
         
         //console.log('INFO! created new '+chain+' pool with ID: '+poolObject);
       
        }catch(err){
            
      console.warn('INFO!: '+err);
       */ 
      

        try{
            console.log(386192725, assetObject.id, accountObject.address, accountObject.mnemonic );
            
            //poolObject = await poolManager.bootB(assetObject.id, 0, accountObject.address, accountObject.mnemonic, store.id+':BITS');
      
            poolObject = await poolManager.boot(386192725, assetObject.id, accountObject.address, accountObject.mnemonic );
      
         entFunc.saveToJsonFile(chainPoolDataFile, poolObject);
         
         console.log('INFO! initialized '+chain+' pool with ID: '+poolObject);
       
      //TRY ADDING LIQUIDITY!
      try{
           await poolManager.add(accountObject.address, accountObject.mnemonic, assetObject.id, 1);
      
         console.log('INFO! added liquidity to '+chain+' pool with ID: '+poolObject);
          
      }catch(err){
          
      console.log('INFO! unable to add liquidity'+err);
      
         
         
         
      }
      
      
      // END TRY LIQUIDITY
           
       
        
      
         
        }catch(error){
            
            console.error('ERROR! unable to create new '+chain+' pool! '+error);
           throw error;
            
        }
      
      
            
        //}
        return poolObject;
         
             
        

        }
async function initWebT() {
return new Promise(async function(resolve, reject) {

  let options = {
  args: ['nodeid:0']
};

PythonShell.run('/root/business/wtp.py', options, function (err, results) {
  if (err) {
     
      console.error('ERROR! unable to initialize web transporter! '+err);
      reject(err);
      
  }else{
      
       console.log('INFO! running web transporter with ' + options.args[0]);
  // results is an array consisting of messages collected during execution
  console.log('results: %j', results);
  
    resolve(results);
  }
  
});

});
}
try{
  rtsp = require('rtsp-ffmpeg');
var uris = ['1',
'2',
'3',
'4'];

async function getRtspSnap(id){
    

  new rtsp.FFMpeg({input: 'rtsp://admin:12Doocanet12!@securitytest.bitsoko.org:554/cam/realmonitor?channel='+id+'&subtype=0'}).on('data', function(data) {
   // console.log(data.toString('base64'));
   return data.toString('base64');
  });     
}

//Listener
socketC.on('connect', async function(){
   try {
      console.log('socket connected to bitsoko server');
      
for (var i in uris){
    
var snap = await getRtspSnap(uris[i]);
  socketC.emit('scene', {img: 'data:image/jpeg;base64,' + snap, room: 'aa', sID: uris[i], services: ["person", "dog", "car"]}); 
  
}

   } catch(e) {
     console.log(e);
   }
});

}catch(er){
console.log(er);    
}




 try{
    var udCmd=
        `
            export GOPATH=/go && export PATH=$GOPATH/bin:/usr/local/go/bin:$PATH ; onionpipe client new soko ; onionpipe 18083~80@soko
        `; 
	 
	 
	    
    nCmd.get(udCmd,function(data, err, stderr){
            if (!err) {
		    var hMsg ='INFO! tor online';
               console.log(hMsg,data);
		    return;
	   
            } else {
       console.warn('ERROR! Tor offline!!! ',err); 
            }
            
        });
 }catch(err){
    
       console.warn('ERROR! Tor error!!! ',err);  
 }


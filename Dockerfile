############################################################
# Dockerfile to build bitsoko container for enterprise businesses
# Based on Ubuntu 18.04
############################################################


# Set the base image to Ubuntu
#FROM ubuntu:18.04
FROM ubuntu:20.04

# File Author / Maintainer
MAINTAINER bitsoko@bitsoko.org

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y build-essential g++ gnupg2 make curl network-manager wireless-tools gcc git iptables libmicrohttpd-dev hostapd dnsmasq bluetooth bluez bluez-tools rfkill libbluetooth-dev libudev-dev --fix-missing

RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -

RUN apt-get update

RUN cd ~ && apt-get install -y nodejs
#RUN cd ~ && npm install jsonfile bleno axios debug write forever express node-cmd@3.0.0 image-downloader download-file node-gcm promised-io compression socket.io pug mysql express-force-ssl html2jade newline-remove promise

#RUN cd ~ && mkdir business


# ALGO START

RUN apt-get install -y software-properties-common && add-apt-repository ppa:deadsnakes/ppa && apt update && apt-get install -y python3.7

RUN update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1 && update-alternatives --config python3 && apt-get install -y python3.7-venv python3.7-dev libffi-dev && python3 --version

RUN apt-get install python3-pip wget nano tor -y

#RUN npm install python-shell algosdk web-push

# add version number to build config
# default always builds master branch

RUN wget https://dl.google.com/go/go1.16.linux-amd64.tar.gz 
RUN tar -xvf go1.16.linux-amd64.tar.gz   
RUN mv go /usr/local  

RUN export GOPATH=/go && export PATH=$GOPATH/bin:/usr/local/go/bin:$PATH && mkdir -p $GOPATH"/src" $GOPATH"/bin" && chmod -R 777 $GOPATH"/" && go install github.com/cmars/onionpipe@latest
WORKDIR /

ARG VERSION=null

#COPY /* ~/business/

RUN cd /root && rm -rf /root/business && git clone --depth=1 http://local.git.bitsoko.org:8929/bitsoko/business.git

RUN cd /root/business && npm install && mkdir bitsAssets && cd bitsAssets && mkdir tmp && cd tmp && mkdir products && mkdir services && mkdir promotions && mkdir html && cd html && mkdir bits && mkdir soko

RUN pip3 install git+http://local.git.bitsoko.org:8929/bitsoko/tinyman-py-sdk.git && pip3 install py-algorand-sdk aioquic


#

# ALGO STOP

RUN cd /root/business && git reset --hard && git pull 

RUN cd /root/business && rm -rf bits && git clone http://local.git.bitsoko.org:8929/bitsoko/bits.git bits

RUN cd /root/business && rm -rf soko && git clone http://local.git.bitsoko.org:8929/bitsoko/soko.git soko

RUN cd /root/business && rm -rf tm && git clone http://local.git.bitsoko.org:8929/bitsoko/token-market.git tm
       
RUN apt-get install ffmpeg -y

EXPOSE 80

HEALTHCHECK --retries=10 --interval=1m --timeout=30s CMD curl --fail http://127.0.0.1:18083/ || exit 1


#ENTRYPOINT cd ~ && rm -rf ~/business && git clone --depth=1 http://gitlab.bitsoko.org/bitsoko/business.git && node business/index.js test_1_default
